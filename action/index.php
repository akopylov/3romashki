<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "����� �� �������� 3 ������� � ����, �������������� �������� ����������� ����������� ��������");
$APPLICATION->SetTitle("�����");
$APPLICATION->SetPageProperty("title", "����� - 3 ������� � ����, �������� ���������");
$APPLICATION->SetPageProperty("description", false);
$APPLICATION->SetPageProperty("keywords", false);
?>

<?
$ProductID = Array();
$dbProductDiscounts = CCatalogDiscount::GetList(
    array("id" => "desc"),
    array(
            "ACTIVE" => "Y",
            "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"),
                                               "YYYY-MM-DD HH:MI:SS",
                                               CSite::GetDateFormat("FULL")),
            "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"),
                                             "YYYY-MM-DD HH:MI:SS",
                                             CSite::GetDateFormat("FULL")),
        ),
    false,
    false,
    array(
            "ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO",
            "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE",
    "VALUE", "CURRENCY", "PRODUCT_ID"
        )
    );
while ($arProductDiscounts = $dbProductDiscounts->Fetch())
{

	$ProductID[$arProductDiscounts["PRODUCT_ID"]] = $arProductDiscounts["PRODUCT_ID"];
}

if(is_array($ProductID) AND count($ProductID)){
	//if(isset($_REQUEST["dev"])){}
		$arSelect = Array("ID", "PROPERTY_CML2_LINK");
		$arFilter = Array(
			"IBLOCK_ID"=>13,
			"ACTIVE_DATE"=>"Y",
			"ACTIVE"=>"Y",
			"ID" => $ProductID
		);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){
		 	$arFields = $ob->GetFields();
		 	unset($ProductID[$arFields["ID"]]);
		 	$ProductID[$arFields["PROPERTY_CML2_LINK_VALUE"]] = $arFields["PROPERTY_CML2_LINK_VALUE"];
		}

	$brend = Array();
	$arSelect = Array("ID", "NAME", "PROPERTY_BRAND");
	$arFilter = Array(
		"IBLOCK_ID"=>12,
		"ACTIVE_DATE"=>"Y",
		"ACTIVE"=>"Y",
		"ID" => $ProductID
	);
	$res = CIBlockElement::GetList(Array(), $arFilter, Array("PROPERTY_BRAND"), false, $arSelect);
	while($ob = $res->GetNextElement()){
	 	$arFields = $ob->GetFields();
	 	if($arFields["PROPERTY_BRAND_ENUM_ID"]){
	 		$brend[$arFields["PROPERTY_BRAND_ENUM_ID"]] = $arFields["PROPERTY_BRAND_VALUE"];
	 	}
	}
}
?>

<? if(count($ProductID)){ ?>
<? global $arrFilteresID;
$arrFilteresID = Array(
	"ID" => $ProductID
);
if(isset($_REQUEST["brands"])){
	$arrFilteresID["PROPERTY_BRAND"] = $_REQUEST["brands"];
}
 ?>

 <? if(is_array($brend) AND count($brend)){ ?>
 <div class="FilterCatalog">
 	<div class="name">�������� ������ ���������� �������������:</div>
 	<div class="items">
 		<? if($_REQUEST["brands"]){ ?>
 		<a href="/action/">��� ������</a>
 		<? } ?>
 		<? foreach($brend AS $key => $value){ ?>
 		<a href="?brands=<?=$key?>"<?=(($_REQUEST["brands"] AND $_REQUEST["brands"] == $key)?" class=\"select\"":"")?>><?=$value?></a>
 		<? } ?>
 	</div>
 </div>
 <style>
 .FilterCatalog{
 	padding:0px 0px 20px 0px;
 }
 .FilterCatalog .name{
 	font-size:16px;
 	font-weight:bold;
 }
 .FilterCatalog .items{
 	font-size:0px;
 	padding:10px 0px 0px 0px
 }
 .FilterCatalog .items a{
 	display:inline-block;
 	font-size:12px;
 	padding:5px 10px;
 	margin:0px 10px 5px 0px;
 	background:#fff;
 	border:2px solid #ff5800;
 	color:#fff;
 	border-radius:10px;
 }
  .FilterCatalog .items a.select,
 .FilterCatalog .items a:hover{
 	background:#ff5800;
 	color:#fff !important;
 }
 </style>
 <? } ?>


<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"gopro",
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAXPAGESID" => "ajaxpages_main",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"COLUMNS5" => "Y",
		"COMPARE_PATH" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "/catalog/#SECTION_CODE_PATH#/#CODE#/",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DONT_SHOW_LINKS" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "timestamp_x",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"FILTER_NAME" => "arrFilteresID",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "A",
		"IS_AJAXPAGES" => $IS_AJAXPAGES,
		"IS_SORTERCHANGE" => $IS_SORTERCHANGE,
		"LINE_ELEMENT_COUNT" => "5",
		"MAIN_TITLE" => "������� �� �������",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "� �������",
		"MESS_BTN_BUY" => "������",
		"MESS_BTN_DETAIL" => "���������",
		"MESS_BTN_SUBSCRIBE" => "�����������",
		"MESS_NOT_AVAILABLE" => "��� � �������",
		"META_DESCRIPTION" => "",
		"META_KEYWORDS" => "",
		"MIN_AMOUNT" => "10",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"OFFERS_LIMIT" => "20",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "PROP_50_WEBASYST",
			1 => "PROP_30_WEBASYST",
			2 => "PROP_33_WEBASYST",
			3 => "PROP_37_WEBASYST",
			4 => "PROP_16_WEBASYST",
			5 => "PROP_28_WEBASYST",
			6 => "PROP_47_WEBASYST",
			7 => "PROP_17_WEBASYST",
			8 => "PROP_40_WEBASYST",
			9 => "PROP_41_WEBASYST",
			10 => "PROP_39_WEBASYST",
			11 => "PROP_36_WEBASYST",
			12 => "PROP_45_WEBASYST",
			13 => "PROP_32_WEBASYST",
			14 => "PROP_22_WEBASYST",
			15 => "PROP_35_WEBASYST",
			16 => "PROP_38_WEBASYST",
			17 => "PROP_29_WEBASYST",
			18 => "PROP_23_WEBASYST",
			19 => "PROP_46_WEBASYST",
			20 => "PROP_31_WEBASYST",
			21 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "asc",
		"OFF_MEASURE_RATION" => "Y",
		"OFF_SMALLPOPUP" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "gopro",
		"PAGER_TITLE" => "������",
		"PAGE_ELEMENT_COUNT" => "20",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "PROP_33_WEBASYST",
			2 => "PROP_37_WEBASYST",
			3 => "PROP_16_WEBASYST",
			4 => "PROP_28_WEBASYST",
			5 => "PROP_47_WEBASYST",
			6 => "PROP_17_WEBASYST",
			7 => "PROP_40_WEBASYST",
			8 => "PROP_41_WEBASYST",
			9 => "PROP_39_WEBASYST",
			10 => "PROP_50_WEBASYST",
			11 => "PROP_30_WEBASYST",
			12 => "PROP_36_WEBASYST",
			13 => "PROP_45_WEBASYST",
			14 => "PROP_32_WEBASYST",
			15 => "PROP_22_WEBASYST",
			16 => "PROP_35_WEBASYST",
			17 => "PROP_38_WEBASYST",
			18 => "PROP_29_WEBASYST",
			19 => "PROP_23_WEBASYST",
			20 => "PROP_34_WEBASYST",
			21 => "PROP_15_WEBASYST",
			22 => "PROP_46_WEBASYST",
			23 => "PROP_31_WEBASYST",
			24 => "",
		),
		"PROPS_ATTRIBUTES" => array(
			0 => "PROP_50_WEBASYST",
			1 => "PROP_30_WEBASYST",
			2 => "PROP_16_WEBASYST",
			3 => "PROP_17_WEBASYST",
			4 => "PROP_22_WEBASYST",
			5 => "PROP_23_WEBASYST",
			6 => "PROP_28_WEBASYST",
			7 => "PROP_29_WEBASYST",
			8 => "PROP_31_WEBASYST",
			9 => "PROP_32_WEBASYST",
			10 => "PROP_33_WEBASYST",
			11 => "PROP_35_WEBASYST",
			12 => "PROP_36_WEBASYST",
			13 => "PROP_37_WEBASYST",
			14 => "PROP_38_WEBASYST",
			15 => "PROP_39_WEBASYST",
			16 => "PROP_40_WEBASYST",
			17 => "PROP_41_WEBASYST",
			18 => "PROP_45_WEBASYST",
			19 => "PROP_46_WEBASYST",
			20 => "PROP_47_WEBASYST",
		),
		"PROPS_ATTRIBUTES_COLOR" => array(
		),
		"PROP_ACCESSORIES" => "ACCESSORIES",
		"PROP_ARTICLE" => "CML2_ARTICLE",
		"PROP_MORE_PHOTO" => "MORE_PHOTO",
		"PROP_SKU_ARTICLE" => "-",
		"PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => $_REQUEST["SECTION_CODE_PATH"],
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "/catalog/#SECTION_CODE_PATH#/",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SEF_RULE" => "/catalog/#SECTION_CODE_PATH#/",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "",
		"USE_AUTO_AJAXPAGES" => "N",
		"USE_FAVORITE" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_SHARE" => "Y",
		"USE_STORE" => "N",
		"VIEW" => "showcase",
		"COMPONENT_TEMPLATE" => "gopro"
	),
	false
);?>

<? } ?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
error_reporting( E_ERROR );
set_time_limit(0);
$_SERVER["DOCUMENT_ROOT"] =   __DIR__.'/..';
define("NO_KEEP_STATISTIC", true); // Не собираем стату по действиям AJAX
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/*
global $USER;
if (!$USER->IsAdmin()){
    die('У вас нет прав');
}
*/
define('CatalogID', 12);
if(CModule::IncludeModule("iblock")) {
    logCatalog('logCatalog.info', 'Import Start:'.date('d.m.Y_H.i.s'));
    $arSelect = Array(
        "ID"
    );
    $arFilter = Array(
        "IBLOCK_ID" => CatalogID,
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y",
        "PROPERTY_DICALOV_BASE_PRICES" => false,
    );
    $res = CIBlockElement::GetList(Array(), $arFilter, false,  false, $arSelect);
    $i = 0;

    while ($ob = $res->GetNextElement()) {
        $CATALOG_PRICE_1 = 1000000000000;
        $arFields = $ob->GetFields();
        $offersExist = CCatalogSKU::getExistOffers($arFields['ID']);
        if($offersExist[$arFields['ID']]) {
            $propFilter['ID'] = array();
            $select = array('CATALOG_GROUP_1');
            $arResult = CCatalogSKU::getOffersList($arFields['ID'], 0, array('ACTIVE' => 'Y'), $select, $propFilter);
            if(count($arResult[$arFields['ID']]) > 0 AND $arResult[$arFields['ID']]){
                foreach ($arResult[$arFields['ID']] AS $value){
                    if($CATALOG_PRICE_1 > $value['CATALOG_PRICE_1'] AND $value['CATALOG_PRICE_1'] != 0){
                        $CATALOG_PRICE_1 = $value['CATALOG_PRICE_1'];
                    }
                }
            }
            if($CATALOG_PRICE_1 != 1000000000000 AND $CATALOG_PRICE_1 != 0) {
                $ar_res = CPrice::GetBasePrice($arFields['ID']);
                $arFieldsP = Array();
                if ($ar_res['PRODUCT_ID'] AND $ar_res['CATALOG_GROUP_ID']) {
                    $arFieldsP = Array(
                        "EXTRA_ID" => $ar_res['EXTRA_ID'],
                        "PRODUCT_ID" => $arFields['ID'],
                        "CATALOG_GROUP_ID" => $ar_res['CATALOG_GROUP_ID'],
                        "PRICE" => $CATALOG_PRICE_1,
                        "CURRENCY" => $ar_res['CURRENCY'],
                    );
                    CPrice::Update($ar_res['ID'], $arFieldsP);
                } else {
                    $arFieldsP = Array(
                        "PRODUCT_ID" => $arFields['ID'],
                        "CATALOG_GROUP_ID" => "1",
                        "PRICE" => $CATALOG_PRICE_1,
                        "CURRENCY" => "RUB",
                    );
                     CPrice::Add($arFieldsP);
                }

            }
        }
    }
    logCatalog('logCatalog.info', 'Import End:'.date('d.m.Y_H.i.s'));
    //echo '<a href="logCatalog('.$timeLog.').info">Результат выгрузки</a>';
}

//logCatalog('logCatalog('.date('d.m.Y_H.i.s').').info', 'ID:  Prices:');
function logCatalog($name, $text){
    $fp = fopen(dirname(__FILE__).'/'.$name, 'a');
    fwrite($fp, $text . "\r\n");
    fclose($fp);
}

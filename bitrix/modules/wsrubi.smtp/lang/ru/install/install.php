<?php
/**
 * @link      http://wsrubi.ru/dev/bitrixsmtp/
 * @author Sergey Blazheev <s.blazheev@gmail.com>
 * @copyright Copyright (c) 2011-2017 Altair TK. (http://www.wsrubi.ru)
 */
	$MESS['WSRUBI_MODULE_NAME'] = "�������� ����� ����� SMTP";
	$MESS['WSRUBI_MODULE_DESCRIPTION'] = "������ �������� ���������� e-mail ����� SMTP";
	$MESS['WSRUBI_PARTNER_NAME'] = "wsrubi.ru";
	$MESS['WSRUBI_PARTNER_URI'] = "http://wsrubi.ru/";
	$MESS['WSRUBI_NEED_RIGHT_VER'] = "��� ��������� ��������� ������� ������ ������ #NEED# ��� ����.";
	$MESS['WSRUBI_NEED_MODULES'] = "��� ��������� ������� ������� ���������� ������� ������ #MODULE#.";
	$MESS['WSRUBI_ERROR'] = "������ �������� ����� ������� custom_mail. ������� custom_mail ��� ������������.";
?>
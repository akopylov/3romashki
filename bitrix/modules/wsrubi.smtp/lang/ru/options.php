<?php
/**
 * @link      http://wsrubi.ru/dev/bitrixsmtp/
 * @author Sergey Blazheev <s.blazheev@gmail.com>
 * @copyright Copyright (c) 2011-2016 Altair TK. (http://www.wsrubi.ru)
 */
	$MESS['WSRUBI_TAB_EXTRAMAIL_SETTINGSOUTSMTP'] 		= "���������";
	$MESS['WSRUBI_TAB_TITLE_EXTRAMAIL_SETTINGSOUTSMTP'] = "��������� ��� �������� ����� ����� smtp ������";
	$MESS['MAIN_TAB_LOG'] 								= "������";
	$MESS['MAIN_TAB_LOG'] 								= "������";
	$MESS['wsrubismtp_include_on'] 						= "���� ������ ���������";
	$MESS['wsrubismtp_include_off'] 					= "���� ������ �� ���������";
    $MESS['wsrubismtp_how_include'] 					= '��� ����������� ������ ���������� �������� ������ <br/><b>include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");</b><br/> � ���� <b>/bitrix/php_interface/init.php</b> ��� <b>/local/php_interface/init.php</b>, ���� ���� ����������� �� ��� ���������� �������';

	$MESS['heading_smtp_type_profile'] 			        = "������� ������� (���� ���������)";
    $MESS['settings_smtp_type_profile'] 			    = "������� �������";
    $MESS['settings_smtp_type_profile_yandex'] 			= "SMTP Yandex";
    $MESS['settings_smtp_type_profile_google'] 			= "SMTP Google";
    $MESS['settings_smtp_type_profile_mail_ru'] 		= "SMTP Mail.ru";

	$MESS['heading_smtp_connection_settings'] 			= "��������� ����������";
 	$MESS['heading_smtp_connection_settings_for'] 		= "��������� ���������� ��� - ";

	$MESS['posting'] 									= "�� ������������ ��� ��������";
    $MESS['onlyposting'] 								= "������������ ������ ��� ��������";
	$MESS['check_settings'] 							= "�������� ��������� ������";
	$MESS['smtp_active'] 								= "������ �������";
    $MESS['settings_smtp_from'] 						= "�� ( ���� �� ������ �� ������������ e-mail �� �������� �������� ������ -> 'E-Mail �������������� ����� (����������� �� ���������)', ���� � �� �� ������ �� test@test.ru )";
	$MESS['settings_smtp_host'] 						= "SMTP ���� ";
	$MESS['settings_smtp_port'] 						= "SMTP ����  ( � ������� 25,465 ...)";
	$MESS['settings_smtp_type_auth'] 					= "��� ������������";
	$MESS['settings_smtp_type_auth_smtp'] 				= "SMTP ������������";
	$MESS['settings_smtp_type_auth_login'] 				= "Login ������������(�� ���������)";
    $MESS['settings_smtp_type_auth_login_info'] 		= "������ SMTP ������� ������� ���������� `SMTP ����� ������������` � ������ ���������� � `�� ����`. ���������� ������� ����� ( ���������->��������� ���������->�������� ������� )";
	$MESS['settings_smtp_type_auth_plain'] 				= "Plain ������������";
	$MESS['settings_smtp_type_auth_crammd5']			= "CRAM-MD5 ������������";
	$MESS['settings_smtp_login'] 						= "SMTP ��� ������������";
	$MESS['settings_smtp_password'] 					= "SMTP ������ ������������";
	$MESS['settings_smtp_log'] 							= "����� ������";
	$MESS['settings_smtp_log_message'] 					= "��� �������";
	$MESS['settings_smtp_test'] 						= "��������";
	$MESS['settings_smtp_connection_default'] 			= "�������� �� �����������";
	$MESS['settings_smtp_connection_error'] 			= "������ ����������";
	$MESS['settings_smtp_connection_success'] 			= "���������� �����������";
	$MESS['settings_smtp_testing']			 			= "���������";
	$MESS['settings_smtp_testing_info']			 		= "����� ������������ ���������� ��������� ���������";
	$MESS['settings_smtp_testing_email']			 	= "�������� ���� ��� �����";
	$MESS['settings_test_message_subject'] 				= "test SMTP";
	$MESS['settings_test_message_body'] 				= "test SMTP";
	$MESS['settings_smtp_log_clean'] 					= "�������� ������";
	$MESS['settings_smtp_type_encryption'] 				= "��� ����������";
	$MESS['settings_smtp_timeout'] 						= "������� (� ��������)";
	$MESS['settings_smtp_timeout_error'] 				= "������ ����� ���� '�������'";
	$MESS['settings_smtp_type_encryption_no'] 			= "��� ����������";
	$MESS['settings_smtp_type_encryption_ssl'] 			= "SSL";
	$MESS['settings_smtp_type_encryption_tls'] 			= "TLS";
	$MESS['settings_smtp_convert_to_utf8'] 				= "�������������� ���� ������ � utf-8(������ ��� ������ � ���������� Windows-1251)";
	$MESS['heading_smtp_connection_settings_advanced']  = "�������������� ���������";
	$MESS['header_settings_smtp_advanced']	 		    = "�������� �������������� E-mail SMTP �������";

	$MESS["MAIN_settings_APPLY"] 						= "���������";
	$MESS["MAIN_settings_APPLY_TITLE"] 					= "��������� ��������� � �������� � �����";
	$MESS['SAVE'] 										= "���������";
	$MESS['ADD'] 										= "��������";
	$MESS['RESET']	 									= "��������";
	$MESS['DELETE']	 									= "�������";
	$MESS['Timeout']	 								= "�������(������)";
    $MESS['addrtovalidation']	 						= "��������� ������ ������ ����������";

	$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_FROM"] = "������� � ������� ���������� ���� \"c\" ������ � ������.";
	$MESS["MAIN_EVENTLOG_WRONG_TIMESTAMP_X_TO"] = "������� � ������� ���������� ���� \"��\" ������ � ������.";
	$MESS["MAIN_EVENTLOG_ID"] = "ID";
	$MESS["MAIN_EVENTLOG_TIMESTAMP_X"] = "�����";
	$MESS["MAIN_EVENTLOG_SEVERITY"] = "���������";
	$MESS["MAIN_EVENTLOG_AUDIT_TYPE_ID"] = "�������";
	$MESS["MAIN_EVENTLOG_MODULE_ID"] = "��������";
	$MESS["MAIN_EVENTLOG_ITEM_ID"] = "������";
	$MESS["MAIN_EVENTLOG_REMOTE_ADDR"] = "IP";
	$MESS["MAIN_EVENTLOG_USER_AGENT"] = "User Agent";
	$MESS["MAIN_EVENTLOG_REQUEST_URI"] = "URL";
	$MESS["MAIN_EVENTLOG_SITE_ID"] = "����";
	$MESS["MAIN_EVENTLOG_USER_ID"] = "������������";
	$MESS["MAIN_EVENTLOG_DESCRIPTION"] = "��������";
	$MESS["MAIN_EVENTLOG_GUEST_ID"] = "�����";
	$MESS["MAIN_EVENTLOG_LIST_PAGE"] = "������";
	$MESS["MAIN_EVENTLOG_PAGE_TITLE"] = "������ �������";
	$MESS["MAIN_EVENTLOG_SEARCH"] = "�����";
	$MESS["MAIN_EVENTLOG_USER_AUTHORIZE"] = "�������� ����";
	$MESS["MAIN_EVENTLOG_USER_LOGIN"] = "������ �����";
	$MESS["MAIN_EVENTLOG_USER_LOGINBYHASH_FAILED"] = "������ ����� ��� ����������� �����������";
	$MESS["MAIN_EVENTLOG_USER_LOGOUT"] = "����� �� �������";
	$MESS["MAIN_EVENTLOG_USER_REGISTER"] = "����������� ������ ������������";
	$MESS["MAIN_EVENTLOG_USER_REGISTER_FAIL"] = "������ �����������";
	$MESS["MAIN_EVENTLOG_USER_INFO"] = "������ �� ����� ������ ������������";
	$MESS["MAIN_EVENTLOG_USER_PASSWORD_CHANGED"] = "����� ������ ������������";
	$MESS["MAIN_EVENTLOG_USER_DELETE"] = "�������� ������������";
	$MESS["MAIN_EVENTLOG_STOP_LIST"] = "����-����";
	$MESS["MAIN_EVENTLOG_FORUM_MESSAGE"] = "���������";
	$MESS["MAIN_EVENTLOG_FORUM_TOPIC"] = "����";
	$MESS["MAIN_EVENTLOG_GROUP"] = "�������� ������ ������������";
	$MESS["MAIN_EVENTLOG_GROUP_POLICY"] = "�������� �������� ������������ ������";
	$MESS["MAIN_EVENTLOG_MODULE"] = "������� ������ ������ � ������";
	$MESS["MAIN_EVENTLOG_FILE"] = "������� ������ � �����";
	$MESS["MAIN_EVENTLOG_TASK"] = "������� ������� �������";
	$MESS["MAIN_EVENTLOG_IBLOCK"] = "��������";
	$MESS["MAIN_EVENTLOG_IBLOCK_DELETE"] = "������";
	$MESS["MAIN_ALL"] = "(���)";
	$MESS["MAIN_EVENTLOG_MP_MODULE_INSTALLED"] = "������� Marketplace ������������";
	$MESS["MAIN_EVENTLOG_MP_MODULE_UNINSTALLED"] = "������� Marketplace �������";
	$MESS["MAIN_EVENTLOG_MP_MODULE_DELETED"] = "������� Marketplace ������";
	$MESS["MAIN_EVENTLOG_MP_MODULE_DOWNLOADED"] = "������� Marketplace �������";

    $MESS['WSRUBI_SUPPORT_TAB'] 		= "���������";
    $MESS['WSRUBI_SUPPORT_TITLE'] 		= "���������";
    $MESS['WSRUBI_ABOUT_DEV'] 		    = "�����������";
    $MESS['WSRUBI'] 		            = "���-������ RUBI";
    $MESS['WSRUBI_SERVICE_TITLE']       = "���� ������";
    $MESS['WSRUBI_SERVICE_LIST']        = "<ul><li>������������������ ���������� CRM/ERP/�������/������ �������;</li><li>����������/��������� � ������������ ������ �� \"�������\";</li><li>������� � ����������� � ���������;</li></ul>";
    $MESS['WSRUBI_SUPPORT']             = "���������";
    $MESS['WSRUBI_SUPPORT_INFO']        = "������ ������ �� ������ ������ ����� �� ����������� ����� <a href='mailto:support@wsrubi.ru' >support@wsrubi.ru</a> ��� �������� �����";
?>
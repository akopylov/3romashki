<?php
/**
 * @link      http://wsrubi.ru/dev/bitrixsmtp/
 * @author Sergey Blazheev <s.blazheev@gmail.com>
 * @copyright Copyright (c) 2011-2017 Altair TK. (http://www.wsrubi.ru)
 */
$arModuleVersion = array(
    "VERSION" => "0.2.10",
    "VERSION_DATE" => "2018-09-06"
);
?>
<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Serge                            #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2011 ALTASIB             #
#################################################
?>
<?
$MESS['ALTASIB_ENABLE_SNOW'] = "Insert a New Year's snow!";
$MESS['ALTASIB_ENABLE_SNOW_AUTH'] = "<font color='red'>OFF</font> snow for authorized users";
$MESS ['ALTASIB_IS'] = "Store ready solutions for 1C-Bitrix";
$MESS ['ALTASIB_SPEED'] = "The rate of dropping out of the snow";
$MESS ['ALTASIB_SNOWLETTER'] = "Enter the character that will represent the snow";
$MESS ['ALTASIB_SNOWMAX'] = "Maximum number of snowflakes";
$MESS ['ALTASIB_COLOR'] = "Colors snowflakes";
?>

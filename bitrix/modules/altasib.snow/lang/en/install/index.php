<?
#################################################
#   Company developer: ALTASIB                  #
#   Developer: Serge                            #
#   Site: http://www.altasib.ru                 #
#   E-mail: dev@altasib.ru                      #
#   Copyright (c) 2006-2011 ALTASIB             #
#################################################
?>
<?
$MESS["ALTASIB_SNOW_MODULE_NAME"] = "Snowfall";
$MESS["ALTASIB_SNOW_MODULE_DESCRIPTION"] = "Falling snow for New Site";
$MESS["ALTASIB_SNOW_INSTALL_TITLE"] = "Installing the module: Snowfall";
$MESS["ALTASIB_SNOW_UNINSTALL_TITLE"] = "Removing the module: Snowfall";
$MESS["ALTASIB_SNOW_INSTALL_ACCESS_DENIED"] = "access denied";
$MESS["ALTASIB_SNOW_INSTALL_FULL_ACCESS"] = "full access";
$MESS["MOD_INST_ERR"]="Error";
$MESS["MOD_INST_OK"]="module installed";
?>

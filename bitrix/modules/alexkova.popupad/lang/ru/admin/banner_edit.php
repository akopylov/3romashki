<?
$MESS['POPUPAD_REK_TAB_DEFAULT'] = "������";
$MESS['POPUPAD_REK_TAB_DEFAULT_TITLE'] = "�������������� �������";
$MESS['POPUPAD_REK_TAB_TARGETING'] = "���������";
$MESS['POPUPAD_REK_TAB_TARGETING_TITLE'] = "�� ����� ��������� ����������/�� ���������� ������";
$MESS['POPUPAD_REK_TAB_STAT'] = "����������";
$MESS['POPUPAD_REK_TAB_STAT_TITLE'] = "���������� �������";
$MESS['POPUPAD_REK_TAB_ICONS'] = "������";
$MESS['POPUPAD_REK_TAB_ICONS_TITLE'] = "��������� ������";
$MESS['POPUPAD_BANNER_SAVE_ERROR'] = "�� ������� ��������� ������";
$MESS['POPUPAD_BANNER_TITLE_EDIT'] = "�������������� ������� #";
$MESS['POPUPAD_BANNER_TITLE_ADD'] = "���������� �������";
$MESS['POPUPAD_BANNER_SAVED'] = "������ ������� ��������";
$MESS['POPUPAD_BANNER_ACTIVE'] = "�������:";
$MESS['POPUPAD_BANNER_NAME'] = "�������� �������:";
$MESS['POPUPAD_BANNER_TYPE'] = "��������� ��������:";
$MESS['POPUPAD_BANNER_FILE'] = "����:";
$MESS['POPUPAD_BANNER_LIST'] = "������ ��������";
$MESS['POPUPAD_BANNER_SHOW_TYPE'] = "���������� ������ ���:";
$MESS['POPUPAD_BANNER_SHOW_ON'] = "��� ����������:<br/>(������������� ����, ������ � ����� ������,<br/>����� ��������� ����� � ������� ������� '*', �������� /news/*/photo/ )";
$MESS['POPUPAD_BANNER_SHOW_OFF'] = "��� �� ����������:<br/>(������������� ����, ������ � ����� ������,<br/>����� ��������� ����� � ������� ������� '*', �������� /news/*/photo/ )";
$MESS['POPUPAD_BANNER_LIST_TITLE'] = "��������� � ������ ��������";
$MESS['POPUPAD_SHOW_DATE'] = "�������� ������:";
$MESS['POPUPAD_BANNER_WEIGHT'] = "���������:";
$MESS['POPUPAD_BANNER_SHOW'] = "������� �������";
$MESS['POPUPAD_BANNER_SHOW_IMAGE'] = "�����������";
$MESS['POPUPAD_BANNER_SHOW_FLASH'] = "Flash";
$MESS['POPUPAD_BANNER_SHOW_HTML'] = "HTML";
$MESS['POPUPAD_BANNER_FLASH_TRANSPARENT'] = "������������:";
$MESS['POPUPAD_BANNER_FLASH_DUMMY_USE'] = "������������ ��������, ���� �������� flash:";
$MESS['POPUPAD_BANNER_DUMMY_FILE'] = "��������:";
$MESS['POPUPAD_BANNER_FLASH_OVERFLOW'] = "��������� ������ �������:";
$MESS['POPUPAD_BANNER_DELETE'] = "������� ������";
$MESS['POPUPAD_BANNER_DELETE_TITLE'] = "������� ������� ������";
$MESS['POPUPAD_BANNER_DELETE_CONFIRM'] = "�������, ��� ������ ������� ������ ������?";
$MESS['POPUPAD_BANNER_NEW'] = "�������� ������";
$MESS['POPUPAD_BANNER_NEW_TITLE'] = "�������� ����� ������";
$MESS['POPUPAD_BANNER_RESET_COUNTER'] = "�������� ������� ������� ��� ����������";
$MESS['POPUPAD_SELECT_BANTYPE'] = "--�������� ��������--";
$MESS['POPUPAD_BANNER_SITE'] = "�����:";
$MESS['POPUPAD_BANNER_URL'] = "������ (������� href):";
$MESS['POPUPAD_BANNER_TITLE'] = "����������� �����(������� title):";
$MESS['POPUPAD_BANNER_TARGET'] = "��� ������� �������� ��� �������� �� ������ (������� target):";
$MESS['POPUPAD_BANNER_REDIRECT'] = "�������� ��� �����";
$MESS['POPUPAD_BANNER_TARGET_SELF'] = "� ������� ���� (_self)";
$MESS['POPUPAD_BANNER_TARGET_BLANK'] = "� ����� ���� (_blank)";
$MESS['POPUPAD_BANNER_TARGET_PARENT'] = "� ������-�������� (_parent)";
$MESS['POPUPAD_BANNER_TARGET_TOP'] = "� ������ ���� �������� (_top)";
$MESS['POPUPAD_BANNER_SHOW_COUNT'] = "������� ���������� �������:";
$MESS['POPUPAD_BANNER_STAT_SHOW'] = "����������� ���������� �������:";
$MESS['POPUPAD_BANNER_STAT_SHOW_TITLE'] = "������:";
$MESS['POPUPAD_BANNER_STAT_CLICK_TITLE'] = "��������:";
$MESS['POPUPAD_BANNER_STAT_CLICK'] = "����������� ���������� ���������:";
$MESS['POPUPAD_BANNER_UNIQUE'] = "����������� ������ ����������";
$MESS['POPUPAD_BANNER_UNIQUE_SESS'] = "������";
$MESS['POPUPAD_BANNER_UNIQUE_COOKIE'] = "cookie";
$MESS['POPUPAD_BANNER_UNIQUE_COOKIE_TIME'] = "����� ����� cookie (� ��������)";
$MESS['POPUPAD_BANNER_UNIQUE_TYPE'] = "���������� ������������ ��";
$MESS['POPUPAD_RK_REDIR_ERROR_1'] = "��� �������� ���������� ��������� ����� �������� ����� \"�������� ��� �����\"!";
$MESS['POPUPAD_RK_REDIR_ERROR_2'] = "��� ����������� ��������� ���������� ��������� ���������� ���������!";
$MESS['POPUPAD_BANNER_STAT_TITLE'] = "�������� ����������";
$MESS['POPUPAD_BANNER_STAT_UNIQUE_TITLE'] = "�������� ������������";
$MESS['POPUPAD_REK_TAB_SHOW_PROP'] = "�������� ������";
$MESS['POPUPAD_REK_TAB_SHOW_PROP_TITLE'] = "�������� ������ ������";
$MESS['POPUPAD_BANNER_SHOW_TIMER'] = "����� �������� (� ��������) ����� ������� ������";
$MESS['POPUPAD_BANNER_TYPE_SHOW'] = "����� ���������� �����";
$MESS['POPUPAD_BANNER_SHOW_PER_TIME'] = "������ ������� (� ��������), � ������� �������� ����������� ���������� ������ ��������";
$MESS['POPUPAD_BANNER_IN_SESSION'] = "��� � ������ ������������";
$MESS['POPUPAD_BANNER_IN_TIME'] = "��� � ������� �������";

$MESS['POPUPAD_BANNER_USE_ICONS'] = "������������ ������";
$MESS['POPUPAD_BANNER_ICON_FILES'] = "���� ������";
$MESS['POPUPAD_BANNER_ICON_PLACE'] = "�������������� ������ �� ������";
$MESS['POPUPAD_BANNER_ICON_MARGIN1'] = "�������� ������";
$MESS['POPUPAD_BANNER_ICON_MARGIN2'] = "�������������� ������";
$MESS['POPUPAD_BANNER_ICON_PLACE_LT'] = "����� ������";
$MESS['POPUPAD_BANNER_ICON_PLACE_LB'] = "����� �����";
$MESS['POPUPAD_BANNER_ICON_PLACE_BL'] = "����� �����";
$MESS['POPUPAD_BANNER_ICON_PLACE_BR'] = "����� ������";
$MESS['POPUPAD_BANNER_ICON_PLACE_RT'] = "������ ������";
$MESS['POPUPAD_BANNER_ICON_PLACE_RB'] = "������ �����";
$MESS['POPUPAD_BANNER_ICON_FIXED'] = "�����������";
$MESS['POPUPAD_BANNER_ICON_SIZE'] = "������� ������";
$MESS['POPUPAD_BANNER_ICON_SIZE_LIM'] = "��";
$MESS['POPUPAD_BANNER_ICON_SHOW'] = "����� ������";
$MESS['POPUPAD_SHOW_TYPE'] = "��� ������";
$MESS['POPUPAD_SHOW_TYPE_DEFAULT'] = "��-���������";
$MESS['POPUPAD_SHOW_TYPE_ICON'] = "������";
$MESS['POPUPAD_BANNER_ICON_SHOW_IMAGE'] = "���� �� ������ ������� ����������� ������";
$MESS['POPUPAD_BANNER_ICON_SHOW_CLOSE'] = "������������ ����� ������ ������";
?>
<?
$MESS ['POPUPAD_ACCESS'] = "������";
$MESS ['POPUPAD_TITLE'] = "��������� �������";
$MESS ['POPUPAD_OPTIONS'] = "���������";
$MESS ['POPUPAD_OPTIONS_TITLE'] = "��������� ������";
$MESS ['POPUPAD_SHOW_MODE_DEFAULT'] = "������� �����";
$MESS ['POPUPAD_SHOW_MODE_ICONS'] = "������";
$MESS ['POPUP_HIDE_FANCY'] = "��������� ����� ������� ����������� �������� (�� � ������ ������)";
$MESS ['POPUP_HIDE_ICON'] = "��������� ����� ������ ����������� ��������";
$MESS ['FORM_SAVE'] = "���������";
$MESS ['FORM_RESET'] = "��������";
$MESS ['MAIN_RESTORE_DEFAULTS'] = "�� ���������";

$MESS ['MAIN_SETTINGS'] = "�������� ���������";
$MESS ['POPUPAD_STAT_DAYS'] = "������� ���� ������� ����������";

$MESS ['POPUPAD_MAIN_OPTION'] = "���������";
$MESS ['POPUP_TIME_DELAY_SHOW'] = "����� �������� ������ �������� (���������� ������ �� ������ �������� �������� �� �������, ����� ����� ������� ������). ����� ���� �������������� � ���������� �������";
$MESS ['POPUP_PROTECTION_TIMER_TITLE'] = "<b>�����</b>, � ������� �������� ����������� ���������� ������� ������� (�� ������) �������� (�������������� ����������� �������� ������). <span style='font-size: 11px;'>(��������: \"3600\" ��������, ��� � ������� 1 ���� � ������� ���������� ������ ������������ �� ����� �������� �� ������ �������.)</span>";
$MESS ['SECONDS'] = '  ���.';
$MESS ['POPUP_FANCYBOX'] = '������������ ���������� fancybox';
$MESS ['POPUP_JQUERY'] = '������������ ���������� jQuery';
$MESS ['POPUP_FANCYBOX_OVERLAY'] = '������������ ���������� ������ fancybox (��� ���������� ������)';
$MESS ['POPUP_VERSION_2_ON'] = '�������� ����� ������ 2.0';
?>
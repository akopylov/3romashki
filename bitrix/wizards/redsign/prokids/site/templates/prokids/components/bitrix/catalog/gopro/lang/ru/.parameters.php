<?
$MESS['DETAIL_TABS_VIEW'] = '��� ����������� �������';
	$MESS['DETAIL_ARR_TABS_VIEW_TAB'] = '�������';
	$MESS['DETAIL_ARR_TABS_VIEW_ANCHOR'] = '�����';

$MESS['RSGOPRO_VIEW_MODE'] = '��� ����������� ��������';
	$MESS['RSGOPRO_VIEW_SECTIONS'] = '���������� �������';
	$MESS['RSGOPRO_VIEW_ELEMENTS'] = '���������� ������';
// section, element
$MESS['PROP_MORE_PHOTO'] = '�������� � ���. ������������� �������';
$MESS['HIGHLOAD'] = '��� ����������� ���';
	$MESS['HIGHLOAD_TYPE_SELECT'] = '���������� ������';
	$MESS['HIGHLOAD_TYPE_LIST'] = '����� � ������';
$MESS['PROP_ARTICLE'] = '�������� � ��������� ������';
$MESS['USE_FAVORITE'] = '������������ ���������';
$MESS['USE_SHARE'] = '������������ ������������ ����������';
$MESS['SHOW_ERROR_EMPTY_ITEMS'] = '���������� ������, ���� ������� �� �������';
$MESS['SHORT_SORTER'] = '������� ��� ����������';
$MESS['USE_AUTO_AJAXPAGES'] = '������������ ������������ ��� ��������� ��������';
$MESS['OFF_MEASURE_RATION'] = '��������� ����������� ��. ���.';
// element
$MESS['PROPS_TABS'] = '��������-�������';
$MESS['USE_CHEAPER'] = '���������� ������ "������ �������"';
$MESS['USE_BLOCK_MODS'] = '���������� ���� "�����������"';
$MESS['USE_BLOCK_BIGDATA'] = '���������� ���� "������������ ������������"';
$MESS['SHOW_PREVIEW_TEXT'] = '���������� � ���� � ������� ������';
// filter
$MESS['FILTER_TEMPLATE'] = '������ �������';
	$MESS['FILTER_TEMPLATE_gopro'] = '������ GoPro';
	$MESS['FILTER_TEMPLATE_gopro_20'] = '������ GoPro 2.0';
$MESS['PROPS_FILTER_COLORS'] = '��������, ��������� � ������� ������������� (�����)';
$MESS['FILTER_PRICE_GROUPED'] = '���� ���, ������� ������ ������������ �������� (�������� ������ ��� ������� ��� �������� �����������)';
$MESS['FILTER_PRICE_GROUPED_FOR'] = '������ ��� ���';
	$MESS['FILTER_PRICE_GROUPED_FOR_PRIDUCTS'] = '�������';
	$MESS['FILTER_PRICE_GROUPED_FOR_SKU'] = '�������� �����������';
$MESS['FILTER_PROP_SCROLL'] = '��������, � ������� ������������ ������';
$MESS['FILTER_PROP_SEARCH'] = '��������, � ������� ������������ �����';
$MESS['FILTER_FIXED'] = '������������ ����������� �������';
$MESS['FILTER_USE_AJAX'] = '������������ AJAX � �������';
$MESS['FILTER_DISABLED_PIC_EFFECT'] = '������ ��� ����������� �����������';
	$MESS['FILTER_DISABLED_PIC_EFFECT_none'] = '��� �������';
	$MESS['FILTER_DISABLED_PIC_EFFECT_cross'] = '��������������';
	$MESS['FILTER_DISABLED_PIC_EFFECT_opacity'] = '������������';
	$MESS['FILTER_DISABLED_PIC_EFFECT_hide'] = '������';
// showcase
$MESS['OFF_SMALLPOPUP'] = '��������� ��������� ��� ��������� ����';
// sorter
$MESS['SORTER_CNT_TEMPLATES'] = '�����������: ���-�� ������������ ��������';
$MESS['SORTER_TEMPLATE_NAME'] = '�����������: ������������ ������� �';
$MESS['SORTER_DEFAULT_TEMPLATE'] = '�����������: ������, ������������ �� ���������';
$MESS['SORTER_OFF_SORT_BY'] = '�����������: ��������� ����������� ����������';
$MESS['SORTER_OFF_OUTPUT_OF_SHOW'] = '�����������: ��������� ����������� �������� ����� ������� �� ��������';
$MESS['SORTER_OUTPUT_OF'] = '�����������: �������� ����� ������� �� �������� (10,15,20,...)';
$MESS['SORTER_OUTPUT_OF_DEFAULT'] = '�����������: ���-�� ������� �� �������� �� ���������';
// stores
$MESS['STORES_TEMPLATE'] = '������ �������';
	$MESS['STORES_TEMPLATE_gopro'] = '������ GoPro';
	$MESS['STORES_TEMPLATE_gopro_20'] = '������ GoPro 2.0';

// mods
$MESS['MODS_BLOCK_NAME'] = '������������ ����� � �������������';
// mods
$MESS['BIGDATA_BLOCK_NAME'] = '������������ ����� � ������������� ��������������';
// offers
$MESS['PROP_SKU_MORE_PHOTO'] = '�������� � ���. ������������� �������� �����������';
$MESS['PROP_SKU_ARTICLE'] = '�������� � ��������� �������� �����������';
$MESS['PROPS_ATTRIBUTES'] = '�������������� ������';
$MESS['PROPS_ATTRIBUTES_COLOR'] = '�������������� ���������� ������';
// offers - filter
$MESS['PROPS_SKU_FILTER_COLORS'] = '��������, ��������� � ������� ������������� (�����) (�������� �����������)';
$MESS['FILTER_SKU_PROP_SCROLL'] = '��������, � ������� ������������ ������ (�������� �����������)';
$MESS['FILTER_SKU_PROP_SEARCH'] = '��������, � ������� ������������ ����� (�������� �����������)';



///// ver 1.2.0
$MESS['CP_BC_TPL_USE_BIG_DATA'] = '���������� ������������ ������������';
$MESS["CP_BC_TPL_BIG_DATA_RCM_TYPE"] = "��� ������������";
$MESS["CP_BC_TPL_RCM_BESTSELLERS"] = "����� �����������";
$MESS["CP_BC_TPL_RCM_PERSONAL"] = "������������ ������������";
$MESS["CP_BC_TPL_RCM_SOLD_WITH"] = "����������� � ���� �������";
$MESS["CP_BC_TPL_RCM_VIEWED_WITH"] = "��������������� � ���� �������";
$MESS["CP_BC_TPL_RCM_SIMILAR"] = "������� ������";
$MESS["CP_BC_TPL_RCM_SIMILAR_ANY"] = "�����������/���������������/������� ������";
$MESS["CP_BC_TPL_RCM_PERSONAL_WBEST"] = "����� �����������/������������";
$MESS["CP_BC_TPL_RCM_RAND"] = "����� ������������";

<?
$MESS['CATALOG_QUANTITY'] = '����������';
$MESS['CATALOG_QUANTITY_FROM_TO'] = '�� #FROM# �� #TO#';
$MESS['CATALOG_QUANTITY_FROM'] = '�� #FROM#';
$MESS['CATALOG_QUANTITY_TO'] = '�� #TO#';
$MESS['CATALOG_PRICE_VAT'] = '� ���';
$MESS['CATALOG_PRICE_NOVAT'] = '��� ���';
$MESS['CATALOG_VAT'] = '���';
$MESS['CATALOG_NO_VAT'] = '�� ����������';
$MESS['CATALOG_VAT_INCLUDED'] = '��� ������� � ����';
$MESS['CATALOG_VAT_NOT_INCLUDED'] = '��� �� ������� � ����';
$MESS['CT_BCE_QUANTITY'] = '����������';
$MESS['CT_BCE_CATALOG_BUY'] = '������';
$MESS['CT_BCE_CATALOG_ADD'] = '� �������';
$MESS['CT_BCE_CATALOG_COMPARE'] = '���������';
$MESS['CT_BCE_CATALOG_NOT_AVAILABLE'] = '��� �� ������';
$MESS['OSTATOK'] = '�������';
$MESS['COMMENTARY'] = '�����������';
$MESS['ECONOMY_INFO'] = '(�������� � ���� - #ECONOMY#)';
$MESS['FULL_DESCRIPTION'] = '������ ��������';
$MESS['CT_BCE_CATALOG_TITLE_ERROR'] = '������';
$MESS['CT_BCE_CATALOG_TITLE_BASKET_PROPS'] = '�������� ������, ����������� � �������';
$MESS['CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR'] = '����������� ������ ��� ���������� ������ � �������';
$MESS['CT_BCE_CATALOG_BTN_SEND_PROPS'] = '�������';
$MESS['CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'] = '�������';

$MESS['GOPRO.MORE'] = '�����';
$MESS['ADD2BASKET'] = '�������� � �������';
$MESS['INBASKET'] = '� �������';
$MESS['INBASKET_TITLE'] = '������� � �������';
$MESS['FAVORITE'] = '� ���������';
$MESS['ADD2COMPARE'] = '�������� � ���������';

$MESS['ZOOM'] = '��� ���������� �������� �������� ������';
$MESS['PHOTO'] = '�������';

$MESS['SOLOPRICE_PRICE'] = '����';
$MESS['SOLOPRICE_PRICE_OLD'] = '������ ����';
$MESS['SOLOPRICE_DISCOUNT'] = '������';

$MESS['ARTICLE'] = '�������';

$MESS['BUY1CLICK'] = '������ � 1 ����';
$MESS['STORAGE'] = '��������� �� ������';

$MESS['SUBSCRIBE_PROD'] = '�����������';
$MESS['SUBSCRIBE_PROD_TITILE'] = '����������� �� ��������� ������';

$MESS['CHEAPER'] = '������ �������?';

$MESS['GO2DETAILFROMPREVIEW'] = '���������';

$MESS['RSGOPRO.DETAIL_PROD_ID'] = 'ID ������';
$MESS['RSGOPRO.DETAIL_PROD_NAME'] = '������������ ������';
$MESS['RSGOPRO.DETAIL_PROD_LINK'] = '������ �� �����';

$MESS['RSGOPRO.DETAIL_CHEAPER_TITLE'] = '����� ������ �������';

// component_epilog
$MESS['TABS_DETAIL_TEXT'] = '��������';
$MESS['TABS_PROPERTIES'] = '��������������';
$MESS['TABS_SET'] = '�����';
$MESS['TABS_REVIEW'] = '������';

// QB and DA2
$MESS['QB_AND_DA2_DAY'] = '��.';
$MESS['QB_AND_DA2_HOUR'] = '���.';
$MESS['QB_AND_DA2_MIN'] = '���.';
$MESS['QB_AND_DA2_SEC'] = '���.';
$MESS['QB_AND_DA2_QUANTITY'] = '��������';
$MESS['QB_AND_DA2_PRODANO'] = '�������';
$MESS['QB_AND_DA2_SHT'] = '��.';
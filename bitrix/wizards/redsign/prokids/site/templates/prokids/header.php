<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

$moduleId = "redsign.prokids";

// check module include
if (!Loader::includeModule($moduleId)) {
	ShowError(Loc::getMessage('RSGOPRO.ERROR_NOT_INSTALLED_GOPRO'));
	die();
}

if (!\Bitrix\Main\Loader::includeModule('redsign.devfunc')) {
    ShowError(Loc::getMessage('RSGOPRO.ERROR_NOT_INSTALLED_DEVFUNC'));
    die();
} else {
    RSDevFunc::Init(array('jsfunc'));
}

// is main page
$IS_MAIN = 'N';
if ($APPLICATION->GetCurPage(true) == SITE_DIR.'index.php')
	$IS_MAIN = 'Y';

// is catalog page
$IS_CATALOG = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'catalog/') === false)
	$IS_CATALOG = 'N';

// is personal page
$IS_PERSONAL = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'personal/') === false)
	$IS_PERSONAL = 'N';

// is auth page
$IS_AUTH = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'auth/') === false)
	$IS_AUTH = 'N';

// is ajax hit
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_SERVER['HTTP_X_FANCYBOX']) || isset($_REQUEST['AJAX_CALL']) && 'Y' == $_REQUEST['AJAX_CALL'];

CJSCore::Init('ajax');
$adaptive = COption::GetOptionString($moduleId, 'adaptive', 'Y');
$prop_option = COption::GetOptionString($moduleId, 'prop_option', 'line_through');
$Asset = Asset::getInstance();

// add strings
$Asset->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge" />');
$Asset->addString('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
$Asset->addString('<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>');

// add styles
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/media.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/jscrollpane/jquery.jscrollpane.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/jssor/style.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/offers.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/popup/style.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/glass/style.css');
if ($IS_AUTH == 'Y'){
	$Asset->addCss(SITE_TEMPLATE_PATH.'/css/auth.css');
}
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/additional.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/custom/style.css');

// add scripts
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery-1.11.0.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.mousewheel.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.cookie.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jscrollpane/jquery.jscrollpane.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jscrollpane/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.core.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.utils.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.slider.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.pack.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/scrollto/jquery.scrollTo.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/offers.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/popup/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/glass/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/timer.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/custom/script.js');

?><!DOCTYPE html>
<html>
<head>
	<title><?php $APPLICATION->ShowTitle(); ?></title>
	<?php $APPLICATION->ShowHead(); ?>
	<script type="text/javascript">
	// some JS params
	var BX_COOKIE_PREFIX = 'BITRIX_SM_',
		SITE_ID = '<?=SITE_ID?>',
		SITE_DIR = '<?=str_replace('//','/',SITE_DIR);?>',
		SITE_TEMPLATE_PATH = '<?=str_replace('//','/',SITE_TEMPLATE_PATH);?>',
		SITE_CATALOG_PATH = 'catalog',
		RSGoPro_Adaptive = <?=( $adaptive=='Y' ? 'true' : 'false' )?>,
		RSGoPro_FancyCloseDelay = 1000,
		RSGoPro_FancyReloadPageAfterClose = false,
		RSGoPro_OFFERS = {},
		RSGoPro_FAVORITE = {},
		RSGoPro_COMPARE = {},
		RSGoPro_INBASKET = {},
		RSGoPro_STOCK = {},
		RSGoPro_PHONETABLET = "N";
	// messages
	BX.message({
		"RSGOPRO_JS_TO_MACH_CLICK_LIKES":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.JS_TO_MACH_CLICK_LIKES'))?>",
		"RSGOPRO_JS_COMPARE":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.RSGOPRO_JS_COMPARE'))?>",
		"RSGOPRO_JS_COMPARE_IN":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.RSGOPRO_JS_COMPARE_IN'))?>"
	});
	</script>
</head>
<body class="prop_option_<?=$prop_option?><?if($adaptive == 'Y'):?> adaptive<?endif;?>">
	<div id="panel"><?=$APPLICATION->ShowPanel()?></div>
	<div class="body"><!-- body -->
		<div class="tline"></div>
		<div id="tpanel" class="tpanel">
			<div class="centering">
				<div class="centeringin clearfix">
					<div class="authandlocation nowrap">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/header/location.php",
							Array(),
							Array("MODE"=>"html")
						);?>
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/header/auth.php",
							Array(),
							Array("MODE"=>"html")
						);?>
					</div>
					<?$APPLICATION->IncludeFile(
						SITE_DIR."include/header/topline_menu.php",
						Array(),
						Array("MODE"=>"html")
					);?>
				</div>
			</div>
		</div>
		<div id="header" class="header">
			<div class="centering">
				<div class="centeringin clearfix">
					<div class="logo column1">
						<div class="column1inner">
							<a href="<?=SITE_DIR?>">
								<?$APPLICATION->IncludeFile(
									SITE_DIR."include/company_logo.php",
									Array(),
									Array("MODE"=>"html")
								);?>
							</a>
						</div>
					</div>
					<div class="phone column1 nowrap">
						<div class="column1inner">
							<i class="icon pngicons mobile_hide"></i>
							<?$APPLICATION->IncludeFile(
								SITE_DIR."include/header/phone.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</div>
					</div>
					<div class="callback column1 nowrap">
						<div class="column1inner">
							<?$APPLICATION->IncludeFile(
								SITE_DIR."include/header/nasvyazi.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</div>
					</div>
					<div class="favorite column1 nowrap">
						<div class="column1inner">
							<?$APPLICATION->IncludeFile(
								SITE_DIR."include/header/favorite.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</div>
					</div>
					<div class="basket column1 nowrap">
						<div class="column1inner">
							<?$APPLICATION->IncludeFile(
								SITE_DIR."include/header/basket_small.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</div>
					</div>
				</div>
			</div>
			<div class="centering">
				<div class="centeringin clearfix">
					<?$APPLICATION->IncludeFile(
						SITE_DIR."include/header/menu.php",
						Array(),
						Array("MODE"=>"html")
					);?>
					<?$APPLICATION->IncludeFile(
						SITE_DIR."include/header/search_title.php",
						Array(),
						Array("MODE"=>"html")
					);?>
				</div>
			</div>
		</div>
		<?php if ($IS_MAIN == 'N'): ?>
			<div id="title" class="title">
				<div class="centering">
					<div class="centeringin clearfix">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/header/breadcrumb.php",
							Array(),
							Array("MODE"=>"html")
						);?>
						<h1 class="pagetitle"><?$APPLICATION->ShowTitle(false)?></h1>
					</div>
				</div>
			</div><!-- /title -->
		<?php endif; ?>
		<div id="content" class="content">
			<div class="centering">
				<div class="centeringin clearfix">

<?php
if ($isAjax) {
	$APPLICATION->RestartBuffer();
}

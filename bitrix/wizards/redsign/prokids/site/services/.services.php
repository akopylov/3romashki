<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = array(
	"main" => array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => array(
			"files.php", // Copy bitrix files
			"search.php", // Indexing files
			"template.php", // Install template
			"theme.php", // Install theme
			"menu.php", // Install menu
			"settings.php",
		),
	),
	"iblock" => array(
		"NAME" => GetMessage("SERVICE_IBLOCK_DEMO_DATA"),
		"STAGES" => array(
			"types.php", // IBlock types
			"action.php",
			"banners.php",
			"brands.php",
			"files.php",
			"news.php",
			"shops.php",
			"references.php", // hl
			"references2.php",
			"catalog.php", // catalog iblock import
			"catalog2.php", // offers iblock import
			"catalog3.php", // catalog binds
			"catalog4.php", // reindex
			"binds_items.php",
		),
	),
	"sale" => array(
		"NAME" => GetMessage("SERVICE_SALE_DEMO_DATA"),
		"STAGES" => array(
			"locations.php",
			"step1.php",
			"step2.php",
			"step3.php"
		),
	),
	"catalog" => array(
		"NAME" => GetMessage("SERVICE_CATALOG_SETTINGS"),
		"STAGES" => array(
			"index.php",
			"eshopapp.php",
		),
	),
	"forum" => array(
		"NAME" => GetMessage("SERVICE_FORUM")
	),
	"redsign" => array(
		"NAME" => GetMessage("SERVICE_REDSIGN"),
        "STAGES" => array(
			"daysarticle.php",
			"devcom.php",
			"devfunc.php",
			"favorite.php",
			"grupper.php",
			"location.php",
			"recaptcha.php",
			"quickbuy.php",
			"easycart.php",
		),
        "MODULE_ID" => "redsign.prokids"
	),
);

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("������");
?> 

<div style="line-height:18px;">
	<div>������ ������ ������ ������ �� ��������� ��� ��� ����������.</div>
	<div>������ � ��������-�������� ������������ ������ � ������. ����� ������������� ������ ���������� ��������-�������� ������ ������ ������� ���� �� �����.</div>
	<div><br /></div>
	
	<a name="1"></a>
	<h3>�������� ������</h3>
	<div>����� ���������������� � ������� ������ ������ �������. �� ������� ���������� ������ �������� ������ ��� ��������� ������. �������� ���� ��������, ��� ���������� ����� ��������� � ������ �� �����������.</div>
	<div><br /></div>
	
	<a name="2"></a>
	<h3>������ ������ ���������� ������</h3>
	<div>�� ��������� ������-������� �� c�������� ��������� ��������:</div>
	<div>
		<table border="0" style="margin:5px 0px;"> 
			<tbody> 
				<tr>
					<td><img width="30" height="19" style="vertical-align:middle; margin-right:5px; border:none;" src="logo_visa.gif" border="0"> Visa &nbsp; &nbsp; &nbsp;</td>
					<td><img width="30" height="19" style="vertical-align:middle; margin-right: 5px;" border="0" src="logo_mc.gif"> MasterCard &nbsp; &nbsp; &nbsp;</td>
					<td><img width="30" height="19" style="vertical-align:middle; margin-right: 5px;" border="0" src="logo_jcb.gif"> JCB &nbsp; &nbsp; &nbsp;</td>
					<td><img width="30" height="19" style="vertical-align:middle; margin-right: 5px;" border="0" src="logo_dc.gif"> DCL &nbsp; &nbsp; &nbsp;</td> </tr>
			</tbody>
		</table>
	</div>
	<div>� ������ �� ����������� ���������� ����� Visa � MasterCard ��� ���� CVV2 / CVC2.</div>
	<div>������ ������ ������������ ����� �������� ��������������� ����� ��� ����������.</div>
	<div>����������� ����� ������� ���������� 500 ������.</div>
	<div>� ������, ���� �� �������� ����� ���������� ��������� � ����� ���������� �� ����, ������� ������������ ������� ������������ �� ��� ���������� (���������) ����.</div>
	<div><br /></div>
	
	<a name="3"></a>
	<h3>����������� ������</h3>
	<div>��� ������������ ������ ������ � ������, ���� ����� ����������� �� ����������� ����. ����������� ����� ������ ��� ����������� ����� ���������� 3500 ������.</div>
	<div>��� ��������� ������ ���������� ����� ��� ���� ������������ �� �����������-��������� � ������������� ��������. ������ � ������� �������� ����, ����-������� � ���������.</div>
	<div><br /></div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>
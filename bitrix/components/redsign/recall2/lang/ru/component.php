<?
$MESS["ALFA_MSG_THEME"] = "����� ��������� ������";

$MESS["ALFA_MSG_EMPTY_REQUIRED_FIELDS"] = "�� ��������� ���� �� ������������ �����";

$MESS["ALFA_MSG_CAPTCHA_WRONG"] = "������� ������ ��� ������";
$MESS["ALFA_MSG_CAPTCHA_EMPRTY"] = "�������� ��� ������";

$MESS["ALFA_MSG_OLD_SESS"] = "���� ������ �������. ����������� ���� ��� ��� � ��������� �������.";

/* install data */
$MESS["INSTALL_EVENT_TYPE_NAME"] = "�������� �������� ������";
$MESS["INSTALL_EVENT_TYPE_DESCRIPTION"] = "#THEME# - ���� ���������
#AUTHOR# - ����� ���������
#COMPANY# - ������������ �����������
#AUTHOR_EMAIL# - E-mail ������
#AUTHOR_PHONE# - ������� ������
#AUTHOR_COMMENT# - ������ ������
#EMAIL_TO# - Email ���������� ������";

$MESS["INSTALL_EVENT_TEMPLATE_BODY"] = "����� ��������� ������

��� ������: #AUTHOR#
E-mail ������ ���������: #AUTHOR_EMAIL#
������������ �����������: #COMPANY#
������� ������: #AUTHOR_PHONE#

������ ������:
#AUTHOR_COMMENT#

------------------------------------------------------------------------------------------------------
��������� ������������� ������������� � ����� #SITE_NAME# (#SERVER_NAME#)";
<?
// Groups
$MESS['WD_POPUP_GROUP_PARAMS'] = "��������� ����������� ����";
$MESS['WD_POPUP_GROUP_POPUP'] = "��������� ������������ ����";
$MESS['WD_POPUP_GROUP_AUTOOPEN'] = "��������� ������������ ����";

/* Include areas */
$MESS['WD_POPUP_INCLUDE_PAGE'] = "��� ��������";
$MESS['WD_POPUP_INCLUDE_SECT'] = "��� �������";
$MESS['WD_POPUP_INCLUDE_FILE'] = "�� �����";
$MESS['WD_POPUP_INCLUDE_AREA_FILE_SHOW'] = "���������� ���������� �������";
$MESS['WD_POPUP_INCLUDE_AREA_FILE_SUFFIX'] = "������� ����� ����� ���������� �������";
$MESS['WD_POPUP_INCLUDE_AREA_FILE_RECURSIVE'] = "����������� ����������� ���������� �������� ��������";
$MESS['WD_POPUP_INCLUDE_EDIT_TEMPLATE'] = "������ ������� �� ���������";
$MESS['WD_POPUP_INCLUDE_PATH'] = "���� � ����� �������";

/* Popup */
$MESS['WD_POPUP_ID'] = "ID ���������� ���� (������ ���� ��������� � �������� ��������)";
$MESS['WD_POPUP_NAME'] = "��������� ���������� ����";
$MESS['WD_POPUP_WIDTH'] = "������ ���������� ���� (� ��������)";
$MESS['WD_POPUP_CLOSE'] = "������� ������ ��������";
$MESS['WD_POPUP_APPEND_TO_BODY'] = "���������� ���� � BODY";
$MESS['WD_POPUP_DISPLAY_NONE'] = "�������� ����";
$MESS['WD_POPUP_ANIMATION'] = "������ ��������";
$MESS['WD_POPUP_CALLBACK_INIT'] = "JS-������� ��� �������� ������������ ����";
$MESS['WD_POPUP_CALLBACK_OPEN'] = "JS-������� ��� ������ �������� ������������ ����";
$MESS['WD_POPUP_CALLBACK_SHOW'] = "JS-������� ��� ������ �������� ������������ ����";
$MESS['WD_POPUP_CALLBACK_CLOSE'] = "JS-������� ��� �������� ������������ ����";
$MESS['WD_POPUP_CLASSES'] = "CSS-������ ������������ ����";
$MESS['WD_POPUP_LINK_SHOW'] = "���������� ������ �������� ������������ ����";
$MESS['WD_POPUP_LINK_TO'] = "jQuery-�������� ��� ����������� ������";
$MESS['WD_POPUP_LINK_TEXT'] = "����� ������";
	$MESS['WD_POPUP_LINK_TEXT_DEFAULT'] = "�������";
$MESS['WD_POPUP_LINK_HIDDEN'] = "������� ������";
$MESS['WD_POPUP_CLOSE_TITLE'] = "����� ������ ��������";
$MESS['WD_POPUP_FIXED'] = "������������� ����";
$MESS['WD_POPUP_MIDDLE'] = "����������� �� ��������� �� ������";
$MESS['WD_POPUP_AUTOOPEN'] = "������������ ������";
$MESS['WD_POPUP_AUTOOPEN_DELAY'] = "�������� ����� ���������, ��";
$MESS['WD_POPUP_AUTOOPEN_ONCE'] = "���������� ������ ���� ��� ��� ������������";
$MESS['WD_POPUP_AUTOOPEN_TERM'] = "���� �������� ���������� �� �������� ���� ������������� (����)";
$MESS['WD_POPUP_AUTOOPEN_PATH'] = "��������� ������ �����";
$MESS['WD_POPUP_AUTOOPEN_FULLHIDE'] = "�������� ���������� ������������ ���� � �������� ���� ��������, ���� ��� �� ����� �������� � ��������� ���";
?>
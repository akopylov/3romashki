<?
$MESS["SONET_C30_T_FILTER_TITLE"] = "Filter";
$MESS["SONET_C30_T_FILTER_CREATED_BY"] = "Autor";
$MESS["SONET_C30_T_FILTER_GROUP"] = "Gruppe";
$MESS["SONET_C30_T_FILTER_USER"] = "Empf�nger";
$MESS["SONET_C30_T_FILTER_DATE"] = "Datum";
$MESS["SONET_C30_T_SHOW_HIDDEN"] = "Ausgeblendete Kategorien anzeigen";
$MESS["SONET_C30_T_SUBMIT"] = "Ausw�hlen";
$MESS["SONET_C30_T_RESET"] = "Zur�cksetzen";
$MESS["SONET_C30_PRESET_FILTER_ALL"] = "Alle Meldungen";
$MESS["SONET_C30_SMART_EXPERT_MODE"] = "Aufgaben ausblenden";
$MESS["SONET_C30_F_EXPERT_MODE_IMAGENAME"] = "de";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TITLE"] = "Modus mit ausgeblendeten Aufgaben";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TEXT1"] = "Wir haben f�r Sie den Modus mit ausgeblendeten Aufgaben aktiviert, weil Sie Aufgaben oft benutzen.<br/>Ihre Aufgaben werden nicht mehr im Activity Stream sichtbar sein. Sie werden darauf direkt im Bereich Aufgaben zugreifen k�nnen.";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TEXT2"] = "Sie k�nnen diesen Modus jederzeit im Activity Stream deaktivieren.";
$MESS["SONET_C30_SMART_FOLLOW_HINT"] = "Der Modus \"Smart verfolgen\" ist aktiv. Nur die Beitr�ge, in denen Sie Autor oder Empf�nger sind bzw. erw�hnt werden, werden nach oben verschoben. Sie verfolgen automatisch jeden Beitrag, den Sie kommentiert haben.";
$MESS["SONET_C30_T_FILTER_COMMENTS"] = "Kommentare einbeziehen";
$MESS["SONET_C30_F_DIALOG_CLOSE_BUTTON"] = "Schlie�en";
$MESS["SONET_C30_EXPERT_MODE_HINT"] = "Der Modus \"Aufgaben ausblenden\" ist jetzt aktiviert. Ihre Aufgaben werden nicht mehr im Activity Stream sichtbar sein. Sie werden darauf direkt im Bereich Aufgaben zugreifen k�nnen.<br /><br />Im Instant Messenger werden Sie nach wie vor die Aufgabenbenachrichtigungen bekommen.";
$MESS["SONET_C30_EXTRANET_ROOT"] = "Extranet";
$MESS["SONET_C30_T_FILTER_TO"] = "An";
$MESS["SONET_C30_SMART_FOLLOW"] = "Modus \"Smart verfolgen\"";
$MESS["SONET_C30_F_DIALOG_READ_BUTTON"] = "Habe gelesen";
?>
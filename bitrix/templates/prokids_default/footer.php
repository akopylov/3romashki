<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_SERVER['HTTP_X_FANCYBOX']) || isset($_REQUEST['AJAX_CALL']) && 'Y' == $_REQUEST['AJAX_CALL'];

if ($isAjax) {
	die();
}

?>

				</div>
			</div>
		</div><!-- /content -->
</div>
	</div><!-- /body -->
	<script type="text/javascript">RSGoPro_SetSet();</script>
<div class="filterFon">

<? if(!defined("basket")){ ?>
	<div id="footer" class="footer"><!-- footer -->
        <div class="tline"></div>
		<div class="centering">
			<div class="centeringin line1 clearfix">
				<div class="block one">
					<div class="logo">
						<a href="<?=SITE_DIR?>">
							<?$APPLICATION->IncludeFile(
								SITE_DIR."include/company_logo.php",
								Array(),
								Array("MODE"=>"html")
							);?>
						</a>
					</div>
					<div class="contacts clearfix">


						<? /* ?><div class="phone2">

							<div class="phone">
								<?$APPLICATION->IncludeFile(
									SITE_DIR."include/footer/phone2.php",
									Array(),
									Array("MODE"=>"html")
								);?>
							</div>
						</div> <? */ ?>

					</div>
				</div>
				<div class="block two" style="min-height: 10px;">
                    <div class="contacts clearfix" style="margin-top: 0px;">
                        <div class="phone1">
                            <div class="phone">
                                <?$APPLICATION->IncludeFile(
                                    SITE_DIR."include/footer/phone1.php",
                                    Array(),
                                    Array("MODE"=>"html")
                                );?>
                            </div>
                        </div>
                    </div>
					<?/*$APPLICATION->IncludeFile(
						SITE_DIR."include/footer/catalog_menu.php",
						Array(),
						Array("MODE"=>"html")
					);*/?>
				</div>
				<div class="block three">
					<?$APPLICATION->IncludeFile(
						SITE_DIR."include/footer/menu.php",
						Array(),
						Array("MODE"=>"html")
					);?>
				</div>
				<div class="block four">
					<div class="sovservice">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/footer/socservice.php",
							Array(),
							Array("MODE"=>"html")
						);?>
					</div>
					<? /* ?><div class="subscribe">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/footer/subscribe.php",
							Array(),
							Array("MODE"=>"html")
						);?>
					</div> <? */ ?>
				</div>
			</div>
		</div>

		<div class="line2">
			<div class="centering">
				<div class="centeringin clearfix">
					<div class="sitecopy">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/footer/law.php",
							Array(),
							Array("MODE"=>"html")
						);?>
					</div>
					<div class="developercopy">
                        <?$APPLICATION->IncludeFile(
                            SITE_DIR."include/footer/developercopy.php",
                            Array(),
                            Array("MODE"=>"html")
                        );?>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /footer -->
	<?$APPLICATION->IncludeFile(
		SITE_DIR."include/footer/easycart.php",
		Array(),
		Array("MODE"=>"html")
	);?>
	<? }else{ ?>
	<div class="footerBasket">
        <div class="centering">
            <a href="tel:88003506545">8-800-350-65-45</a>
            <a href="tel:84872384722">8-4872-38-47-22</a>
            <a href="tel:89038404722">8-903-840-47-22</a>
        </div>
    </div>
    <? } ?>
    <? /* ?>
	<div id="fixedcomparelist">
		<?$APPLICATION->IncludeFile(
			SITE_DIR."include/footer/compare.php",
			Array(),
			Array("MODE"=>"html")
		);?>
	</div>
	<? */ ?>
</div>
<? /* ?>
<?$APPLICATION->IncludeComponent(
	"webdebug:popup_window",
	".default",
	array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "popup",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/img.l.php",
		"POPUP_ANIMATION" => "fadeAndPop",
		"POPUP_APPEND_TO_BODY" => "Y",
		"POPUP_AUTOOPEN" => "Y",
		"POPUP_AUTOOPEN_DELAY" => "5000",
		"POPUP_AUTOOPEN_FULLHIDE" => "N",
		"POPUP_AUTOOPEN_ONCE" => "Y",
		"POPUP_AUTOOPEN_PATH" => "N",
		"POPUP_AUTOOPEN_TERM" => "3",
		"POPUP_CALLBACK_CLOSE" => "",
		"POPUP_CALLBACK_INIT" => "",
		"POPUP_CALLBACK_OPEN" => "",
		"POPUP_CALLBACK_SHOW" => "",
		"POPUP_CLASSES" => array(
			0 => "wd_popup_style_01",
			1 => "",
		),
		"POPUP_CLOSE" => "Y",
		"POPUP_CLOSE_TITLE" => "",
		"POPUP_DISPLAY_NONE" => "N",
		"POPUP_FIXED" => "Y",
		"POPUP_ID" => "BabNG",
		"POPUP_LINK_HIDDEN" => "N",
		"POPUP_LINK_SHOW" => "N",
		"POPUP_LINK_TEXT" => "�������",
		"POPUP_MIDDLE" => "Y",
		"POPUP_NAME" => "",
		"POPUP_WIDTH" => "",
		"COMPONENT_TEMPLATE" => ".default",
		"POPUP_LINK_TO" => ""
	),
	false
);
?>    <? */ ?>


<? if(isset($_REQUEST["dev"])){ ?>
	<div class="my_youtube_block">
 	<a href="https://youtube.com/channel/UCI48b2Ij0C7QHoSbJhw3eLg" target="_blank" class="my_youtube">��� YouTube �����</a>
 	</div><? } ?>


<link href="/css/a.kopylov.css" type="text/css"  rel="stylesheet" />
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter5453968 = new Ya.Metrika2({
                    id:5453968,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/5453968" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- BEGIN JIVOSITE CODE  -->

<script type='text/javascript'>
(function(){ var widget_id = 'pavKtuPw1q';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>

<!--  END JIVOSITE CODE -->
<noindex>
<a class="AMetrika" rel="nofollow" onclick="yaCounter5453968.reachGoal('order_sent'); ga(�send', �event', �order�, �sent�); return false;"></a>
</noindex>
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">

<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?cbk_code=e4642f1fbad602d887bc13687e8284d1" charset="UTF-8" async></script>

</body>
</html>
<?$APPLICATION->IncludeFile(
    SITE_DIR."meta/crutch.meta.php",
    Array(),
    Array("MODE"=>"php")
);?>
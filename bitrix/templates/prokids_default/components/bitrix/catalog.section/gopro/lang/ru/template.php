<?
$MESS['CATALOG_QUANTITY'] = '����������';
$MESS['CATALOG_QUANTITY_FROM_TO'] = '�� #FROM# �� #TO#';
$MESS['CATALOG_QUANTITY_FROM'] = '�� #FROM#';
$MESS['CATALOG_QUANTITY_TO'] = '�� #TO#';
$MESS['CATALOG_PRICE_VAT'] = '� ���';
$MESS['CATALOG_PRICE_NOVAT'] = '��� ���';
$MESS['CATALOG_VAT'] = '���';
$MESS['CATALOG_NO_VAT'] = '�� ����������';
$MESS['CATALOG_VAT_INCLUDED'] = '��� ������� � ����';
$MESS['CATALOG_VAT_NOT_INCLUDED'] = '��� �� ������� � ����';
$MESS['CT_BCE_QUANTITY'] = '����������';
$MESS['CT_BCE_CATALOG_BUY'] = '������';
$MESS['CT_BCE_CATALOG_ADD'] = '� �������';
$MESS['CT_BCE_CATALOG_COMPARE'] = '���������';
$MESS['CT_BCE_CATALOG_NOT_AVAILABLE'] = '��� �� ������';
$MESS['OSTATOK'] = '�������';
$MESS['COMMENTARY'] = '�����������';
$MESS['ECONOMY_INFO'] = '(�������� � ���� - #ECONOMY#)';
$MESS['FULL_DESCRIPTION'] = '������ ��������';
$MESS['CT_BCE_CATALOG_TITLE_ERROR'] = '������';
$MESS['CT_BCE_CATALOG_TITLE_BASKET_PROPS'] = '�������� ������, ����������� � �������';
$MESS['CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR'] = '����������� ������ ��� ���������� ������ � �������';
$MESS['CT_BCE_CATALOG_BTN_SEND_PROPS'] = '�������';
$MESS['CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'] = '�������';

$MESS['ARTICLE'] = '�������';
$MESS['GOPRO.MORE'] = '�����';
$MESS['INBASKET'] = '� �������';
$MESS['INBASKET_TITLE'] = '������� � �������';
$MESS['ADD2COMPARE'] = '�������� � ���������';
$MESS['FAVORITE'] = '� ���������';
$MESS['NOPRICESS'] = '���� �� �������';

$MESS['GOPRO_TH_PRODUCT'] = '�����';
$MESS['GOPRO_TH_QUANTITY'] = '�������';
$MESS['GOPRO_TH_ZAKAZ'] = '��������';

$MESS['GOPRO_QUANTITY_ISSET'] = '����';
$MESS['GOPRO_QUANTITY_LOW'] = '����';
$MESS['GOPRO_QUANTITY_EMPTY'] = '���';

$MESS['ADD2BASKET'] = '�������� � �������';

$MESS['EMAIL2FRIEND'] = '���������� � ������';
$MESS['YSHARE'] = '����������...';
$MESS['YSHARE2'] = '���������� � ��������';

$MESS['ERROR_EMPTY_ITEMS'] = '� ������ ������� ������ �����������';

$MESS['AJAXPAGES_LOAD_MORE'] = '��������� ���';

// QB and DA2
$MESS['QB_AND_DA2_DAY'] = '��.';
$MESS['QB_AND_DA2_HOUR'] = '���.';
$MESS['QB_AND_DA2_MIN'] = '���.';
$MESS['QB_AND_DA2_SEC'] = '���.';
$MESS['QB_AND_DA2_QUANTITY'] = '��������';
$MESS['QB_AND_DA2_PRODANO'] = '�������';
$MESS['QB_AND_DA2_SHT'] = '��.';
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if(!\Bitrix\Main\Loader::includeModule('redsign.devfunc'))
	return;

$this->setFrameMode(true);

$arParams['CACHE_TYPE'] = "N";
$arParams['CACHE_GROUPS'] = "N";

if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) )
{
	$ELEMENT_ID = IntVal($_REQUEST['element_id']);
	if($_REQUEST['AJAX_CALL']=='Y' && $_REQUEST['action']=='rsgppopup' && $ELEMENT_ID>0){
		// +++++++++++++++++++++++++++++++ get element popup +++++++++++++++++++++++++++++++ //
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		if($ELEMENT_ID<1)
		{
			$arJson = array( 'TYPE' => 'ERROR', 'MESSAGE' => 'Element id is empty' );
			echo json_encode($arJson);
			die();
		}
		$ElementID = $APPLICATION->IncludeComponent(
			'bitrix:catalog.element',
			'popup',
			array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
				'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
				'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
				'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
				'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
				'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
				'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'SET_TITLE' => $arParams['SET_TITLE'],
				'SET_STATUS_404' => $arParams['SET_STATUS_404'],
				'PRICE_CODE' => $arParams['PRICE_CODE'],
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
				'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
				'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
				'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
				'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
				'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
				'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
				'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
				'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
				'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],
				'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
				'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
				'OFFERS_PROPERTY_CODE' => $arParams['DETAIL_OFFERS_PROPERTY_CODE'],
				'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
				'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
				'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
				'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
				'ELEMENT_ID' => $ELEMENT_ID,
				'ELEMENT_CODE' => '',
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
				'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
				'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
				// fix
				'USE_COMPARE' => $arParams['USE_COMPARE'],
				// goPro params
				'PROP_MORE_PHOTO' => $arParams['PROP_MORE_PHOTO'],
				'HIGHLOAD' => $arParams['HIGHLOAD'],
				'PROP_ARTICLE' => $arParams['PROP_ARTICLE'],
				'PROP_ACCESSORIES' => $arParams['PROP_ACCESSORIES'],
				'USE_FAVORITE' => $arParams['USE_FAVORITE'],
				'USE_SHARE' => $arParams['USE_SHARE'],
				'PROP_SKU_MORE_PHOTO' => $arParams['PROP_SKU_MORE_PHOTO'],
				'PROP_SKU_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
				'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
				'PROPS_ATTRIBUTES_COLOR' => $arParams['PROPS_ATTRIBUTES_COLOR'],
				// store
				'USE_STORE' => $arParams['USE_STORE'],
				'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
				'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
				'MAIN_TITLE' => $arParams['MAIN_TITLE'],
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
		die();
	} elseif($arParams['USE_COMPARE']=='Y' && $_REQUEST['AJAX_CALL']=='Y' && ($_REQUEST['action']=='ADD_TO_COMPARE_LIST' || $_REQUEST['action']=='DELETE_FROM_COMPARE_LIST') )
	{
		// +++++++++++++++++++++++++++++++ add2compare +++++++++++++++++++++++++++++++ //
		global $APPLICATION,$JSON;
		$APPLICATION->IncludeComponent(
			'bitrix:catalog.compare.list',
			'json',
			Array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'NAME' => $arParams['COMPARE_NAME'],
				'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
				'COMPARE_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
				'IS_AJAX_REQUEST' => 'Y',
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
		$APPLICATION->RestartBuffer();
		if(SITE_CHARSET != 'utf-8')
		{
			$data = $APPLICATION->ConvertCharsetArray($JSON, SITE_CHARSET, 'utf-8');
			$json_str_utf = json_encode($data);
			$json_str = $APPLICATION->ConvertCharset($json_str_utf, 'utf-8', SITE_CHARSET);
			echo $json_str;
		} else {
			echo json_encode( $JSON );
		}
		die();
	} elseif($_REQUEST['AJAX_CALL']=='Y' && $_REQUEST['action']=='add2favorite' && $ELEMENT_ID>0)
	{
		// +++++++++++++++++++++++++++++++ add2favorite +++++++++++++++++++++++++++++++ //
		global $APPLICATION,$JSON;
		$res = RSFavoriteAddDel($ELEMENT_ID);
		$APPLICATION->RestartBuffer();
		$APPLICATION->IncludeComponent('redsign:favorite.list','json',array());
		$APPLICATION->RestartBuffer();
		if($res==2)
		{
			$arJson = array('TYPE'=>'OK','MESSAGE'=>'Element add2favorite','ACTION'=>'ADD','HTMLBYID'=>$JSON['HTMLBYID']);
		} elseif($res==1) {
			$arJson = array('TYPE'=>'OK','MESSAGE'=>'Element removed from favorite','ACTION'=>'REMOVE','HTMLBYID'=>$JSON['HTMLBYID']);
		} else {
			$arJson = array('TYPE'=>'ERROR','MESSAGE'=>'Bad request');
		}
		echo json_encode($arJson);
		die();
	} elseif($_REQUEST['AJAX_CALL']=='Y' && $_REQUEST['action']=='add2basket')
	{
		// +++++++++++++++++++++++++++++++ add2basket +++++++++++++++++++++++++++++++ //
		global $APPLICATION,$JSON;
		$ProductID = IntVal($_REQUEST[$arParams["PRODUCT_ID_VARIABLE"]]);
		$QUANTITY = doubleval($_REQUEST[$arParams["PRODUCT_QUANTITY_VARIABLE"]]);
		$params = Array(
			'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
			'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
			'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
			'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
			'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
		);
		$restat = RSDF_EasyAdd2Basket($ProductID,$QUANTITY,$params);
		$APPLICATION->RestartBuffer();
		$APPLICATION->IncludeComponent('bitrix:sale.basket.basket.line','json',array());
		$APPLICATION->RestartBuffer();
		if(SITE_CHARSET != 'utf-8')
		{
			$data = $APPLICATION->ConvertCharsetArray($JSON, SITE_CHARSET, 'utf-8');
			$json_str_utf = json_encode($data);
			$json_str = $APPLICATION->ConvertCharset($json_str_utf, 'utf-8', SITE_CHARSET);
			echo $json_str;
		} else {
			echo json_encode( $JSON );
		}
		die();
	} elseif($_REQUEST['AJAX_CALL']=='Y' && $_REQUEST['action']=='get_element_json' && $ELEMENT_ID>0)
	{
		// +++++++++++++++++++++++++++++++ get element json +++++++++++++++++++++++++++++++ //
		global $APPLICATION,$JSON;
		$APPLICATION->RestartBuffer();
		if($ELEMENT_ID<1)
		{
			$arJson = array( 'TYPE' => 'ERROR', 'MESSAGE' => 'Element id is empty' );
			echo json_encode($arJson);
			die();
		}
		$ElementID=$APPLICATION->IncludeComponent(
			'bitrix:catalog.element',
			'json',
			Array(
				'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
				'IBLOCK_ID' => $arParams['IBLOCK_ID'],
				'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
				'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
				'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
				'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
				'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
				'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
				'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'CACHE_TYPE' => $arParams['CACHE_TYPE'],
				'CACHE_TIME' => $arParams['CACHE_TIME'],
				'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
				'SET_TITLE' => $arParams['SET_TITLE'],
				'SET_STATUS_404' => $arParams['SET_STATUS_404'],
				'PRICE_CODE' => $arParams['PRICE_CODE'],
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
				'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
				'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
				'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
				'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
				'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
				'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
				'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
				'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
				'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],
				'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
				'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
				'OFFERS_PROPERTY_CODE' => $arParams['DETAIL_OFFERS_PROPERTY_CODE'],
				'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
				'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
				'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
				'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
				'ELEMENT_ID' => $ELEMENT_ID,
				'ELEMENT_CODE' => '',
				'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
				'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
				'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
				'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
				'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
				'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
				// goPro params
				'PROP_MORE_PHOTO' => $arParams['PROP_MORE_PHOTO'],
				'HIGHLOAD' => $arParams['HIGHLOAD'],
				'PROP_ARTICLE' => $arParams['PROP_ARTICLE'],
				'PROP_ACCESSORIES' => $arParams['PROP_ACCESSORIES'],
				'USE_FAVORITE' => $arParams['USE_FAVORITE'],
				'USE_SHARE' => $arParams['USE_SHARE'],
				'SHOW_ERROR_EMPTY_ITEMS' => $arParams['SHOW_ERROR_EMPTY_ITEMS'],
				'PROP_SKU_MORE_PHOTO' => $arParams['PROP_SKU_MORE_PHOTO'],
				'PROP_SKU_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
				'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
				// store
				'USE_STORE' => $arParams['USE_STORE'],
				'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
				'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
				'MAIN_TITLE' => $arParams['MAIN_TITLE'],
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		);
		$APPLICATION->RestartBuffer();
		if (SITE_CHARSET != 'utf-8')
		{
			$data = $APPLICATION->ConvertCharsetArray($JSON, SITE_CHARSET, 'utf-8');
			$json_str_utf = json_encode($data);
			$json_str = $APPLICATION->ConvertCharset($json_str_utf, 'utf-8', SITE_CHARSET);
			echo $json_str;
		} else {
			echo json_encode( $JSON );
		}
		die();
	}
}
$arSelect = Array(
    "ID",
    "NAME",
    "DATE_ACTIVE_FROM",
);
$arFilter = Array(
    "IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']),
    "ACTIVE_DATE"=>"Y",
    "ACTIVE"=>"Y",
    "CODE" => $arResult['VARIABLES']['ELEMENT_CODE'],
    "SECTION_CODE" => $arResult['VARIABLES']['SECTION_CODE']
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
if($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
}else{
    $arResult['VARIABLES']['ELEMENT_CODE'] = $arResult['VARIABLES']['ELEMENT_CODE'].'_erroro';
}

?><?$ElementID = $APPLICATION->IncludeComponent(
	'bitrix:catalog.element',
	'gopro',
	array(
		'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
		'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
		'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
		'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
		'BASKET_URL' => $arParams['BASKET_URL'],
		'SET_CANONICAL_URL' => $arParams['DETAIL_SET_CANONICAL_URL'],
		'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
		'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
		'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
		'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
		'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
		'CACHE_TYPE' => $arParams['CACHE_TYPE'],
		'CACHE_TIME' => $arParams['CACHE_TIME'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
		'SET_TITLE' => $arParams['SET_TITLE'],
		'SET_STATUS_404' => $arParams['SET_STATUS_404'],
		'PRICE_CODE' => $arParams['PRICE_CODE'],
		'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
		'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
		'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
		'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
		'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
		'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
		'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
		'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
		'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
		'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
		'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
		'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],

		'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
		'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
		'OFFERS_PROPERTY_CODE' => $arParams['DETAIL_OFFERS_PROPERTY_CODE'],
		'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
		'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
		'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
		'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],

		'LIST_OFFERS_FIELD_CODE' => $arParams['LIST_OFFERS_FIELD_CODE'],
		'LIST_OFFERS_PROPERTY_CODE' => $arParams['LIST_OFFERS_PROPERTY_CODE'],
		'LIST_OFFERS_LIMIT' => $arParams['LIST_OFFERS_LIMIT'],

		'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
		'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
		'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
		'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
		'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
		'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
		'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
		'USE_COMPARE' => $arParams['USE_COMPARE'],
		// goPro params
		'PROP_MORE_PHOTO' => $arParams['PROP_MORE_PHOTO'],
		'HIGHLOAD' => $arParams['HIGHLOAD'],
		'PROP_ARTICLE' => $arParams['PROP_ARTICLE'],
		'USE_FAVORITE' => $arParams['USE_FAVORITE'],
		'USE_SHARE' => $arParams['USE_SHARE'],
		'OFF_MEASURE_RATION' => $arParams['OFF_MEASURE_RATION'],
		'PROP_SKU_MORE_PHOTO' => $arParams['PROP_SKU_MORE_PHOTO'],
		'PROP_SKU_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
		'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
		'PROPS_ATTRIBUTES_COLOR' => $arParams['PROPS_ATTRIBUTES_COLOR'],
		// store
		'STORES_TEMPLATE' => $arParams['STORES_TEMPLATE'],
		'USE_STORE' => $arParams['USE_STORE'],
		"STORE_PATH" => $arParams['STORE_PATH'],
		'MAIN_TITLE' => $arParams['MAIN_TITLE'],
		'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
		'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
		"STORES" => $arParams['STORES'],
		"SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
		"SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
		"USER_FIELDS" => $arParams['USER_FIELDS'],
		"FIELDS" => $arParams['FIELDS'],
		// element
		'PROPS_TABS' => $arParams['PROPS_TABS'],
		'USE_CHEAPER' => $arParams['USE_CHEAPER'],
		'USE_BLOCK_MODS' => $arParams['USE_BLOCK_MODS'],
		'DETAIL_TABS_VIEW' => $arParams['DETAIL_TABS_VIEW'],
		'SHOW_PREVIEW_TEXT' => $arParams['SHOW_PREVIEW_TEXT'],
		// seo
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
	),
	$component
);?><?

?><div class="clear"></div><?
?><div class="clear"></div><?
if($ElementID > 0){
?><div class="detailtabs <?if($arParams['DETAIL_TABS_VIEW']=='anchor'):?>anchor<?else:?>tabs<?endif;?>"><?
	?><div class="headers clearfix"><?
		$APPLICATION->ShowViewContent('TABS_HTML_HEADERS');
		if( $arParams['USE_REVIEW']=='Y' && IsModuleInstalled('forum') )
		{
			?><a class="switcher" href="#review"><?=GetMessage('TABS_REVIEW')?></a><?
		}
	?></div><?
	?><div class="contents"><?
		$APPLICATION->ShowViewContent('TABS_HTML_CONTENTS');
		if( $arParams['USE_REVIEW']=='Y' && IsModuleInstalled('forum') ) {
			?><div class="content selected review" id="review"><?
				?><a class="switcher" href="#review"><?=GetMessage('TABS_REVIEW')?></a><?
				?><div class="contentbody clearfix"><?
					?><a class="add2review btn3" href="#addreview"><?=GetMessage('ADD_REVIEW')?></a><?
					?><?$APPLICATION->IncludeComponent(
						'bitrix:forum.topic.reviews',
						'gopro',
						Array(
							"URL_TEMPLATES_DETAIL" => $arParams["REVIEWS_URL_TEMPLATES_DETAIL"],
							"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
							"FORUM_ID" => $arParams["FORUM_ID"],
							'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
							'IBLOCK_ID' => $arParams['IBLOCK_ID'],
							'ELEMENT_ID' => $ElementID,
							"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
							"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
							"PAGE_NAVIGATION_TEMPLATE" => $arParams["PAGE_NAVIGATION_TEMPLATE"],
							"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
							"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							'AJAX_POST' => 'N',
							'AJAX_MODE' => 'N',
						),
						$component,
						array('HIDE_ICONS' => 'Y')
					);?><?
				?></div><?
			?></div><?
		}
	?></div><!-- /contents --><?
?></div>

<?}else{ ?> <br /> <? }
// MODS
if($arParams['USE_BLOCK_MODS']=='Y') {
	$obCache = new CPHPCache();
	if($obCache->InitCache(36000, serialize($arFilter) ,'/iblock/catalog')) {
		$arCurIBlock = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {
		$arCurIBlock = CIBlockPriceTools::GetOffersIBlock($arParams['IBLOCK_ID']);
		if(defined('BX_COMP_MANAGED_CACHE')) {
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache('/iblock/catalog');
			if($arCurIBlock) {
				$CACHE_MANAGER->RegisterTag('iblock_id_'.$arParams['IBLOCK_ID']);
			}
			$CACHE_MANAGER->EndTagCache();
		} else {
			if(!$arCurIBlock) {
				$arCurIBlock = array();
			}
		}
		$obCache->EndDataCache($arCurIBlock);
	}
	?><div class="mods"><!-- mods --><?
		global $modFilter,$JSON;
		$modFilter = array('PROPERTY_'.$arCurIBlock['OFFERS_PROPERTY_ID']=>$ElementID);
		?><h3 class="title2"><?=$arParams['MODS_BLOCK_NAME']?></h3><?
global $alfaCTemplate, $alfaCSortType, $alfaCSortToo, $alfaCOutput;
$APPLICATION->IncludeComponent('redsign:catalog.sorter', 'gopro', array(
	'ALFA_ACTION_PARAM_NAME' => 'alfaction',
	'ALFA_ACTION_PARAM_VALUE' => 'alfavalue',
	'ALFA_CHOSE_TEMPLATES_SHOW' => 'Y',
	'ALFA_CNT_TEMPLATES' => '3',
	'ALFA_DEFAULT_TEMPLATE' => 'table',
	'ALFA_CNT_TEMPLATES_0' => $arParams['SORTER_TEMPLATE_NAME_3'],
	'ALFA_CNT_TEMPLATES_NAME_0' => 'table',
	'ALFA_CNT_TEMPLATES_1' => $arParams['SORTER_TEMPLATE_NAME_2'],
	'ALFA_CNT_TEMPLATES_NAME_1' => 'gallery',
	'ALFA_CNT_TEMPLATES_2' => $arParams['SORTER_TEMPLATE_NAME_1'],
	'ALFA_CNT_TEMPLATES_NAME_2' => 'showcase',
	'ALFA_SORT_BY_SHOW' => 'N',
	'ALFA_OUTPUT_OF_SHOW' => 'N',
	'AJAXPAGESID' => 'ajaxpages_mods',
	),
	false
);

		?><div class="clear"></div><?
		?><div id="ajaxpages_mods" class="ajaxpages_gmci"><!-- ajaxpages_gmci --><?
global $APPLICATION,$JSON;
$IS_SORTERCHANGE = 'N';
if($_REQUEST['AJAX_CALL']=='Y' && $_REQUEST['sorterchange']=='ajaxpages_mods') {
	$IS_SORTERCHANGE = 'Y';
	$JSON['TYPE'] = 'OK';
}
$IS_AJAXPAGES = 'N';
if($_REQUEST['ajaxpages']=='Y' && $_REQUEST['ajaxpagesid']=='ajaxpages_mods') {
	$IS_AJAXPAGES = 'Y';
	$JSON['TYPE'] = 'OK';
}
			$APPLICATION->IncludeComponent(
				'bitrix:catalog.section',
				'gopro',
				array(
					'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
					'IBLOCK_ID' => $arCurIBlock['OFFERS_IBLOCK_ID'],
					'ELEMENT_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],//$arParams['ELEMENT_SORT_FIELD'],
					'ELEMENT_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],//$arParams['ELEMENT_SORT_ORDER'],
					'ELEMENT_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],//$arParams['ELEMENT_SORT_FIELD2'],
					'ELEMENT_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],//$arParams['ELEMENT_SORT_ORDER2'],
					'PROPERTY_CODE' => $arParams['LIST_OFFERS_PROPERTY_CODE'],
					'META_KEYWORDS' => $arParams['LIST_META_KEYWORDS'],
					'META_DESCRIPTION' => $arParams['LIST_META_DESCRIPTION'],
					'BROWSER_TITLE' => $arParams['LIST_BROWSER_TITLE'],
					'INCLUDE_SUBSECTIONS' => 'N',
					'BASKET_URL' => $arParams['BASKET_URL'],
					'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
					'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
					'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
					'FILTER_NAME' => 'modFilter',
					'CACHE_TYPE' => $arParams['CACHE_TYPE'],
					'CACHE_TIME' => $arParams['CACHE_TIME'],
					'CACHE_FILTER' => $arParams['CACHE_FILTER'],
					'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
					'SET_TITLE' => 'N',
					'SET_STATUS_404' => 'N',
					'DISPLAY_COMPARE' => 'N',
					'PAGE_ELEMENT_COUNT' => '100',
					'LINE_ELEMENT_COUNT' => $arParams['LINE_ELEMENT_COUNT'],
					'PRICE_CODE' => $arParams['PRICE_CODE'],
					'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
					'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],

					'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
					'USE_PRODUCT_QUANTITY' => $arParams['~USE_PRODUCT_QUANTITY'],
					'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['OFFERS_CART_PROPERTIES']) ? $arParams['OFFERS_CART_PROPERTIES'] : ''),
					'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
					'PRODUCT_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],

					'DISPLAY_TOP_PAGER' => 'N',
					'DISPLAY_BOTTOM_PAGER' => 'N',
					'PAGER_TITLE' => $arParams['PAGER_TITLE'],
					'PAGER_SHOW_ALWAYS' => $arParams['PAGER_SHOW_ALWAYS'],
					'PAGER_TEMPLATE' => $arParams['PAGER_TEMPLATE'],
					'PAGER_DESC_NUMBERING' => $arParams['PAGER_DESC_NUMBERING'],
					'PAGER_DESC_NUMBERING_CACHE_TIME' => $arParams['PAGER_DESC_NUMBERING_CACHE_TIME'],
					'PAGER_SHOW_ALL' => $arParams['PAGER_SHOW_ALL'],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					// ajaxpages
					'AJAXPAGESID' => 'ajaxpages_mods',
					'IS_AJAXPAGES' => $IS_AJAXPAGES,
					'IS_SORTERCHANGE' => $IS_SORTERCHANGE,
					// goPro params
					'PROP_MORE_PHOTO' => $arParams['PROP_MORE_PHOTO'],
					'HIGHLOAD' => $arParams['HIGHLOAD'],
					'PROP_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
					'PROP_ACCESSORIES' => $arParams['PROP_ACCESSORIES'],
					'USE_FAVORITE' => 'N',
					'USE_SHARE' => 'N',
					'SHOW_ERROR_EMPTY_ITEMS' => 'N',
					'OFF_MEASURE_RATION' => $arParams['OFF_MEASURE_RATION'],
					'PROP_SKU_MORE_PHOTO' => $arParams['PROP_SKU_MORE_PHOTO'],
					'PROP_SKU_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
					'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
					// store
					'USE_STORE' => $arParams['USE_STORE'],
					'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
					'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
					'MAIN_TITLE' => $arParams['MAIN_TITLE'],
					// -----
					'BY_LINK' => 'Y',
					'DONT_SHOW_LINKS' => 'Y',
					'VIEW' => $alfaCTemplate,
					'COLUMNS5' => 'Y',
				),
				$component,
				array('HIDE_ICONS'=>'Y')
			);
if($IS_AJAXPAGES=='Y' || $IS_SORTERCHANGE=='Y') {
	$APPLICATION->RestartBuffer();
	if(SITE_CHARSET!='utf-8') {
		$data = $APPLICATION->ConvertCharsetArray($JSON, SITE_CHARSET, 'utf-8');
		$json_str_utf = json_encode($data);
		$json_str = $APPLICATION->ConvertCharset($json_str_utf, 'utf-8', SITE_CHARSET);
		echo $json_str;
	} else {
		echo json_encode($JSON);
	}
	die();
}
		?></div><!-- /ajaxpages_gmci --><?
	?></div><!-- /mods --><?
	?><script>
	if( $('#ajaxpages_mods').find('.js-element').length<1 ) {
		$('.mods').hide();
	}
	</script><?
}

// bigdata
if( $arParams['USE_BLOCK_BIGDATA']=='Y' ) {
	$obCache = new CPHPCache();
	if($obCache->InitCache(36000, serialize($arFilter) ,'/iblock/catalog')) {
		$arCurIBlock = $obCache->GetVars();
	} elseif($obCache->StartDataCache()) {
		$arCurIBlock = CIBlockPriceTools::GetOffersIBlock($arParams['IBLOCK_ID']);
		if(defined('BX_COMP_MANAGED_CACHE')) {
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache('/iblock/catalog');
			if($arCurIBlock) {
				$CACHE_MANAGER->RegisterTag('iblock_id_'.$arParams['IBLOCK_ID']);
			}
			$CACHE_MANAGER->EndTagCache();
		} else {
			if(!$arCurIBlock) {
				$arCurIBlock = array();
			}
		}
		$obCache->EndDataCache($arCurIBlock);
	}
	?>
<div class="bigdata js-bigdata"><!-- /bigdata --><?
		?><h3 class="title2"><?=$arParams['BIGDATA_BLOCK_NAME']?></h3>
		<div class="clear"></div><br />
<? global $arrFliterS, $ElementDetail;
if($ElementDetail['PROPERTIES']['ELEMENT_R']['VALUE'] AND count($ElementDetail['PROPERTIES']['ELEMENT_R']['VALUE']) > 0){
    $arrFliterS['ID'] = $ElementDetail['PROPERTIES']['ELEMENT_R']['VALUE'];
}


?>
		<div id="ajaxpages_bigdata" class="ajaxpages_gmci"><!-- /ajaxpages_gmci --><?
        $APPLICATION->IncludeComponent("bitrix:catalog.section", "gopro", Array(
	"IBLOCK_TYPE" => "catalog",	// ��� ���������
		"IBLOCK_ID" => "12",	// ��������
		"SECTION_ID" => "",	// ID �������
		"SECTION_CODE" => "",	// ��� �������
		"SECTION_USER_FIELDS" => array(	// �������� �������
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "RAND",	// �� ������ ���� ��������� ��������
		"ELEMENT_SORT_ORDER" => "desc",	// ������� ���������� ���������
		"ELEMENT_SORT_FIELD2" => "shows",	// ���� ��� ������ ���������� ���������
		"ELEMENT_SORT_ORDER2" => "asc",	// ������� ������ ���������� ���������
		"FILTER_NAME" => "arrFliterS",	// ��� ������� �� ���������� ������� ��� ���������� ���������
		"INCLUDE_SUBSECTIONS" => "A",	// ���������� �������� ����������� �������
		"SHOW_ALL_WO_SECTION" => "Y",	// ���������� ��� ��������, ���� �� ������ ������
		"HIDE_NOT_AVAILABLE" => "N",	// ������, �� ��������� ��� �������
		"PAGE_ELEMENT_COUNT" => "5",	// ���������� ��������� �� ��������
		"LINE_ELEMENT_COUNT" => "3",	// ���������� ��������� ��������� � ����� ������ �������
		"PROPERTY_CODE" => array(	// ��������
			0 => "CML2_ARTICLE",
			1 => "PROP_33_WEBASYST",
			2 => "PROP_37_WEBASYST",
			3 => "PROP_16_WEBASYST",
			4 => "PROP_28_WEBASYST",
			5 => "PROP_47_WEBASYST",
			6 => "PROP_17_WEBASYST",
			7 => "PROP_20_WEBASYST",
			8 => "PROP_40_WEBASYST",
			9 => "PROP_41_WEBASYST",
			10 => "PROP_39_WEBASYST",
			11 => "PROP_50_WEBASYST",
			12 => "PROP_30_WEBASYST",
			13 => "PROP_36_WEBASYST",
			14 => "PROP_45_WEBASYST",
			15 => "PROP_32_WEBASYST",
			16 => "PROP_22_WEBASYST",
			17 => "PROP_35_WEBASYST",
			18 => "PROP_38_WEBASYST",
			19 => "PROP_29_WEBASYST",
			20 => "PROP_23_WEBASYST",
			21 => "PROP_34_WEBASYST",
			22 => "PROP_15_WEBASYST",
			23 => "PROP_46_WEBASYST",
			24 => "PROP_31_WEBASYST",
			25 => "PROP_49_WEBASYST",
			26 => "PROP_21_WEBASYST",
			27 => "PROP_42_WEBASYST",
			28 => "PROP_24_WEBASYST",
			29 => "PROP_25_WEBASYST",
			30 => "",
		),
		"OFFERS_LIMIT" => "0",	// ������������ ���������� ����������� ��� ������ (0 - ���)
		"TEMPLATE_THEME" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",	// URL, ������� �� �������� � ���������� �������
		"DETAIL_URL" => "",	// URL, ������� �� �������� � ���������� �������� �������
		"SECTION_ID_VARIABLE" => "SECTION_ID",	// �������� ����������, � ������� ���������� ��� ������
		"AJAX_MODE" => "N",	// �������� ����� AJAX
		"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
		"AJAX_OPTION_STYLE" => "N",	// �������� ��������� ������
		"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
		"CACHE_TYPE" => "A",	// ��� �����������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_GROUPS" => "N",	// ��������� ����� �������
		"SET_META_KEYWORDS" => "N",	// ������������� �������� ����� ��������
		"META_KEYWORDS" => "",	// ���������� �������� ����� �������� �� ��������
		"SET_META_DESCRIPTION" => "N",	// ������������� �������� ��������
		"META_DESCRIPTION" => "",	// ���������� �������� �������� �� ��������
		"BROWSER_TITLE" => "-",	// ���������� ��������� ���� �������� �� ��������
		"ADD_SECTIONS_CHAIN" => "N",	// �������� ������ � ������� ���������
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",	// ������������� ��������� ��������
		"SET_STATUS_404" => "N",	// ������������� ������ 404
		"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
		"PRICE_CODE" => array(	// ��� ����
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",	// ������������ ����� ��� � �����������
		"SHOW_PRICE_COUNT" => "1",	// �������� ���� ��� ����������
		"PRICE_VAT_INCLUDE" => "N",	// �������� ��� � ����
		"CONVERT_CURRENCY" => "Y",	// ���������� ���� � ����� ������
		"BASKET_URL" => "/personal/cart/",	// URL, ������� �� �������� � �������� ����������
		"ACTION_VARIABLE" => "action",	// �������� ����������, � ������� ���������� ��������
		"PRODUCT_ID_VARIABLE" => "id",	// �������� ����������, � ������� ���������� ��� ������ ��� �������
		"USE_PRODUCT_QUANTITY" => "Y",	// ��������� �������� ���������� ������
		"ADD_PROPERTIES_TO_BASKET" => "N",	// ��������� � ������� �������� ������� � �����������
		"PRODUCT_PROPS_VARIABLE" => "prop",	// �������� ����������, � ������� ���������� �������������� ������
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// ��������� ��������� � ������� ������, � ������� ��������� �� ��� ��������������
		"PRODUCT_PROPERTIES" => "",	// �������������� ������
		"PAGER_TEMPLATE" => "gopro",	// ������ ������������ ���������
		"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
		"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
		"PAGER_TITLE" => "Товары",	// �������� ���������
		"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
		"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
		"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
		"OFFERS_FIELD_CODE" => array(	// ���� �����������
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(	// �������� �����������
			0 => "PROP_30_WEBASYST",
			1 => "PROP_33_WEBASYST",
			2 => "PROP_37_WEBASYST",
			3 => "PROP_16_WEBASYST",
			4 => "PROP_28_WEBASYST",
			5 => "PROP_47_WEBASYST",
			6 => "PROP_17_WEBASYST",
			7 => "PROP_20_WEBASYST",
			8 => "PROP_40_WEBASYST",
			9 => "PROP_41_WEBASYST",
			10 => "PROP_39_WEBASYST",
			11 => "PROP_50_WEBASYST",
			12 => "PROP_36_WEBASYST",
			13 => "PROP_45_WEBASYST",
			14 => "PROP_32_WEBASYST",
			15 => "PROP_22_WEBASYST",
			16 => "PROP_35_WEBASYST",
			17 => "PROP_38_WEBASYST",
			18 => "PROP_29_WEBASYST",
			19 => "PROP_23_WEBASYST",
			20 => "PROP_46_WEBASYST",
			21 => "PROP_31_WEBASYST",
			22 => "PROP_49_WEBASYST",
			23 => "PROP_21_WEBASYST",
			24 => "PROP_34_WEBASYST",
			25 => "PROP_15_WEBASYST",
			26 => "PROP_42_WEBASYST",
			27 => "PROP_24_WEBASYST",
			28 => "PROP_25_WEBASYST",
			29 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",	// �� ������ ���� ��������� ����������� ������
		"OFFERS_SORT_ORDER" => "asc",	// ������� ���������� ����������� ������
		"OFFERS_SORT_FIELD2" => "id",	// ���� ��� ������ ���������� ����������� ������
		"OFFERS_SORT_ORDER2" => "asc",	// ������� ������ ���������� ����������� ������
		"PROP_MORE_PHOTO" => "MORE_PHOTO",	// �������� � ���. ������������� �������
		"PROP_ARTICLE" => "CML2_ARTICLE",	// �������� � ��������� ������
		"PROP_ACCESSORIES" => "ACCESSORIES",	// �������� � ��������� �����������
		"USE_FAVORITE" => "N",	// ������������ ���������
		"USE_SHARE" => "Y",	// ������������ ������������ ����������
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",	// ���������� ������, ���� ������� �� �������
		"DONT_SHOW_LINKS" => "N",	// �� ���������� ������ �� �����
		"USE_STORE" => "N",	// ���������� ���� "���������� ������ �� ������"
		"USE_MIN_AMOUNT" => "Y",	// ��������� ����� �� ���������
		"MIN_AMOUNT" => "10",	// ��������, ���� �������� ��������� "����"
		"MAIN_TITLE" => "Наличие на складах",	// ��������� �����
		"PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",	// �������� � ���. ������������� �������� �����������
		"PROP_SKU_ARTICLE" => "-",	// �������� � ��������� �������� �����������
		"PROPS_ATTRIBUTES" => array(	// �������������� ������
			0 => "PROP_30_WEBASYST",
			1 => "PROP_16_WEBASYST",
			2 => "PROP_17_WEBASYST",
			3 => "PROP_20_WEBASYST",
			4 => "PROP_22_WEBASYST",
			5 => "PROP_23_WEBASYST",
			6 => "PROP_28_WEBASYST",
			7 => "PROP_29_WEBASYST",
			8 => "PROP_31_WEBASYST",
			9 => "PROP_32_WEBASYST",
			10 => "PROP_33_WEBASYST",
			11 => "PROP_35_WEBASYST",
			12 => "PROP_36_WEBASYST",
			13 => "PROP_37_WEBASYST",
			14 => "PROP_38_WEBASYST",
			15 => "PROP_39_WEBASYST",
			16 => "PROP_40_WEBASYST",
			17 => "PROP_41_WEBASYST",
			18 => "PROP_45_WEBASYST",
			19 => "PROP_46_WEBASYST",
			20 => "PROP_47_WEBASYST",
			21 => "PROP_50_WEBASYST",
		),
		"OFFERS_CART_PROPERTIES" => "",	// �������� �����������, ����������� � �������
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// �������� ����������, � ������� ���������� ���������� ������
		"AJAXPAGESID" => "ajaxpages_main",
		"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
		"VIEW" => "showcase",
		"COLUMNS5" => "Y",
		"SET_BROWSER_TITLE" => "N",	// ������������� ��������� ���� ��������
		"USE_AUTO_AJAXPAGES" => "N",	// ������������ ������������ ��� ��������� ��������
		"PROPS_ATTRIBUTES_COLOR" => "",
		"COMPARE_PATH" => "",	// ���� � �������� ���������
		"OFF_SMALLPOPUP" => "N",
		"COMPONENT_TEMPLATE" => "light",
		"BACKGROUND_IMAGE" => "-",	// ���������� ������� �������� ��� ������� �� ��������
		"SEF_MODE" => "N",	// �������� ��������� ���
		"SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
		"USE_MAIN_ELEMENT_SECTION" => "N",	// ������������ �������� ������ ��� ������ ��������
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"OFF_MEASURE_RATION" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
		"SHOW_404" => "N",	// ����� ����������� ��������
		"MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// �� ���������� js-���������� � ����������
		"CURRENCY_ID" => "RUB",	// ������, � ������� ����� ��������������� ����
		"USE_COMPARE" => "N",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST"
	),
	false
);
		?></div><!-- /ajaxpages_gmci --><?
	?></div><!-- /bigdata --><?
}
// /bigdata

// tabs
?>
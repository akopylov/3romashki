<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	'SHOW_COUNT_LVL1' => array(
		'NAME' => GetMessage('SHOW_COUNT_LVL1'),
		'TYPE' => 'STRING',
		'DEFAULT' => '8',
	),
	'SHOW_COUNT_LVL2' => array(
		'NAME' => GetMessage('SHOW_COUNT_LVL2'),
		'TYPE' => 'STRING',
		'DEFAULT' => '11',
	),
	'BLOCK_NAME' => array(
		'NAME' => GetMessage('BLOCK_NAME'),
		'TYPE' => 'STRING',
	),
	"COUNT_MAIN" => array(
		"NAME" => GetMessage("COUNT_MAIN"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"IMAG_SHOW" => array(
		"NAME" => GetMessage("IMAG_SHOW"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
);
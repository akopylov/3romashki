<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

?><div class="search_page clearfix"><?
	?><form action="" method="get" class="form_search"><?
		?><input type="hidden" name="tags" value="<?echo $arResult['REQUEST']['TAGS']?>" /><?
		?><input type="text" class="q" name="q" value="<?=$arResult['REQUEST']['QUERY']?>" size="40" /><?
		?><input class="btn btn1" type="submit" class="search_94" value="<?=GetMessage('SEARCH_GO')?>" /><?
		?><input type="hidden" name="how" value="<?=$arResult['REQUEST']['HOW']=='d'? 'd': 'r'?>" /><?
	?></form><?
?></div><?

if($arResult['ERROR_CODE']!=0)
{
	ShowError($arResult['ERROR_TEXT']);
	?><p><?=GetMessage('SEARCH_CORRECT_AND_CONTINUE')?></p><br /><br /><?
} elseif(count($arResult['SEARCH'])>0)
{
	?><div class="spage"><?
		////////////////// IBLOCKS
		if(!empty($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"]))
		{
			foreach($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"] as $iblock_id => $arIblock)
			{
				// catalog
				if(in_array($iblock_id,$arParams['IBLOCK_ID']))
				{
					global $arrSearchFilter;
					$arIds = array();
					foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
					{
						if( $arItem['ITEM_ID']!=$arItem['ID'] && IntVal($arItem['ITEM_ID'])>0 )
						{
							$arIds[] = $arItem['ITEM_ID'];
						}
					}
					if( is_array($arIds) && count($arIds)>0 )
					{ 
						?><div class="iblock"><?
							?><div class="title"><h3><?=$arIblock["NAME"]?></h3></div><?
							//$arrSearchFilter = array('ID'=>$arIds);
							$arrSearchFilter['NAME'] = '%'.$arResult['REQUEST']['QUERY'].'%';
//						 	print_arr($arrSearchFilter);
							$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"searchpage", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "12",
		"ELEMENT_SORT_FIELD" => "SORT",
		"ELEMENT_SORT_ORDER" => "ASC",
		"PROPERTY_CODE" => array(
			0 => "BRAND",
			1 => "PROP_33_WEBASYST",
			2 => "PROP_37_WEBASYST",
			3 => "PROP_16_WEBASYST",
			4 => "PROP_28_WEBASYST",
			5 => "PROP_47_WEBASYST",
			6 => "PROP_17_WEBASYST",
			7 => "PROP_20_WEBASYST",
			8 => "PROP_40_WEBASYST",
			9 => "PROP_41_WEBASYST",
			10 => "FORUM_MESSAGE_CNT",
			11 => "PROP_39_WEBASYST",
			12 => "PROP_30_WEBASYST",
			13 => "PROP_36_WEBASYST",
			14 => "PROP_45_WEBASYST",
			15 => "PROP_32_WEBASYST",
			16 => "PROP_22_WEBASYST",
			17 => "PROP_38_WEBASYST",
			18 => "PROP_29_WEBASYST",
			19 => "PROP_23_WEBASYST",
			20 => "PROP_34_WEBASYST",
			21 => "PROP_15_WEBASYST",
			22 => "COLOR",
			23 => "PROP_46_WEBASYST",
			24 => "PROP_31_WEBASYST",
			25 => "",
		),
		"META_KEYWORDS" => "",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "-",
		"INCLUDE_SUBSECTIONS" => "Y",
		"BASKET_URL" => "",
		"ACTION_VARIABLE" => "",
		"PRODUCT_ID_VARIABLE" => "",
		"SECTION_ID_VARIABLE" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"FILTER_NAME" => "arrSearchFilter",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"DISPLAY_COMPARE" => "N",
		"PAGE_ELEMENT_COUNT" => "15",
		"LINE_ELEMENT_COUNT" => "",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "N",
		"PRICE_VAT_INCLUDE" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "gopro",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
		"PAGER_SHOW_ALL" => "N",
		"OFFERS_CART_PROPERTIES" => array(
			0 => "COLOR",
			1 => "PROP_30_WEBASYST",
			2 => "FASAD",
			3 => "PROP_33_WEBASYST",
			4 => "PROP_37_WEBASYST",
			5 => "PROP_16_WEBASYST",
			6 => "PROP_28_WEBASYST",
			7 => "PROP_47_WEBASYST",
			8 => "PROP_17_WEBASYST",
			9 => "PROP_20_WEBASYST",
			10 => "PROP_40_WEBASYST",
			11 => "PROP_41_WEBASYST",
			12 => "PROP_39_WEBASYST",
			13 => "PROP_50_WEBASYST",
			14 => "PROP_36_WEBASYST",
			15 => "PROP_45_WEBASYST",
			16 => "PROP_32_WEBASYST",
			17 => "PROP_22_WEBASYST",
			18 => "PROP_35_WEBASYST",
			19 => "PROP_38_WEBASYST",
			20 => "PROP_29_WEBASYST",
			21 => "PROP_23_WEBASYST",
			22 => "PROP_46_WEBASYST",
			23 => "PROP_31_WEBASYST",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "COLOR",
			1 => "PROP_30_WEBASYST",
			2 => "FASAD",
			3 => "PROP_33_WEBASYST",
			4 => "PROP_37_WEBASYST",
			5 => "PROP_16_WEBASYST",
			6 => "PROP_28_WEBASYST",
			7 => "PROP_47_WEBASYST",
			8 => "PROP_17_WEBASYST",
			9 => "PROP_20_WEBASYST",
			10 => "PROP_40_WEBASYST",
			11 => "PROP_41_WEBASYST",
			12 => "PROP_39_WEBASYST",
			13 => "PROP_50_WEBASYST",
			14 => "PROP_36_WEBASYST",
			15 => "PROP_45_WEBASYST",
			16 => "PROP_32_WEBASYST",
			17 => "PROP_22_WEBASYST",
			18 => "PROP_35_WEBASYST",
			19 => "PROP_38_WEBASYST",
			20 => "PROP_29_WEBASYST",
			21 => "PROP_23_WEBASYST",
			22 => "PROP_46_WEBASYST",
			23 => "PROP_31_WEBASYST",
			24 => "",
		),
		"OFFERS_SORT_FIELD" => "catalog_PRICE_1",
		"OFFERS_SORT_ORDER" => "ASC",
		"OFFERS_LIMIT" => "0",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"BY_LINK" => "Y",
		"COMPONENT_TEMPLATE" => "searchpage",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"BACKGROUND_IMAGE" => "-",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PRODUCT_PROPERTIES" => array(
			0 => "BRAND",
			1 => "PROP_33_WEBASYST",
			2 => "PROP_37_WEBASYST",
			3 => "PROP_16_WEBASYST",
			4 => "PROP_28_WEBASYST",
			5 => "PROP_47_WEBASYST",
			6 => "PROP_17_WEBASYST",
			7 => "PROP_20_WEBASYST",
			8 => "PROP_40_WEBASYST",
			9 => "PROP_41_WEBASYST",
			10 => "PROP_39_WEBASYST",
			11 => "PROP_50_WEBASYST",
			12 => "PROP_30_WEBASYST",
			13 => "PROP_36_WEBASYST",
			14 => "PROP_45_WEBASYST",
			15 => "PROP_32_WEBASYST",
			16 => "PROP_22_WEBASYST",
			17 => "PROP_35_WEBASYST",
			18 => "PROP_38_WEBASYST",
			19 => "PROP_29_WEBASYST",
			20 => "PROP_23_WEBASYST",
			21 => "PROP_34_WEBASYST",
			22 => "PROP_15_WEBASYST",
			23 => "PROP_46_WEBASYST",
			24 => "PROP_31_WEBASYST",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"VIEW" => "",
		"PROP_MORE_PHOTO" => "-",
		"PROP_ARTICLE" => "-",
		"PROP_ACCESSORIES" => "-",
		"USE_FAVORITE" => "Y",
		"USE_SHARE" => "Y",
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"DONT_SHOW_LINKS" => "N",
		"USE_AUTO_AJAXPAGES" => "N",
		"OFF_MEASURE_RATION" => "N",
		"USE_STORE" => "Y",
		"USE_MIN_AMOUNT" => "Y",
		"MIN_AMOUNT" => "",
		"MAIN_TITLE" => "",
		"COLUMNS5" => "N",
		"OFF_SMALLPOPUP" => "N",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PROP_SKU_MORE_PHOTO" => "-",
		"PROP_SKU_ARTICLE" => "-",
		"PROPS_ATTRIBUTES" => "",
		"RS_SECONDARY_ACTION_VARIABLE" => "",
		"RS_TAB_IDENT" => "rsec_thistab_viewed",
		"RS_TAB_NAME" => "",
		"TEMPLATE_THEME" => "blue",
		"PRODUCT_DISPLAY_MODE" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "������",
		"MESS_BTN_ADD_TO_BASKET" => "� �������",
		"MESS_BTN_SUBSCRIBE" => "�����������",
		"MESS_BTN_COMPARE" => "��������",
		"MESS_BTN_DETAIL" => "���������",
		"MESS_NOT_AVAILABLE" => "��� � �������",
		"ADD_TO_BASKET_ACTION" => "ADD"
	),
	false
);
						?></div><?
					}
				}
			}
			foreach($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"] as $iblock_id => $arIblock)
			{
				// catalog
				if(in_array($iblock_id,$arParams['IBLOCK_ID']))
				{
					$c = false;
					foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
					{
						if( IntVal($arItem['ITEM_ID'])<1 )
						{
							$c = true;
							break;
						}
					}
					if($c)
					{
						?><div class="iblock"><?
							?><div class="title"><h3><?=$arIblock["NAME"]?></h3></div><?
							foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
							{
								if( IntVal($arItem['ITEM_ID'])<1 )
								{
									?><div class="sitem"><?
										?><div class="name"><a href="<?=$arItem['URL']?>"><?=$arItem['TITLE_FORMATED']?></a></div><?
										?><div class="description"><?=$arItem['BODY_FORMATED']?></div><?
										?><div class="chain"><?=$arItem['CHAIN_PATH']?></div><?
									?></div><?
								}
							}
						?></div><?
					}
				}
			}
			// other
			foreach($arResult['EXT_SEARCH']['IBLOCK']['IBLOCKS'] as $iblock_id => $arIblock)
			{
				if(!in_array($iblock_id,$arParams['IBLOCK_ID']))
				{
					?><div class="iblock"><?
						?><div class="title<?if($isFirst):?> first<?endif;?>"><h3><?=$arIblock['NAME']?></h3></div><?
						foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
						{
							?><div class="sitem"><?
								?><div class="name"><a href="<?=$arItem['URL']?>"><?=$arItem['TITLE_FORMATED']?></a></div><?
								?><div class="description"><?=$arItem['BODY_FORMATED']?></div><?
								?><div class="chain"><?=$arItem['CHAIN_PATH']?></div><?
							?></div><?
						}
					?></div><?
					$isFirst = false;
				}
			}
		}
		////////////////// OTHER
		if(!empty($arResult['EXT_SEARCH']['OTHER']['ITEMS']))
		{
			?><div class="iblock"><?
				foreach($arResult['EXT_SEARCH']['OTHER']['ITEMS'] as $arOther)
				{
					?><div class="sitem"><?
						?><div class="name"><a href="<?=$arOther['URL']?>"><?=$arOther['TITLE_FORMATED']?></a></div><?
						?><div class="description"><?=$arOther['BODY_FORMATED']?></div><?
						?><div class="chain"><?=$arOther['CHAIN_PATH']?></div><?
					?></div><?
				}
			?></div><?
		}
	?></div><?
	
	if($arParams['DISPLAY_BOTTOM_PAGER'] != 'N'){
		echo '<br />'.$arResult['NAV_STRING'];
	}
}
else{
	ShowNote(GetMessage('SEARCH_NOTHING_TO_FOUND'));
};
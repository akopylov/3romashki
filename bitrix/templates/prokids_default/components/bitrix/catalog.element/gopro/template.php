<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$HAVE_OFFERS = (is_array($arResult['OFFERS']) && count($arResult['OFFERS'])>0) ? true : false;
if($HAVE_OFFERS) { $PRODUCT = &$arResult['OFFERS'][0]; } else { $PRODUCT = &$arResult; }
?>
    <link rel="stylesheet" href="/dist/css/lightbox.min.css">
    <script src="/dist/js/lightbox.js"></script>
    <script src="/dist/js/zoomsl-3.0.js"></script>
    <script>
        jQuery(function(){

            $(".my-foto").imagezoomsl({

                zoomrange: [1, 10],
                cursorshadeborder: "5px solid #000",
                magnifiereffectanimate: "fadeIn",
                magnifierpos: "right"
            });
        });
    </script>
    <div class="elementdetail js-element js-elementid<?=$arResult['ID']?> <?if($HAVE_OFFERS):?>offers<?else:?>simple<?endif;?><?
    if( isset($arResult['DAYSARTICLE2']) || isset($PRODUCT['DAYSARTICLE2']) ) { echo ' da2'; }
    if( isset($arResult['QUICKBUY']) || isset($PRODUCT['QUICKBUY']) ) { echo ' qb'; }
    ?> propvision1 clearfix" data-elementid="<?=$arResult['ID']?>" <?
    ?> data-elementname="<?=CUtil::JSEscape($arResult['NAME'])?>" data-detail="<?=$arResult['DETAIL_PAGE_URL']?>"<?
    ?>><i class="icon da2qb"></i><?
    // PICTURES
        ?><div class="pictures changegenimage"><?
        ?><div class="pic"><?

            if(isset($arResult['FIRST_PIC_DETAIL']['SRC']))
            {
            	$alt = (($arResult['FIRST_PIC_DETAIL']['DESCRIPTION'])?$arResult['FIRST_PIC_DETAIL']['DESCRIPTION']:$arResult['NAME']);
                ?><div class="glass"><?//<loop></loop>
                    ?><img data-lightbox="example-1" href="<?=$arResult['FIRST_PIC_DETAIL']['SRC']?>" class="my-foto genimage click" src="<?=$arResult['FIRST_PIC_DETAIL']['SRC']?>"  data-large="<?=$arResult['FIRST_PIC_DETAIL']['SRC']?>" alt="<?=$alt;?>" title="<?=$alt;?>"/><?
                    ?><div class="glass_lupa"></div><?
                    ?></div><?
            } else {
                ?><img src="<?=$arResult['NO_PHOTO']['src']?>" title="<?=$arResult['NAME']?>" alt="<?=$arResult['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']?>" /><?
            }
        ?></div><?
        if(isset($arResult['FIRST_PIC_DETAIL']['SRC']))
        {

            ?><div class="picslider horizontal scrollp"><?
                ?><a rel="nofollow" class="scrollbtn prev page" href="#"><i class="icon pngicons"></i></a><?
                ?><a rel="nofollow" class="scrollbtn next page" href="#"><i class="icon pngicons"></i></a><?
                ?><div class="d_jscrollpane scroll horizontal-only" id="d_scroll_<?=$arResult['ID']?>"><?
                    $imagesCnt = 0;
                    $imagesHTML = '';
                    $first = false;
                    if($HAVE_OFFERS)
                    {
                        foreach($arResult['OFFERS'] as $arOffer)
                        {
                            if( is_array($arOffer['DETAIL_PICTURE']['RESIZE']) )
                            {
                                $imagesHTML.= '<a rel="nofollow" class="changeimage';
                                if($arOffer['ID']==$PRODUCT['ID'])
                                {
                                    $imagesHTML.= ' scrollitem';
                                }
                                $imagesHTML.= ' imgoffer imgofferid'.$arOffer['ID'].'"';
                                if($arOffer['ID']==$PRODUCT['ID'])
                                {
                                    $imagesCnt++;
                                } else {
                                    $imagesHTML.= ' style="display:none;"';
                                }
                                $alt = (($arOffer['DETAIL_PICTURE']['DESCRIPTION'])?$arOffer['DETAIL_PICTURE']['DESCRIPTION']:$arOffer['DETAIL_PICTURE']['TITLE']);
                                $imagesHTML.= ' href="#">';
                                    $imagesHTML.= '<img src="'.$arOffer['DETAIL_PICTURE']['RESIZE']['src'].'" ';
                                        $imagesHTML.= 'alt="'.$alt.'" ';
                                        $imagesHTML.= 'title="'.$alt.'" ';
                                        $imagesHTML.= 'data-bigimage="'.$arOffer['DETAIL_PICTURE']['SRC'].'" ';
                                        $imagesHTML.= 'href="'.$arOffer['DETAIL_PICTURE']['SRC'].'" ';
                                        $imagesHTML.= 'data-lightbox="example-1" ';
                                    $imagesHTML.= '/>';
                                $imagesHTML.= '</a>';
                            }
                            if (is_array($arOffer['PROPERTIES'][$arParams['PROP_SKU_MORE_PHOTO']]['VALUE'][0]['RESIZE']))
                            {
                                foreach($arOffer['PROPERTIES'][$arParams['PROP_SKU_MORE_PHOTO']]['VALUE'] as $arImage)
                                {
                                    $imagesHTML.= '<a rel="nofollow" class="changeimage ';
                                    if($arOffer['ID']==$PRODUCT['ID'])
                                    {
                                        $imagesHTML.= ' scrollitem';
                                    }
                                    $imagesHTML.= ' imgoffer imgofferid'.$arOffer['ID'].'"';
                                    if($arOffer['ID']==$PRODUCT['ID'])
                                    {
                                        $imagesCnt++;
                                    } else {
                                        $imagesHTML.= ' style="display:none;"';
                                    }
                                    $imagesHTML.= ' href="#">';
                                        $imagesHTML.= '<img src="'.$arImage['RESIZE']['src'].'" ';
                                            $imagesHTML.= 'alt="'.$arResult['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'].'" ';
                                            $imagesHTML.= 'title="'.$arOffer['NAME'].'" ';
                                            $imagesHTML.= 'data-bigimage="'.$arImage['SRC'].'" ';
                                            $imagesHTML.= 'href="'.$arImage['SRC'].'" ';
                                            $imagesHTML.= 'data-lightbox="example-1" ';
                                        $imagesHTML.= '/>';
                                    $imagesHTML.= '</a>';
                                }
                            }
                        }
                    }
                    if( is_array($arResult['DETAIL_PICTURE']['RESIZE']) )
                    {
                    	$alt = (($arResult['DETAIL_PICTURE']['DESCRIPTION'])?$arResult['DETAIL_PICTURE']['DESCRIPTION']:$arResult['DETAIL_PICTURE']['TITLE']);
                        $imagesHTML.= '<a rel="nofollow" class="changeimage scrollitem" href="#">';
                            $imagesHTML.= '<img src="'.$arResult['DETAIL_PICTURE']['RESIZE']['src'].'" ';
                                $imagesHTML.= 'alt="'.$alt.'" ';
                                $imagesHTML.= 'title="'.$alt.'" ';
                                $imagesHTML.= 'data-bigimage="'.$arResult['DETAIL_PICTURE']['SRC'].'" ';
                                $imagesHTML.= 'href="'.$arResult['DETAIL_PICTURE']['SRC'].'" ';
                                $imagesHTML.= 'data-lightbox="example-1" ';
                            $imagesHTML.= '/>';
                        $imagesHTML.= '</a>';
                        $imagesCnt++;
                    }
                    if( is_array($arResult['PROPERTIES'][$arParams['PROP_MORE_PHOTO']]['VALUE'][0]['RESIZE']) )
                    {
                        foreach($arResult['PROPERTIES'][$arParams['PROP_MORE_PHOTO']]['VALUE'] as $arImage)
                        {
                        	$alt = (($arImage['DESCRIPTION'])?$arImage['DESCRIPTION']:$arResult['NAME']);
                            $imagesHTML.= '<a rel="nofollow" class="changeimage scrollitem" href="#">';
                                $imagesHTML.= '<img src="'.$arImage['RESIZE']['src'].'" ';
                                    $imagesHTML.= 'alt="'.$alt.'" ';
                                    $imagesHTML.= 'title="'.$alt.'" ';
                                    $imagesHTML.= 'data-bigimage="'.$arImage['SRC'].'" ';
                                    $imagesHTML.= 'href="'.$arImage['SRC'].'" ';
                                    $imagesHTML.= 'data-lightbox="example-1" ';
                                $imagesHTML.= '/>';
                            $imagesHTML.= '</a>';
                            $imagesCnt++;
                        }
                    }
                    ?><div class="sliderin scrollinner" style="width:<?=($imagesCnt*112)?>px;"><?=$imagesHTML?></div><?
                ?></div><?
            ?></div><?
            ?><div class="fancyimages noned" title="<?=$arResult['NAME']?>"><?
                ?><div class="fancygallery"><?
                    ?><table class="changegenimage"><?
                        ?><tbody><?
                            ?><tr><?
                                ?><td class="image"><img class="max genimage" src="<?=$arResult['FIRST_PIC']['SRC']?>" alt="" title="" /></td><?
                                ?><td class="slider"><?
                                    ?><div class="picslider scrollp vertical"><?
                                        ?><a rel="nofollow" class="scrollbtn prev pop" href="#"><i class="icon pngicons"></i></a><?
                                        ?><div class="popd_jscrollpane scroll vertical-only max" id="d_scroll_popup_<?=$arResult['ID']?>"><?
                                            ?><div class="scrollinner"><?
                                                ?><?=$imagesHTML?><?
                                            ?></div><?
                                        ?></div><?
                                        ?><a rel="nofollow" class="scrollbtn next pop" href="#"><i class="icon pngicons"></i></a><?
                                    ?></div><?
                                ?></td><?
                            ?></tr><?
                        ?></tbody><?
                    ?></table><?
                ?></div><?
            ?></div><?
        }
    ?></div><?
    // INFO
    ?><div class="info"><?
        // ARTICLE && STORES
        ?><div class="articleandstores clearfix"><?
            // ARTICLE
            if( isset($PRODUCT['PROPERTIES'][$arParams['PROP_SKU_ARTICLE']]['VALUE']) || isset($arResult['PROPERTIES'][$arParams['PROP_ARTICLE']]['VALUE']) ) {
                ?><div class="article"><?
                    if( $PRODUCT['PROPERTIES'][$arParams['PROP_SKU_ARTICLE']]['VALUE']!='' || $arResult['PROPERTIES'][$arParams['PROP_ARTICLE']]['VALUE']!='' ) {
                        ?><?=GetMessage('ARTICLE')?>: <span class="offer_article" <?
                            ?>data-prodarticle="<?=( $arResult['PROPERTIES'][$arParams['PROP_ARTICLE']]['VALUE']!='' ? $arResult['PROPERTIES'][$arParams['PROP_ARTICLE']]['VALUE'] : '' )?>"><?
                            ?><?=( $PRODUCT['PROPERTIES'][$arParams['PROP_SKU_ARTICLE']]['VALUE']!='' ? $PRODUCT['PROPERTIES'][$arParams['PROP_SKU_ARTICLE']]['VALUE'] : $arResult['PROPERTIES'][$arParams['PROP_ARTICLE']]['VALUE'] )?><?
                        ?></span><?
                    }
                ?></div><?
            } else {
                ?><div class="article" style="display:none;"><?=GetMessage('ARTICLE')?>: <span class="offer_article"></span></div><?
            }
            // STORES
            if($arParams['USE_STORE']=='Y') {
                ?><?$APPLICATION->IncludeComponent(
                    'bitrix:catalog.store.amount',
                    ( $arParams['STORES_TEMPLATE']!='' ? $arParams['STORES_TEMPLATE'] : 'gopro' ),
                    array(
                        "ELEMENT_ID" => $arResult['ID'],
                        "STORE_PATH" => $arParams['STORE_PATH'],
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000",
                        "MAIN_TITLE" => $arParams['MAIN_TITLE'],
                        "USE_STORE_PHONE" => $arParams['USE_STORE_PHONE'],
                        "SCHEDULE" => $arParams['USE_STORE_SCHEDULE'],
                        "USE_MIN_AMOUNT" => "N",
                        "GOPRO_USE_MIN_AMOUNT" => $arParams['USE_MIN_AMOUNT'],
                        "MIN_AMOUNT" => $arParams['MIN_AMOUNT'],
                        "SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
                        "SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
                        "USER_FIELDS" => $arParams['USER_FIELDS'],
                        "FIELDS" => $arParams['FIELDS'],
                        // gopro
                        'DATA_QUANTITY' => $arResult['DATA_QUANTITY'],
                        'FIRST_ELEMENT_ID' => $PRODUCT['ID'],
                    ),
                    $component,
                    array('HIDE_ICONS'=>'Y')
                );?><?
            }
        ?></div><?
        // PRICES
        if(is_array($arResult['CAT_PRICES']) && count($arResult['CAT_PRICES'])>1)
        {
            ?><div class="prices horizontal scrollp"><?
                $cnt = 0;
                $pricesHTML_head = '';
                $pricesHTML_old_price = '';
                $pricesHTML_price = '';
                $arPriceDefault = false;
                foreach($arResult['CAT_PRICES'] as $PRICE_CODE => $arResPrice)
                {
                    if(!$arResult['CAT_PRICES'][$PRICE_CODE]['CAN_VIEW'])
                        continue;
                    $arPrice = $PRODUCT['PRICES'][$PRICE_CODE];
                    if(intval($arPrice['PRINT_DISCOUNT_VALUE']) OR intval($arPrice['PRINT_DISCOUNT_VALUE']) > 0) {
                        // header
                        $pricesHTML_head .= '<th class="nowrap">' . $arResPrice['TITLE'] . '</th>';
                        // old price
                        $pricesHTML_old_price .= '<td class="nowrap"><span class="price old price_pv_' . $PRICE_CODE . '">';
                        if ($arPrice['DISCOUNT_DIFF'] > 0) {
                            $pricesHTML_old_price .= $arPrice['PRINT_VALUE'];
                        } else {
                            $pricesHTML_old_price .= '';
                        }
                        $pricesHTML_old_price .= '</span></td>';
                        // price
                        $pricesHTML_price .= '<td class="nowrap"><span class="price';
                        if ($arPrice['DISCOUNT_DIFF'] > 0) {
                            $pricesHTML_price .= ' new';
                        }
                        $pricesHTML_price .= ' price_pdv_' . $PRICE_CODE . '">' . $arPrice['PRINT_DISCOUNT_VALUE'] . '</span></td>';
                        $cnt++;
                    }else{
                        $arPriceDefault = true;
                    }
                }
                ?><a rel="nofollow" class="scrollbtn prev" href="#"><span></span><i class="icon pngicons"></i></a><?
                ?><a rel="nofollow" class="scrollbtn next" href="#"><span></span><i class="icon pngicons"></i></a><?
                ?><div class="prs_jscrollpane scroll horizontal-only" id="prs_scroll_<?=$arResult['ID']?>"><?
                    ?><div class="scrollinner" style="width:<?=($cnt*160)?>px;"><?
                        ?><table class="pricestable scrollitem"><?
                            ?><thead><?
                                ?><tr><?
                                    ?><?=$pricesHTML_head?><?
                                ?></tr><?
                            ?></thead><?
                            ?><tbody><?
                                ?><tr><?
                                    ?><?=$pricesHTML_old_price?><?
                                ?></tr><?
                                ?><tr><?
                                    ?><?=$pricesHTML_price?><?
                                ?></tr><?
                            ?></tbody><?
                        ?></table><?
                    ?></div><?
                ?></div><?
            ?></div><?
        } elseif(is_array($arResult['CAT_PRICES']) && count($arResult['CAT_PRICES'])==1) {
            ?><div class="soloprice"><?

                foreach($arResult['CAT_PRICES'] as $PRICE_CODE => $arResPrice)
                {
                    if(!$arResult['CAT_PRICES'][$PRICE_CODE]['CAN_VIEW'])
                        continue;
                    $arPrice = $PRODUCT['PRICES'][$PRICE_CODE];
                    if(intval($arPrice['PRINT_DISCOUNT_VALUE']) OR intval($arPrice['PRINT_DISCOUNT_VALUE']) > 0) {
                    ?><table><?
                        ?><tr><?
                            ?><td><div class="line"><span class="name"><?=GetMessage('SOLOPRICE_PRICE')?><span></div></td><td class="nowrap"><span class="price<?if( $arPrice['DISCOUNT_DIFF']>0 ):?> new<?endif;?> gen price_pdv_<?=$PRICE_CODE?>"><?=$arPrice['PRINT_DISCOUNT_VALUE']?></span></td><?
                        ?></tr><?
                        if( $arPrice['DISCOUNT_DIFF']>0 )
                        {
                            ?><tr class="hideifzero"><?
                                ?><td><div class="line"><span class="name"><?=GetMessage('SOLOPRICE_PRICE_OLD')?><span></div></td><td class="nowrap"><span class="price old price_pv_<?=$PRICE_CODE?>"><?=$arPrice['PRINT_VALUE']?></span></td><?
                            ?></tr><?
                            ?><tr class="hideifzero"><?
                                ?><td><div class="line"><span class="name"><?=GetMessage('SOLOPRICE_DISCOUNT')?><span></div></td><td class="nowrap"><span class="discount price_pd_<?=$PRICE_CODE?>"><?=$arPrice['PRINT_DISCOUNT_DIFF']?></span></td><?
                            ?></tr><?
                        }
                    ?></table><?
                    }else{
                        $arPriceDefault = true;
                    }
                }
            ?></div><?
        }
        ?><div style="color:#ff0000; font-size:14px; padding-bottom:10px;">������������ ���� ��������� � ��������� ��������</div><?
        // PROPERTIES
        if($arResult['DISPLAY_PROPERTIES']['BRAND']['VALUE']){
            echo '<!--'.$arResult['PROPERTIES']['BRAND']["VALUE_ENUM_ID"].'--><p>'.$arResult['DISPLAY_PROPERTIES']['BRAND']['NAME'].': <b>'.$arResult['DISPLAY_PROPERTIES']['BRAND']['VALUE'].'</b></p>';
            global $ProizText;
            if($ProizText[$arResult['PROPERTIES']['BRAND']["VALUE_ENUM_ID"]]){
            	echo $ProizText[$arResult['PROPERTIES']['BRAND']["VALUE_ENUM_ID"]];
            }
            echo '<br/>';
        }
        if($arResult["UF_TEXT_1"]){
        	echo $arResult["UF_TEXT_1"];
        }

    if($arParams['HIGHLOAD'] == 'HIGHLOAD_TYPE_LIST') {
      if(is_array($arResult['OFFERS_EXT']['PROPERTIES']) && count($arResult['OFFERS_EXT']['PROPERTIES'])>0)
      {
        ?><div class="properties properties_list clearfix"><?
          foreach($arResult['OFFERS_EXT']['PROPERTIES'] as $propCode => $arProperty)
          {
            $isColor = false;
            ?><div class="offer_prop offer_prop_list prop_<?=$propCode?> closed<?
              if(is_array($arParams['PROPS_ATTRIBUTES_COLOR']) && in_array($propCode,$arParams['PROPS_ATTRIBUTES_COLOR']))
              {
                $isColor = true;
                ?> color<?
              }
              ?>" data-code="<?=$propCode?>">
              <div class="offer_prop-name"><?=$arResult['OFFERS_EXT']['PROPS'][$propCode]['NAME']?>: </div><?
              ?><div class="div_select"><?
                ?><div class="div_options div_options_list"><?
                $firstVal = false;
                foreach($arProperty as $value => $arValue)
                {
                  ?><span class="div_option<?
                    if($arValue['FIRST_OFFER'] == 'Y'):?> selected<?
                    elseif($arValue['DISABLED_FOR_FIRST'] == 'Y'):?> disabled<?
                    endif;?>" data-value="<?=htmlspecialcharsbx($arValue['VALUE'])?>"><?
                    if($isColor)
                    {
                      ?><span style="background-image:url('<?=$arValue['PICT']['SRC']?>');" title="<?=$arValue['VALUE']?>"></span><?
                    } else {
                      ?><span class="list-item"><?=$arValue['VALUE']?></span><?
                    }
                  ?></span><?
                  if($arValue['FIRST_OFFER'] == 'Y')
                  {
                    $firstVal = $arValue;
                  }
                }
                ?></div><?
                if(is_array($firstVal))
                {
                  ?><div class="div_selected div_selected_list"><?
                    if($isColor)
                    {
                      ?><span style="background-image:url('<?=$firstVal['PICT']['SRC']?>');" title="<?=$firstVal['VALUE']?>"></span><?
                    } else {
                      ?><span><?=$firstVal['VALUE']?></span><?
                    }
                    ?><i class="icon pngicons"></i><?
                  ?></div><?
                }
              ?></div><?
            ?></div><?
          }
        ?></div><?
      }
    }
    //if($arParams['HIGHLOAD'] == 'HIGHLOAD_TYPE_SELECT'){
    else {
      if(is_array($arResult['OFFERS_EXT']['PROPERTIES']) && count($arResult['OFFERS_EXT']['PROPERTIES'])>0)
      {
        ?><div class="properties clearfix"><?
			global $USER;

          foreach($arResult['OFFERS_EXT']['PROPERTIES'] as $propCode => $arProperty)
          {
            $isColor = false;
            ?><div class="offer_prop prop_<?=$propCode?> closed<?
              if(is_array($arParams['PROPS_ATTRIBUTES_COLOR']) && in_array($propCode,$arParams['PROPS_ATTRIBUTES_COLOR']))
              {
                $isColor = true;
                ?> color<?
              }
              ?>" data-code="<?=$propCode?>">
              <span class="offer_prop-name"><?=($propCode == 'COLOR')?GetMessage('COLOR'):$arResult['OFFERS_EXT']['PROPS'][$propCode]['NAME']?>: </span><?
              ?><div class="div_select"><?
                ?><div class="div_options"><?
                $firstVal = false;
                foreach($arProperty as $value => $arValue)
                {
                  ?><div class="div_option<?
                    if($arValue['FIRST_OFFER'] == 'Y'):?> selected<?
                    elseif($arValue['DISABLED_FOR_FIRST'] == 'Y'):?> disabled<?
                    endif;?>" data-value="<?=htmlspecialcharsbx($arValue['VALUE'])?>"><?
                    if($isColor)
                    {
                      ?><span style="background-image:url('<?=$arValue['PICT']['SRC']?>');" title="<?=$arValue['VALUE']?>"></span> &nbsp; <?=$arValue['VALUE']?><?
                    } else {
                      ?><span><?=$arValue['VALUE']?></span><?
                    }
                  ?></div><?
                  if($arValue['FIRST_OFFER'] == 'Y')
                  {
                    $firstVal = $arValue;
                  }
                }
                ?></div><?
                if(is_array($firstVal))
                {
                  ?><div class="div_selected"><?
                    if($isColor)
                    {
                        ?><span style="background-image:url('<?=$firstVal['PICT']['SRC']?>');" title="<?=$firstVal['VALUE']?>"></span>  <t><?=$firstVal['VALUE']?></t><?
                    } else {
                      ?><span><?=$firstVal['VALUE']?></span><?
                    }
                    ?><i class="icon pngicons"></i><?
                  ?></div><?
                }
              ?></div><?
            ?></div><?
          }
        ?></div><?
      }
    }
    if($HAVE_OFFERS){
        ?><div class="charactersiticSKU"><?
            foreach($PRODUCT['DISPLAY_PROPERTIES'] as $arProp){
                if(!in_array($arProp['CODE'], $arParams['PROPS_ATTRIBUTES'])){
                    ?><div class="SKU_prop prop_num<?=$arProp['ID']?>">
                        <span class="name_prop_sku"><?=$arProp['NAME'].': ';?></span>
                        <span class="val_prop_sku"><?
                        echo(is_array($arProp['DISPLAY_VALUE']) ? implode(' / ', $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE']);
                        ?></span>
                    </div><?
                }
            }
        ?></div><?
        }
        // ADD2BASKET
            if(!$arPriceDefault) {
                ?>
                <noindex>
                <div class="buy clearfix"><?
                    ?>
                    <form class="add2basketform js-buyform<?= $arResult['ID'] ?> js-synchro<? if (!$PRODUCT['CAN_BUY']): ?> cantbuy<?endif; ?> clearfix"
                          name="add2basketform"><?
                        ?><input type="hidden" name="<?= $arParams['ACTION_VARIABLE'] ?>" value="ADD2BASKET"><?
                        ?><input type="hidden" name="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>" class="js-add2basketpid"
                                 value="<?= $PRODUCT['ID'] ?>"><?
                        if ($arParams['USE_PRODUCT_QUANTITY']) {
                            ?><span class="quantitytitle"><?= GetMessage('CT_BCE_QUANTITY') ?>&nbsp; &nbsp;</span><?
                            ?><span class="quantity"><?
                            ?><a class="minus js-minus">-</a><?
                            ?><input type="text" class="js-quantity"
                                     name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>"
                                     value="<?= $PRODUCT['CATALOG_MEASURE_RATIO'] ?>"
                                     data-ratio="<?= $PRODUCT['CATALOG_MEASURE_RATIO'] ?>"><?
                            if ($arParams['OFF_MEASURE_RATION'] != 'Y') {
                                ?><span class="js-measurename"><?= $PRODUCT['CATALOG_MEASURE_NAME'] ?></span><?
                            }
                            ?><a class="plus js-plus">+</a><?
                            ?></span><?
                        }
                        ?><a rel="nofollow" class="submit add2basket" href="#"
                             title="<?= GetMessage('ADD2BASKET') ?>"><i
                                    class="icon pngicons"></i><?= GetMessage('CT_BCE_CATALOG_ADD') ?></a><?
                        ?><a rel="nofollow" class="inbasket" href="<?= $arParams['BASKET_URL'] ?>"
                             title="<?= GetMessage('INBASKET_TITLE') ?>"><i
                                    class="icon pngicons"></i><?= GetMessage('INBASKET') ?></a><?
                        ?><a rel="nofollow" class="go2basket"
                             href="<?= $arParams['BASKET_URL'] ?>"><?= GetMessage('INBASKET_TITLE') ?></a><?
                        ?><a rel="nofollow" class="buy1click detail fancyajax fancybox.ajax"
                             href="<?= SITE_DIR ?>include/popup/buy1click/"
                             onclick="ga(�send', �event', �click�, �sent�); return true;"
                             title="<?= GetMessage('BUY1CLICK') ?>"><?= GetMessage('BUY1CLICK') ?></a><?

						$pos = strpos($arResult["DETAIL_PAGE_URL"], "/catalog/mebel/");
						if($pos !== false AND $arResult["PRICES"]["BASE"]["VALUE"] >= 10000){ ?>
						<br style="clear:both;" />
						<a class="fancyajax fancybox.ajax ras" href="/include/popup/ras/?urlElement=<?=$arResult["DETAIL_PAGE_URL"];?>">������ � ���������</a>
						<? }
                        /*
                        if($PRODUCT['CATALOG_SUBSCRIPTION']=='Y')
                        {
                            ?><a rel="nofollow" class="btn btn1 product2subscribe" href="#" title="<?=GetMessage('SUBSCRIBE_PROD_TITILE')?>"><?=GetMessage('SUBSCRIBE_PROD')?></a><?
                        }
                        */
                        ?><input type="submit" name="submit" class="noned" value=""/><?
                        ?></form><?
                    ?></div></noindex><?
            }
        // COMPARE & FAVORITE & CHEAPER
        ?><div class="threeblock clearfix"><?
            // COMPARE
            if($arParams['USE_COMPARE']=='Y')
            {
                ?><div class="compare"><?
                    ?><a rel="nofollow" class="add2compare" href="<?=$arResult['COMPARE_URL']?>"><i class="icon pngicons"></i><?=GetMessage('ADD2COMPARE')?></a><?
                ?></div><?
            }
            // FAVORITE & CHEAPER
            if($arParams['USE_FAVORITE']=='Y' || $arParams['USE_CHEAPER']=='Y')
            {
                ?><div class="favoriteandcheaper"><?
                    // FAVORITE
                    if($arParams['USE_FAVORITE']=='Y')
                    {
                        ?><div class="favorite"><?
                            ?><a rel="nofollow" class="add2favorite" href="#favorite"><i class="icon pngicons"></i><?=GetMessage('FAVORITE')?></a><?
                        ?></div><?
                    }
                    // CHEAPER
                    if($arParams['USE_CHEAPER']=='Y')
                    {
                        ?><div class="cheaper"><?
                            ?><a rel="nofollow" class="cheaper detail fancyajax fancybox.ajax" href="<?=SITE_DIR?>include/cheaper/" title="<?=GetMessage('CHEAPER')?>"><i class="icon pngicons"></i><?=GetMessage('CHEAPER')?></a><?
                        ?></div><?
                    }
                ?></div><?
            }
        ?></div>
        <?
		if ($arResult['PROPERTIES']["CIRK"]['VALUE_XML_ID']){ ?>
		<div class="cirk">
		<?=(($arResult['PROPERTIES']["CIRK"]["ACTION"])?'<img src="'.$arResult['PROPERTIES']["CIRK"]["ACTION"].'"/>':'');?>
		<?=(($arResult['PROPERTIES']["CIRK"]["HIT"])?'<img src="'.$arResult['PROPERTIES']["CIRK"]["HIT"].'"/>':'');?>
		<?=(($arResult['PROPERTIES']["CIRK"]["NEW"])?'<img src="'.$arResult['PROPERTIES']["CIRK"]["NEW"].'"/>':'');?>
		<?=(($arResult['PROPERTIES']["CIRK"]["SUPER"])?'<img src="'.$arResult['PROPERTIES']["CIRK"]["SUPER"].'"/>':'');?>
		</div>
		<?}?>
        <?
        // SHARE
        if($arParams['USE_SHARE']=='Y')
        {
            ?><div class="share"><?
                /*?><span class="b-share"><a class="email2friend b-share__handle b-share__link b-share-btn__vkontakte" href="#email2friend" title="<?=GetMessage('EMAIL2FRIEND')?>"><i class="b-share-icon icon pngicons"></i></a></span><?*/
                ?><span id="detailYaShare_<?=$arResult['ID']?>"></span><?
                ?><script type="text/javascript">
                new Ya.share({
                    link: 'http://<?=$_SERVER['HTTP_HOST']?><?=$arResult['DETAIL_PAGE_URL']?>',
                    title: '<?=CUtil::JSEscape($arResult['TITLE'])?>',
                    <?if(isset($arResult['PREVIEW_TEXT']) && $arResult['PREVIEW_TEXT']!=''):?>description: '<?=CUtil::JSEscape($arResult['PREVIEW_TEXT'])?>',<?endif;?>
                    <?if(isset($arResult['FIRST_PIC_DETAIL']['SRC'])):?>image: 'https://<?=$_SERVER['HTTP_HOST']?><?=$arResult['FIRST_PIC_DETAIL']['SRC']?>',<?endif;?>
                    element: 'detailYaShare_<?=$arResult['ID']?>',
                    elementStyle: {
                        'type': 'button',
                        'border': false,
                        'text': '<?=GetMessage('YSHARE')?>',
                        'quickServices': ['yaru','vkontakte','facebook','twitter','odnoklassniki']
                    },
                    popupStyle: {
                        blocks: {
                            '<?=GetMessage('YSHARE2')?>': ['yaru','vkontakte','facebook','twitter','odnoklassniki','gplus','liveinternet','lj','moikrug','moimir','myspace']
                        },
                        copyPasteField: false
                    }
                });
                </script><?
            ?></div><?
        }
        // PREVIEW TEXT
        if($arParams['SHOW_PREVIEW_TEXT']=='Y' && $arResult['PREVIEW_TEXT']!='')
        {
            ?><div class="previewtext">
                <?=$arResult['PREVIEW_TEXT']?>
            </div><?
        }
        if($arResult["SET_ARRAY"]){
        	echo $arResult["SET_ARRAY"];
        }

        global $TextCtalogDetail;
        echo $TextCtalogDetail;


        if( $arResult['TABS']['DETAIL_TEXT'] ) { ?>
            <br /><a class="go2detailfrompreview" href="#detailtext"><?=GetMessage('GO2DETAILFROMPREVIEW')?></a>
        <? }
    ?></div><?
?></div><?
?><script>
    BX.message({
        RSGoPro_DETAIL_PROD_ID: '<?=GetMessageJS('RSGOPRO.DETAIL_PROD_ID')?>',
        RSGoPro_DETAIL_PROD_NAME: '<?=GetMessageJS('RSGOPRO.DETAIL_PROD_NAME')?>',
        RSGoPro_DETAIL_PROD_LINK: '<?=GetMessageJS('RSGOPRO.DETAIL_PROD_LINK')?>',

        RSGoPro_DETAIL_CHEAPER_TITLE: '<?=GetMessageJS('RSGOPRO.DETAIL_CHEAPER_TITLE')?>',
    });
    $(document).ready(function() {
        if ($(document).width()<670) {
            $(".add2review").css("margin-top", "10px");
            $(".add2review").css("margin-left", "0px");
        }
    });
</script><?
// tabs
// tabs -> HEADERS
$this->SetViewTarget('TABS_HTML_HEADERS');
if( $arResult['TABS']['SET'] )
{
    ?><a class="switcher" href="#setOfers"><?=GetMessage('TABS_SET')?></a><?
}
if( $arResult['PROPERTIES']['SET']['VALUE'] AND count($arResult['PROPERTIES']['SET']['VALUE']) > 0)
{
    ?><a class="switcher" href="#set"><?=GetMessage('TABS_SET')?></a><?
}
if( $arResult['TABS']['DETAIL_TEXT'] )
{
    ?><a class="switcher" href="#detailtext"><?=GetMessage('TABS_DETAIL_TEXT')?></a><?
}
if(is_array($arResult['PROPERTIES']['SERT']['VALUE'])) {
    ?><a class="switcher" href="#sert"><?=GetMessage('SERT')?></a><?
}
/*if( $arResult['TABS']['DISPLAY_PROPERTIES'] )
{
    ?><a class="switcher" href="#properties"><?=GetMessage('TABS_PROPERTIES')?></a><?
}*/
//print_arr($arResult['PROPERTIES']['SET']);


if( $arResult['TABS']['PROPS_TABS'] )
{
    foreach($arParams['PROPS_TABS'] as $sPropCode)
    {
        if(
            $sPropCode!='' &&
            $arResult['PROPERTIES'][$sPropCode]['PROPERTY_TYPE']=='E' &&
            isset($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            is_array($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            count($arResult['PROPERTIES'][$sPropCode]['VALUE'])>0
        )
        {
            ?><a class="switcher" href="#prop<?=$sPropCode?>"><?=$arResult['PROPERTIES'][$sPropCode]['NAME']?></a><?
        } elseif(
            $sPropCode!='' &&
            $arResult['PROPERTIES'][$sPropCode]['PROPERTY_TYPE']=='F' &&
            isset($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            is_array($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            count($arResult['PROPERTIES'][$sPropCode]['VALUE'])>0
        ) { // files
            ?><a class="switcher" href="#prop<?=$sPropCode?>"><?=$arResult['PROPERTIES'][$sPropCode]['NAME']?></a><?
        } elseif( $sPropCode!='' && isset($arResult['DISPLAY_PROPERTIES'][$sPropCode]['DISPLAY_VALUE']) ) { // else
            ?><a class="switcher" href="#prop<?=$sPropCode?>"><?=$arResult['DISPLAY_PROPERTIES'][$sPropCode]['NAME']?></a><?
        }
    }
}
$this->EndViewTarget();
// tabs -> CONTENTS
$this->SetViewTarget('TABS_HTML_CONTENTS');
if( $arResult['TABS']['SET'] )
{
    ?><div class="content set selected" id="setOfers"><?
    ?><div class="contentbody clearfix Set"><?
    //if($HAVE_OFFERS && $arResult['OFFERS_IBLOCK']>0)
    if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
   {
       foreach($arResult['OFFERS'] as $arOffer)
       {
           if(!$arOffer['HAVE_SET'])
               continue;
           ?><div class="aroundset offer offerid<?=$arOffer['ID']?><?if($PRODUCT['ID']!=$arOffer['ID']):?> noned<?endif;?>"><?
               ?><?$APPLICATION->IncludeComponent('bitrix:catalog.set.constructor',
                   'gopro',
                   array(
                       'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
                       'ELEMENT_ID' => $arOffer['ID'],
                       'PRICE_CODE' => $arParams['PRICE_CODE'],
                       'BASKET_URL' => $arParams['BASKET_URL'],
                       'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                       'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                       'CACHE_TIME' => $arParams['CACHE_TIME'],
                       'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                   ),
                   $component,
                   array('HIDE_ICONS' => 'Y')
               );?><?
           ?></div><?
       }
   }
   else {
       //if($arResult['HAVE_SET'])
       //{
           ?><div class="aroundset simple"><?
               ?><?$APPLICATION->IncludeComponent('bitrix:catalog.set.constructor',
                   'gopro',
                   array(
                       'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                       'ELEMENT_ID' => $arResult['ID'],
                       'PRICE_CODE' => $arParams['PRICE_CODE'],
                       'BASKET_URL' => $arParams['BASKET_URL'],
                       'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                       'CACHE_TIME' => $arParams['CACHE_TIME'],
                       'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                       "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
                       "CURRENCY_ID" => $arParams['CURRENCY_ID'],
                   ),
                   $component,
                   array('HIDE_ICONS' => 'Y')
               );?><?
           ?></div><?
       //}
   }?>
    </div><?
    ?></div><?
}
?><?
if( $arResult['PROPERTIES']['SET']['VALUE'] AND count($arResult['PROPERTIES']['SET']['VALUE']) > 0 )
{
    ?><div class="content set selected" id="set"><?
        ?><div class="contentbody clearfix Set"><?
            //if($HAVE_OFFERS && $arResult['OFFERS_IBLOCK']>0)
             /*if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
            {
                foreach($arResult['OFFERS'] as $arOffer)
                {
                    if(!$arOffer['HAVE_SET'])
                        continue;
                    ?><div class="aroundset offer offerid<?=$arOffer['ID']?><?if($PRODUCT['ID']!=$arOffer['ID']):?> noned<?endif;?>"><?
                        ?><?$APPLICATION->IncludeComponent('bitrix:catalog.set.constructor',
                            'gopro',
                            array(
                                'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
                                'ELEMENT_ID' => $arOffer['ID'],
                                'PRICE_CODE' => $arParams['PRICE_CODE'],
                                'BASKET_URL' => $arParams['BASKET_URL'],
                                'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                'CACHE_TIME' => $arParams['CACHE_TIME'],
                                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );?><?
                    ?></div><?
                }
            }
            else {
                //if($arResult['HAVE_SET'])
                //{
                    ?><div class="aroundset simple"><?
                        ?><?$APPLICATION->IncludeComponent('bitrix:catalog.set.constructor',
                            'gopro',
                            array(
                                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                'ELEMENT_ID' => $arResult['ID'],
                                'PRICE_CODE' => $arParams['PRICE_CODE'],
                                'BASKET_URL' => $arParams['BASKET_URL'],
                                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                'CACHE_TIME' => $arParams['CACHE_TIME'],
                                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
                                "CURRENCY_ID" => $arParams['CURRENCY_ID'],
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );?><?
                    ?></div><?
                //}
            }*/
             global $lightFilterSet;
            $lightFilterSet = array(
                'ID' => $arResult['PROPERTIES']['SET']['VALUE']
            );

            ?><?$APPLICATION->IncludeComponent("bitrix:catalog.section", "gopro", Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// ??? ?????????
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// ????????
		"SECTION_ID" => "",	// ID ???????
		"SECTION_CODE" => "",	// ??? ???????
		"SECTION_USER_FIELDS" => $arParams["SECTION_USER_FIELDS"],
		"ELEMENT_SORT_FIELD" => "RAND",	// ?? ?????? ???? ????????? ????????
		"ELEMENT_SORT_ORDER" => "desc",	// ??????? ?????????? ?????????
		"ELEMENT_SORT_FIELD2" => "shows",	// ???? ??? ?????? ?????????? ?????????
		"ELEMENT_SORT_ORDER2" => "asc",	// ??????? ?????? ?????????? ?????????
		"FILTER_NAME" => "lightFilterSet",	// ??? ??????? ?? ?????????? ??????? ??? ?????????? ?????????
		"INCLUDE_SUBSECTIONS" => "A",	// ?????????? ???????? ??????????? ???????
		"SHOW_ALL_WO_SECTION" => "Y",	// ?????????? ??? ????????, ???? ?? ?????? ??????
		"HIDE_NOT_AVAILABLE" => "N",	// ??????, ?? ????????? ??? ???????
		"PAGE_ELEMENT_COUNT" => "100",	// ?????????? ????????? ?? ????????
		"LINE_ELEMENT_COUNT" => "3",	// ?????????? ????????? ????????? ? ????? ?????? ???????
		"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
		"OFFERS_LIMIT" => "0",	// ???????????? ?????????? ??????????? ??? ?????? (0 - ???)
		"TEMPLATE_THEME" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
		"MESS_BTN_BUY" => $arParams["MESS_BTN_BUY"],
		"MESS_BTN_ADD_TO_BASKET" => $arParams["MESS_BTN_ADD_TO_BASKET"],
		"MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
		"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
		"MESS_NOT_AVAILABLE" => $arParams["MESS_NOT_AVAILABLE"],
		"SECTION_URL" => $arParams["SECTION_URL"],	// URL, ??????? ?? ???????? ? ?????????? ???????
		"DETAIL_URL" => $arParams["DETAIL_URL"],	// URL, ??????? ?? ???????? ? ?????????? ???????? ???????
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],	// ???????? ??????????, ? ??????? ?????????? ??? ??????
		"AJAX_MODE" => "N",	// ???????? ????? AJAX
		"AJAX_OPTION_JUMP" => "N",	// ???????? ????????? ? ?????? ??????????
		"AJAX_OPTION_STYLE" => "N",	// ???????? ????????? ??????
		"AJAX_OPTION_HISTORY" => "N",	// ???????? ???????? ????????? ????????
		"CACHE_TYPE" => "A",	// ??? ???????????
		"CACHE_TIME" => "36000000",	// ????? ??????????? (???.)
		"CACHE_GROUPS" => "N",	// ????????? ????? ???????
		"SET_META_KEYWORDS" => "N",	// ????????????? ???????? ????? ????????
		"META_KEYWORDS" => "",	// ?????????? ???????? ????? ???????? ?? ????????
		"SET_META_DESCRIPTION" => "N",	// ????????????? ???????? ????????
		"META_DESCRIPTION" => "",	// ?????????? ???????? ???????? ?? ????????
		"BROWSER_TITLE" => "-",	// ?????????? ????????? ???? ???????? ?? ????????
		"ADD_SECTIONS_CHAIN" => "N",	// ???????? ?????? ? ??????? ?????????
		"DISPLAY_COMPARE" => "Y",
		"SET_TITLE" => "N",	// ????????????? ????????? ????????
		"SET_STATUS_404" => "N",	// ????????????? ?????? 404
		"CACHE_FILTER" => "N",	// ?????????? ??? ????????????? ???????
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => "N",	// ???????????? ????? ??? ? ???????????
		"SHOW_PRICE_COUNT" => "1",	// ???????? ???? ??? ??????????
		"PRICE_VAT_INCLUDE" => "N",	// ???????? ??? ? ????
		"CONVERT_CURRENCY" => "Y",	// ?????????? ???? ? ????? ??????
		"BASKET_URL" => "/personal/cart/",	// URL, ??????? ?? ???????? ? ???????? ??????????
		"ACTION_VARIABLE" => "action",	// ???????? ??????????, ? ??????? ?????????? ????????
		"PRODUCT_ID_VARIABLE" => "id",	// ???????? ??????????, ? ??????? ?????????? ??? ?????? ??? ???????
		"USE_PRODUCT_QUANTITY" => "Y",	// ????????? ???????? ?????????? ??????
		"ADD_PROPERTIES_TO_BASKET" => "N",	// ????????? ? ??????? ???????? ??????? ? ???????????
		"PRODUCT_PROPS_VARIABLE" => "prop",	// ???????? ??????????, ? ??????? ?????????? ?????????????? ??????
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// ????????? ????????? ? ??????? ??????, ? ??????? ????????? ?? ??? ??????????????
		"PRODUCT_PROPERTIES" => "",	// ?????????????? ??????
		"PAGER_TEMPLATE" => "gopro",	// ?????? ???????????? ?????????
		"DISPLAY_TOP_PAGER" => "N",	// ???????? ??? ???????
		"DISPLAY_BOTTOM_PAGER" => "N",	// ???????? ??? ???????
		"PAGER_TITLE" => "������",	// ???????? ?????????
		"PAGER_SHOW_ALWAYS" => "N",	// ???????? ??????
		"PAGER_DESC_NUMBERING" => "N",	// ???????????? ???????? ?????????
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ????? ??????????? ??????? ??? ???????? ?????????
		"PAGER_SHOW_ALL" => "N",	// ?????????? ?????? "???"
		"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => "sort",	// ?? ?????? ???? ????????? ??????????? ??????
		"OFFERS_SORT_ORDER" => "asc",	// ??????? ?????????? ??????????? ??????
		"OFFERS_SORT_FIELD2" => "id",	// ???? ??? ?????? ?????????? ??????????? ??????
		"OFFERS_SORT_ORDER2" => "asc",	// ??????? ?????? ?????????? ??????????? ??????
		"PROP_MORE_PHOTO" => "MORE_PHOTO",	// ???????? ? ???. ????????????? ???????
		"PROP_ARTICLE" => "CML2_ARTICLE",	// ???????? ? ????????? ??????
		"PROP_ACCESSORIES" => "ACCESSORIES",	// ???????? ? ????????? ???????????
		"USE_FAVORITE" => "N",	// ???????????? ?????????
		"USE_SHARE" => "Y",	// ???????????? ???????????? ??????????
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",	// ?????????? ??????, ???? ??????? ?? ???????
		"DONT_SHOW_LINKS" => "N",	// ?? ?????????? ?????? ?? ?????
		"USE_STORE" => "N",	// ?????????? ???? "?????????? ?????? ?? ??????"
		"USE_MIN_AMOUNT" => "Y",	// ????????? ????? ?? ?????????
		"MIN_AMOUNT" => "10",	// ????????, ???? ???????? ????????? "????"
		"MAIN_TITLE" => "������� �� �������",	// ????????? ?????
		"PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",	// ???????? ? ???. ????????????? ???????? ???????????
		"PROP_SKU_ARTICLE" => "-",	// ???????? ? ????????? ???????? ???????????
		"PROPS_ATTRIBUTES" => $arParams["PROPS_ATTRIBUTES"],
		"OFFERS_CART_PROPERTIES" => "",	// ???????? ???????????, ??????????? ? ???????
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// ???????? ??????????, ? ??????? ?????????? ?????????? ??????
		"AJAXPAGESID" => "ajaxpages_main",
		"AJAX_OPTION_ADDITIONAL" => "",	// ?????????????? ?????????????
		"VIEW" => "showcase",
		"COLUMNS5" => "Y",
		"SET_BROWSER_TITLE" => "N",	// ????????????? ????????? ???? ????????
		"USE_AUTO_AJAXPAGES" => "N",	// ???????????? ???????????? ??? ????????? ????????
		"PROPS_ATTRIBUTES_COLOR" => "",
		"COMPARE_PATH" => "",	// ???? ? ???????? ?????????
		"OFF_SMALLPOPUP" => "N",
		"COMPONENT_TEMPLATE" => "light",
		"BACKGROUND_IMAGE" => "-",	// ?????????? ??????? ???????? ??? ??????? ?? ????????
		"SEF_MODE" => "N",	// ???????? ????????? ???
		"SET_LAST_MODIFIED" => "N",	// ????????????? ? ?????????? ?????? ????? ??????????? ????????
		"USE_MAIN_ELEMENT_SECTION" => "N",	// ???????????? ???????? ?????? ??? ?????? ????????
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"OFF_MEASURE_RATION" => "Y",
		"PAGER_BASE_LINK_ENABLE" => "N",	// ???????? ????????? ??????
		"SHOW_404" => "N",	// ????? ??????????? ????????
		"MESSAGE_404" => "",	// ????????? ??? ?????? (?? ????????? ?? ??????????)
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// ?? ?????????? js-?????????? ? ??????????
		"CURRENCY_ID" => "RUB",	// ??????, ? ??????? ????? ??????????????? ????
		"USE_COMPARE" => "N",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST"
	),
	false
);?>
        </div><?
    ?></div><?
}

if( $arResult['TABS']['DETAIL_TEXT'] )
{
    ?><div class="content " id="detailtext"><?
        ?><div class="contentbody clearfix"><?
            ?><div class="contentinner"><?
                ?><?=$arResult['DETAIL_TEXT']?><?
            ?></div><?
        ?></div><?
    ?></div><?
}

if( is_array($arResult['PROPERTIES']['SERT']['VALUE']) ) { ?>
    <div class="content " id="sert">
        <div class="contentbody clearfix">
            <div class="contentinner">
                <? foreach($arResult['PROPERTIES']['SERT']['FILES'] AS $value){ ?>

                    <a href="<?=$value['FILES']?>" data-lightbox="image-1"><img
                            data-lightbox="example-sert"
                            src="<?=$value['RESIZE']?>"
                        /></a>
                <? } ?>
            </div>
        </div>
    </div>
<? }


if( $arResult['TABS']['DISPLAY_PROPERTIES'] )
{
    ?><div class="content properties " id="properties"><?
        ?><div class="contentbody clearfix"><?
            ?><div class="contentinner"><?
                $arTemp = array();
                if(is_array($arParams['PROPS_TABS']) && count($arParams['PROPS_TABS'])>0)
                {
                    foreach($arParams['PROPS_TABS'] as $sPropCode)
                    {
                        $arTemp[$sPropCode] = $sPropCode;
                    }
                }
                $APPLICATION->IncludeComponent('redsign:grupper.list',
                    'gopro',
                    array(
                        'DISPLAY_PROPERTIES' => array_diff_key($arResult['DISPLAY_PROPERTIES'], $arTemp),
                        'CACHE_TIME' => 36000,
                    ),
                    $component,
                    array('HIDE_ICONS'=>'Y')
                );
            ?></div><?
        ?></div><?
    ?></div><?
}

if( $arResult['TABS']['PROPS_TABS'] )
{
    global $lightFilter;
    foreach($arParams['PROPS_TABS'] as $sPropCode)
    {
        if(
            $sPropCode!='' &&
            $arResult['PROPERTIES'][$sPropCode]['PROPERTY_TYPE']=='E' &&
            isset($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            is_array($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            count($arResult['PROPERTIES'][$sPropCode]['VALUE'])>0
        )
        { // binds to elements
            ?><div class="content selected" id="prop<?=$sPropCode?>"><?
                ?><div class="contentbody clearfix"><?
                    ?><div class="contentinner"><?
                        $lightFilter = array(
                            'ID' => $arResult['PROPERTIES'][$sPropCode]['VALUE'],
                        );
                        ?><?$intSectionID = $APPLICATION->IncludeComponent(
                            'bitrix:catalog.section',
                            'light',
                            array(
                                'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                'ELEMENT_SORT_FIELD' => $arParams['ELEMENT_SORT_FIELD'],
                                'ELEMENT_SORT_ORDER' => $arParams['ELEMENT_SORT_ORDER'],
                                'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
                                'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
                                'PROPERTY_CODE' => $arParams['LIST_PROPERTY_CODE'],
                                'META_KEYWORDS' => $arParams['LIST_META_KEYWORDS'],
                                'META_DESCRIPTION' => $arParams['LIST_META_DESCRIPTION'],
                                'BROWSER_TITLE' => $arParams['LIST_BROWSER_TITLE'],
                                'INCLUDE_SUBSECTIONS' => $arParams['INCLUDE_SUBSECTIONS'],
                                'BASKET_URL' => $arParams['BASKET_URL'],
                                'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                                'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                                'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                                'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                                'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                                'FILTER_NAME' => 'lightFilter',
                                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                'CACHE_TIME' => $arParams['CACHE_TIME'],
                                'CACHE_FILTER' => $arParams['CACHE_FILTER'],
                                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                'SET_TITLE' => $arParams['SET_TITLE'],
                                'SET_STATUS_404' => $arParams['SET_STATUS_404'],
                                'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
                                'PAGE_ELEMENT_COUNT' => $arParams['PAGE_ELEMENT_COUNT'],
                                'LINE_ELEMENT_COUNT' => $arParams['LINE_ELEMENT_COUNT'],
                                'PRICE_CODE' => $arParams['PRICE_CODE'],
                                'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                                'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],

                                'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                                'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                                'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                                'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                                'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],

                                'DISPLAY_TOP_PAGER' => $arParams['DISPLAY_TOP_PAGER'],
                                'DISPLAY_BOTTOM_PAGER' => $arParams['DISPLAY_BOTTOM_PAGER'],
                                'PAGER_TITLE' => $arParams['PAGER_TITLE'],
                                'PAGER_SHOW_ALWAYS' => $arParams['PAGER_SHOW_ALWAYS'],
                                'PAGER_TEMPLATE' => $arParams['PAGER_TEMPLATE'],
                                'PAGER_DESC_NUMBERING' => $arParams['PAGER_DESC_NUMBERING'],
                                'PAGER_DESC_NUMBERING_CACHE_TIME' => $arParams['PAGER_DESC_NUMBERING_CACHE_TIME'],
                                'PAGER_SHOW_ALL' => $arParams['PAGER_SHOW_ALL'],

                                'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                                'OFFERS_FIELD_CODE' => $arParams['LIST_OFFERS_FIELD_CODE'],
                                'OFFERS_PROPERTY_CODE' => $arParams['LIST_OFFERS_PROPERTY_CODE'],
                                'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                                'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                                'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                                'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
                                'OFFERS_LIMIT' => $arParams['LIST_OFFERS_LIMIT'],

                                'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
                                'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
                                'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
                                'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                                // goPro params
                                'PROP_MORE_PHOTO' => $arParams['PROP_MORE_PHOTO'],
                                'PROP_ARTICLE' => $arParams['PROP_ARTICLE'],
                                'PROP_ACCESSORIES' => $arParams['PROP_ACCESSORIES'],
                                'USE_FAVORITE' => "N",
                                'USE_SHARE' => $arParams['USE_SHARE'],
                                'SHOW_ERROR_EMPTY_ITEMS' => $arParams['SHOW_ERROR_EMPTY_ITEMS'],
                                'PROP_SKU_MORE_PHOTO' => $arParams['PROP_SKU_MORE_PHOTO'],
                                'PROP_SKU_ARTICLE' => $arParams['PROP_SKU_ARTICLE'],
                                'PROPS_ATTRIBUTES' => $arParams['PROPS_ATTRIBUTES'],
                                // store
                                'USE_STORE' => $arParams['USE_STORE'],
                                'USE_MIN_AMOUNT' => $arParams['USE_MIN_AMOUNT'],
                                'MIN_AMOUNT' => $arParams['MIN_AMOUNT'],
                                'MAIN_TITLE' => $arParams['MAIN_TITLE'],
                                // some...
                                'BY_LINK' => 'Y',
                                // seo
                                "ADD_SECTIONS_CHAIN" => "N",
                                "SET_BROWSER_TITLE" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "ADD_ELEMENT_CHAIN" => "N",
                                "USE_COMPARE" => "N",
                            ),
                            $component,
                            array('HIDE_ICONS'=>'N')
                        );?><?
                    ?></div><?
                ?></div><?
            ?></div><?
        } elseif(
            $sPropCode!='' &&
            $arResult['PROPERTIES'][$sPropCode]['PROPERTY_TYPE']=='F' &&
            isset($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            is_array($arResult['PROPERTIES'][$sPropCode]['VALUE']) &&
            count($arResult['PROPERTIES'][$sPropCode]['VALUE'])>0
        ) { // files
            ?><div class="content files selected" id="prop<?=$sPropCode?>"><?
                ?><div class="contentbody clearfix"><?
                    ?><div class="contentinner"><?
                        $index = 1;
                        foreach($arResult['PROPERTIES'][$sPropCode]['VALUE'] as $arFile)
                        {
                            ?><a class="docs" href="<?=$arFile['FULL_PATH']?>"><?
                                ?><i class="icon pngicons <?=$arFile['TYPE']?>"></i><?
                                ?><span class="name"><?=$arFile['ORIGINAL_NAME']?></span><?
                                if( isset($arFile['DESCRIPTION']) ) { ?><span class="description"><?=$arFile['DESCRIPTION']?></span><? }
                                ?><span class="size">(<?=$arFile['TYPE']?>, <?=$arFile['SIZE']?>)</span><?
                            ?></a><?
                            if($index>3) { $index==0; }
                            ?><span class="separator x<?=$index?>"></span><?
                            $index++;
                        }
                    ?></div><?
                ?></div><?
            ?></div><?
        } elseif( $sPropCode!='' && isset($arResult['DISPLAY_PROPERTIES'][$sPropCode]['DISPLAY_VALUE']) ) { // else
            ?><div class="content selected" id="prop<?=$sPropCode?>"><?
                ?><div class="contentbody clearfix"><?
                    ?><div class="contentinner"><?
                        ?><?
                        if(is_array($arResult['DISPLAY_PROPERTIES'][$sPropCode]['DISPLAY_VALUE'])){
                            echo implode(' / ', $arResult['DISPLAY_PROPERTIES'][$sPropCode]['DISPLAY_VALUE']);
                        } else {
                            echo $arResult['DISPLAY_PROPERTIES'][$sPropCode]['DISPLAY_VALUE'];
                        }
                        ?><?
                    ?></div><?
                ?></div><?
            ?></div><?
        }
    }
}

if($arResult["BannerCatalogDetail"]){
	echo $arResult["BannerCatalogDetail"];
}
$this->EndViewTarget();

<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Page\Asset;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

$moduleId = "redsign.prokids";

// check module include
if (!Loader::includeModule($moduleId)) {
	ShowError(Loc::getMessage('RSGOPRO.ERROR_NOT_INSTALLED_GOPRO'));
	die();
}

if (!\Bitrix\Main\Loader::includeModule('redsign.devfunc')) {
    ShowError(Loc::getMessage('RSGOPRO.ERROR_NOT_INSTALLED_DEVFUNC'));
    die();
} else {
    RSDevFunc::Init(array('jsfunc'));
}

// is main page
$IS_MAIN = 'N';
if ($APPLICATION->GetCurPage(true) == SITE_DIR.'index.php')
	$IS_MAIN = 'Y';

// is catalog page
$IS_CATALOG = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'catalog/') === false)
	$IS_CATALOG = 'N';

// is personal page
$IS_PERSONAL = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'personal/') === false)
	$IS_PERSONAL = 'N';

// is auth page
$IS_AUTH = 'Y';
if (strpos($APPLICATION->GetCurPage(true), SITE_DIR.'auth/') === false)
	$IS_AUTH = 'N';

// is ajax hit
$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_SERVER['HTTP_X_FANCYBOX']) || isset($_REQUEST['AJAX_CALL']) && 'Y' == $_REQUEST['AJAX_CALL'];

CJSCore::Init('ajax');
$adaptive = COption::GetOptionString($moduleId, 'adaptive', 'Y');
$prop_option = COption::GetOptionString($moduleId, 'prop_option', 'line_through');
$Asset = Asset::getInstance();

// add strings
$Asset->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge" />');
$Asset->addString('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
$Asset->addString('<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>');
//$Asset->addString('<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>');

// add styles
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/media.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/jscrollpane/jquery.jscrollpane.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/jssor/style.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/offers.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/popup/style.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/js/glass/style.css');
if ($IS_AUTH == 'Y'){
	$Asset->addCss(SITE_TEMPLATE_PATH.'/css/auth.css');
}
$Asset->addCss(SITE_TEMPLATE_PATH.'/css/additional.css');
$Asset->addCss(SITE_TEMPLATE_PATH.'/custom/style.css');

// add scripts
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery-1.11.0.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.mousewheel.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.cookie.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jscrollpane/jquery.jscrollpane.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jscrollpane/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.core.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.utils.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jssor/jssor.slider.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.pack.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/scrollto/jquery.scrollTo.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/offers.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/popup/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/glass/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.maskedinput.min.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/timer.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/custom/script.js');
$Asset->addJs(SITE_TEMPLATE_PATH.'/js/a.kopylov.js');

?><!DOCTYPE html>
<html lang="ru">
<head>
	<title><?= $APPLICATION->ShowTitle(); ?></title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googleta�gmanager.com/gtag/js�?id=UA-114185907-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments)};
	gtag('js', new Date());
	gtag('config', 'UA-114185907-1');
	</script>
	<?php $APPLICATION->ShowHead(); ?>
	<script type="text/javascript">
	// some JS params
	var BX_COOKIE_PREFIX = 'BITRIX_SM_',
		SITE_ID = '<?=SITE_ID?>',
		SITE_DIR = '<?=str_replace('//','/',SITE_DIR);?>',
		SITE_TEMPLATE_PATH = '<?=str_replace('//','/',SITE_TEMPLATE_PATH);?>',
		SITE_CATALOG_PATH = 'catalog',
		RSGoPro_Adaptive = <?=( $adaptive=='Y' ? 'true' : 'false' )?>,
		RSGoPro_FancyCloseDelay = 1000,
		RSGoPro_FancyReloadPageAfterClose = false,
		RSGoPro_OFFERS = {},
		RSGoPro_FAVORITE = {},
		RSGoPro_COMPARE = {},
		RSGoPro_INBASKET = {},
		RSGoPro_STOCK = {},
		RSGoPro_PHONETABLET = "N";
	// messages
	BX.message({
		"RSGOPRO_JS_TO_MACH_CLICK_LIKES":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.JS_TO_MACH_CLICK_LIKES'))?>",
		"RSGOPRO_JS_COMPARE":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.RSGOPRO_JS_COMPARE'))?>",
		"RSGOPRO_JS_COMPARE_IN":"<?=CUtil::JSEscape(GetMessage('RSGOPRO.RSGOPRO_JS_COMPARE_IN'))?>"
	});
	</script>
	<link rel="icon" href="/favicon.png" type="image/x-icon" />
	<? if(defined('PAGEN') == 'Y'){ ?>
	<meta name="robots" content="noindex, follow" />
	<? } ?>
	<? if ($APPLICATION->GetCurPage(false) === '/'){ ?>
	<meta name="google-site-verification" content="qUsmlNP_S5p_N5kLDghPavnlTq62rmlT6syYnxnXJ9k" />
	<meta name="yandex-verification" content="4c10b9e534f87b4a" />
	<meta name="robots" content="noyaca"/>
	<meta name="robots" content="noodp"/>
	<? } ?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?$APPLICATION->ShowViewContent("LinkRel");?>
</head>
<body class="prop_option_<?=$prop_option?><?if($adaptive == 'Y'):?> adaptive<?endif;?><?=((defined("basket"))?" basket":"");?>">
	<div id="panel"><?=$APPLICATION->ShowPanel()?></div>
	<? if ($IS_MAIN == 'N'){ ?>
		<h1 class="pagetitle"><?$APPLICATION->ShowTitle(false)?></h1>
	<? }?>
	<?$APPLICATION->IncludeFile(
		SITE_DIR."include/header/metrik.php",
		Array(),
		Array("MODE"=>"html")
	);?>
	<div class="body"><!-- body -->
        <div class="fixHeader"></div>
        <? if(!defined("basket")){ ?>
        <div class="filterFon">
            <div id="tpanel" class="tpanel">
            <? /* ?><div class="rabMagasin">������, 13 ������� (�������) ��� ������� �������� � 9:30 �� 16:00. � � 14-�� ������� ���� ��� ����� ��������� � 9:30 �� 19:00</div><? */ ?>
                <div class="centering">
                    <div class="centeringin clearfix">
                        <div class="authandlocation nowrap">
                            <?$APPLICATION->IncludeFile(
                                SITE_DIR."include/header/location.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>
							<span class="adr"><?=Loc::getMessage('ADR')?></span>
                            <?$APPLICATION->IncludeFile(
                                SITE_DIR."include/header/auth.php",
                                Array(),
                                Array("MODE"=>"html")
                            );?>

                        </div>
                        <?$APPLICATION->IncludeFile(
                            SITE_DIR."include/header/topline_menu.php",
                            Array(),
                            Array("MODE"=>"html")
                        );?>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>
        <div class="headerFix">
            <div id="header" class="header">
                <? if(!defined("basket")){ ?>
            	<div class="MobyleBlock ContactInfo">������� ������� ������ "3 �������"<br /><a href="https://yandex.ru/maps/15/tula/house/kaluzhskoye_shosse_1a/Z04Ycw9jS0wEQFtufX10dHRiYQ==/?ll=37.583292%2C54.155815&z=16"><span style="color:#ff0000">����, ��������� �����, 1�, �� ������������</span></a></div>
                <? } ?>
                <div class="filterFon">
                    <div class="centering">
                        <div class="centeringin clearfix">
                            <div class="logo column1">
                                <div class="column1inner">
                                    <a href="<?=SITE_DIR?>">
                                        <?$APPLICATION->IncludeFile(
                                            SITE_DIR."include/company_logo.php",
                                            Array(),
                                            Array("MODE"=>"html")
                                        );?>
                                    </a>
                                </div>
                            </div>
                            <div class="phone column1 nowrap">
                                <div class="column1inner">

                                    <?$APPLICATION->IncludeFile(
                                        SITE_DIR."include/header/phone.php",
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                            </div>
                            <? if(!defined("basket")){ ?>
                            <div class="phone1 phone-first column1 nowrap">
                                <div class="column1inner">
                                    <a class="fancyajax fancybox.ajax recall" onclick="ga('send','event','callback','sent'); yaCounter5453968.reachGoal('order_sent'); return true;" href="<?=SITE_DIR?>include/popup/recall/"><i class="material-icons">&#xE61D;</i><?=Loc::getMessage('RSGOPRO.RECALL')?></a>
                                </div>
                            </div>

                            <div class="phone1 column1 nowrap">
                                <div class="column1inner">
                                    <a class="recall" href="/action/"><i class="material-icons">&#xE3E3;</i><?=Loc::getMessage('ACTION')?></a>
                                </div>
                            </div>
                            <div class="phone1 column1 nowrap">
                                <div class="column1inner">
                                    <a class="recall" href="/v-nalichii/" style="font-size:90%;padding: 5px 0px;width: 70px;"><i class="material-icons">&#xE3E3;</i><br />�&nbsp;�������</a>
                                </div>
                            </div>

							<div class="phone1 column1 nowrap soc">
								<div style="padding-top:2px; padding-left:5px; line-height:25px;">
								<i style="font-size:10px;"><?=Loc::getMessage('SOC')?></i><br/>
                                <a href="https://ok.ru/profile/570608720462"><img src="/include/icon_ok.png"></a>
								<a href="https://vk.com/club56382550"><img src="/include/icon_vk.png"></a>
								<a href="https://youtube.com/channel/UCI48b2Ij0C7QHoSbJhw3eLg"><img src="/include/icon_ut.png"></a>
								</div>
                            </div>
                            <? if(time() < 1672606801){ ?>
                            <div class="favorite column1 nowrap">
                                <div class="column1inner" style="color:#ff0000; font-weight:bold;">
                                	31 ������� � 1 ������ <br />������� �� ��������
                                </div>
                            </div>
                            <? } ?>
                            <? /* ?><div class="favorite column1 nowrap">
                                <div class="column1inner">
                                    <?$APPLICATION->IncludeFile(
                                        SITE_DIR."include/header/favorite.php",
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                            </div><? */ ?>
                            <div class="basket column1 nowrap" style="float: right;">
                                <div class="column1inner">
                                    <?$APPLICATION->IncludeFile(
                                        SITE_DIR."include/header/basket_small.php",
                                        Array(),
                                        Array("MODE"=>"html")
                                    );?>
                                </div>
                            </div>
                            <? } ?>
                        </div>
                    </div>
                    <div class="tline"></div>
                </div>
                <? if(!defined("basket")){ ?>
                <div class="centering">
                    <div class="centeringin clearfix">
                        <?$APPLICATION->IncludeFile(
                            SITE_DIR."include/header/menu.php",
                            Array(),
                            Array("MODE"=>"html")
                        );?>

                        <?$APPLICATION->IncludeFile(
                            SITE_DIR."include/header/search_title.php",
                            Array(),
                            Array("MODE"=>"html")
                        );?>

                    </div>
                </div>
                <? } ?>
            </div>
        </div>
        <div class="filterFon">
		<?php if ($IS_MAIN == 'N'): ?>
			<div id="title" class="title">
				<div class="centering">
					<div class="centeringin clearfix">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/header/breadcrumb.php",
							Array(),
							Array("MODE"=>"html")
						);?>
						<div class="H1BlockRepared">
							<script type="text/javascript">
								if($('.H1BlockRepared').length > 0 && $('h1').length > 0){
									$('.H1BlockRepared').after($('h1'));
									$('.H1BlockRepared').remove();
								}
							</script>
						</div>
					</div>
				</div>
			</div><!-- /title -->
		<?php endif; ?>
		<div id="content" class="content">
			<div class="centering">
				<div class="centeringin clearfix">

<?php
if ($isAjax) {
	$APPLICATION->RestartBuffer();
}

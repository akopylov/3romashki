/**
 * Created by A.Kopylov on 26.03.2017.
 */
$(document).ready(function () {
    if($('.filterFon').length) {
        $('.catalogmenucolumn').hover(
            function () {
                $('.filterFon').addClass('active');
            },
            function () {
                $('.filterFon').removeClass('active');
            }
        );
    }


    $(window).scroll(function(){
        var tpanel = $('#tpanel').height();
        var header = $('#header').height();
        var H = (tpanel+header);
        if(H > $(this).scrollTop()){
            $('.fixHeader').html('').removeClass('FixActiv');
        }else{
            $('.fixHeader').html((($('.headerFix').length)?$('.headerFix').html():"")+(($('#tpanel').length)?$('#tpanel').html():"")+'<div class="tline"></div>').addClass('FixActiv');
        }
    });

	if($('input[name="ORDER_PROP_3"]').length > 0){
		$('input[name="ORDER_PROP_3"]').mask("8-999-999-99-99");
	}


});
$(document).delegate( "div.tracker", 'click', function(e) {
    $('.pictures .glass img[data-lightbox="example-1"]').click();
    // console.log($(this).attr("class"));
});
$(document).delegate( "#knet_mrp_popup", 'click', function(e) {
    $(this).hide("slow");
	$('body').removeClass('knet_mrp_blur');
});


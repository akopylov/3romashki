<?
/**
 *	Company developer: ALTASIB
 *	Developers: adumnov
 *	Site: http://www.altasib.ru
 *	E-mail: dev@altasib.ru
 *	Copyright (c) 2006-2015 ALTASIB
 */

define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
// define('NOT_CHECK_PERMISSIONS', true);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');

global $DB;

$incMod = CModule::IncludeModuleEx("altasib.floataction");
if ($incMod == '3')
{
	echo GetMessage("ALTASIB_FLOATACTION_ERROR_TRIAL_VER");
	die();
}
if ($incMod == '0') die();

$popupWindow = new CJSPopup(GetMessage("ALTASIB_FLOATACTION_FORM_TITLE"), array("SUFFIX"=>($_GET['subdialog'] == 'Y'? 'subdialog':'')));

//Page path
if (!isset($_REQUEST["PATH_LINK"]) && strlen($_REQUEST["PATH_LINK"]) <= 0)
	die();

//Site ID
$site = SITE_ID;
if (isset($_REQUEST["SITE_ID"]) && strlen($_REQUEST["SITE_ID"]) > 0)
{
	$obSite = CSite::GetByID($_REQUEST["SITE_ID"]);
	if ($arSite = $obSite->Fetch())
		$site = $_REQUEST["SITE_ID"];
}

$strWarning = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" && !check_bitrix_sessid())
{
	CUtil::JSPostUnescape();
	$strWarning = GetMessage("MAIN_SESSION_EXPIRED");
}

$POST_RIGHT = $APPLICATION->GetGroupRight("altasib.floataction");
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$message = null;	// error message
$strMess = null;

// Processing changes form
if ($_SERVER["REQUEST_METHOD"] == "POST"
	&& (isset($_REQUEST["save"]) || strlen($_REQUEST["save"]) > 0)
	&& $POST_RIGHT=="W"			// check for permissions to write to the module
	&& check_bitrix_sessid()
)
{
	CUtil::JSPostUnescape();
	// Processing form
	$arFields = Array(
		"PATH_LINK"	=> $_REQUEST['PATH_LINK'],
		"IMG"		=> $_REQUEST['IMG'],
		"URL"		=> $_REQUEST['URL'],
		"ACTIVE"	=> ($_REQUEST['ACTIVE'] <> "Y"? "N":"Y"),
		"SITE_ID"	=> $_REQUEST['SITE_ID'],
		"DATE_BEGIN"=> $_REQUEST['DATE_BEGIN'],
		"DATE_END"	=> $_REQUEST['DATE_END'],
		"TITLE"		=> $_REQUEST['TITLE'],
		"ALT"		=> $_REQUEST['ALT'],
		"HEIGHT"	=> $_REQUEST['HEIGHT'],
		"WIDTH"		=> $_REQUEST['WIDTH'],
		"SIZE_TAB"	=> $_REQUEST['SIZE_TAB'],
	);

	if(isset($_REQUEST["ID"]) || intval($_REQUEST["ID"]) > 0)
	{
		if(isset($_REQUEST['Delete']) && $_REQUEST['Delete'] == 'Y')
		{
			CAltasib_FloatActionSql::DeleteAction($_REQUEST["ID"]);
			$strMess .= GetMessage("ALTASIB_FLOATACTION_SUCC_DELETED");
		}
		else
		{
			$ID = CAltasib_FloatActionDB::UpdateActions(intval($_REQUEST["ID"]), $arFields);

			if ($ID)
				$strMess .= GetMessage("ALTASIB_FLOATACTION_SUCC_CHANGED");
		}
	}
	else
	{	// saving data
		$ID = CAltasib_FloatActionDB::AddAction($arFields);

		if ($ID > 0)
			$strMess .= GetMessage("ALTASIB_FLOATACTION_SUCC_ADDED");
		else
			CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("ALTASIB_FLOATACTION_TEXT_ERROR").GetMessage("ALTASIB_FLOATACTION_FORM_DESCR").$_REQUEST["PATH_LINK"].GetMessage("ALTASIB_FLOATACTION_NOT_ADDED"), "TYPE"=>"NO"));
	}
	if(!$ID)
	{
		// if errors occurred while saving - display the text of the error
		if($e = $APPLICATION->GetException())
			$message = new CAdminMessage(GetMessage("ALTASIB_FLOATACTION_SAVE_ERROR"), $e);
	}
}

//	Sampling and preparation of data form.
//	date values
$fFull = CSite::GetDateFormat("FULL");
$db_format = "YYYY-MM-DD HH:MI:SS";

if ($POST_RIGHT<="R")
{
	CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("ALTASIB_FLOATACTION_NOT_ACCESS"), "TYPE"=>"NO"));
	die();
}
if (!check_bitrix_sessid())
{
	CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("ALTASIB_FLOATACTION_SESSION_NOT_FOT"), "TYPE"=>"NO"));
	die();
}

if (!isset($_REQUEST["save"]) || strlen($_REQUEST["save"]) <= 0){
	$res = CAltasib_FloatActionDB::CheckActionByPath($_REQUEST['PATH_LINK'], $_REQUEST['SITE_ID']);
	if (!$res){
		$strDescr = GetMessage("ALTASIB_FLOATACTION_FORM_DESCR_ADD");
		if(isset($ID))
			$arAction = CAltasib_FloatActionDB::GetActionByID($ID, false);
		else
		{	// default values
			$arValuesDef = array(
				"PATH_LINK"	=> $_REQUEST['PATH_LINK'],
				"IMG"		=> '',
				"URL"		=> '',
				"ACTIVE"	=> 'Y',
				"SITE_ID"	=> $_REQUEST['SITE_ID'],
				"DATE_BEGIN"=> '',
				"DATE_END"	=> '',
				"TITLE"		=> '',
				"ALT"		=> '',
				"HEIGHT"	=> "250",
				"WIDTH"		=> "250",
				"SIZE_TAB"	=> "100"
			);
		}
	}
	else{
		$arAction = CAltasib_FloatActionDB::GetActionByID($res, false);
		$strDescr = GetMessage("ALTASIB_FLOATACTION_FORM_DESCR_EDIT");
		$strActID = GetMessage("ALTASIB_FLOATACTION_CONFIRM_NUMBER").$res.")";
		$strUpdateInp = '<input type="text" value="'.$res.'" size="30" name="ID" style="display: none;" />';
		$arValuesDef = array(
			"PATH_LINK"	=> $PATH_LINK,
			"IMG"		=> $arAction['IMG'],
			"URL"		=> $arAction['URL'],
			"ACTIVE"	=> ($arAction['ACTIVE'] <> "Y"? "N":"Y"),
			"SITE_ID"	=> $SITE_ID,
			"DATE_BEGIN"=> $arAction['DATE_BEGIN'],
			"DATE_END"	=> $arAction['DATE_END'],
			"TITLE"		=> $arAction['TITLE'],
			"ALT"		=> $arAction['ALT'],
			"HEIGHT"	=> $arAction['HEIGHT'],
			"WIDTH"		=> $arAction['WIDTH'],
			"SIZE_TAB"	=> $arAction['SIZE_TAB']
		);
	}
}

// if there are error messages or successfully saved - derive their.

if($message)
	echo $message->Show();

// next deduce the actual form

//HTML Output
$popupWindow->ShowTitlebar(GetMessage("ALTASIB_FLOATACTION_FORM_TITLE"));

if (!isset($_REQUEST["save"]) || strlen($_REQUEST["save"]) <= 0):

// ------------- If not saving

$popupWindow->StartDescription("bx-property-page");

if ($strWarning != '')
	$popupWindow->ShowValidationError($strWarning);

?><p><?=$strDescr?> "<b><?=htmlspecialcharsbx($_REQUEST["PATH_LINK"])?></b>" <?=$strActID?></p><?

$popupWindow->EndDescription();
$popupWindow->StartContent();


$dateBegin = "";
$dateEnd = "";
if(!empty($arValuesDef['DATE_BEGIN']))
	$dateBegin = date($DB->DateFormatToPHP($fFull), MakeTimeStamp($arValuesDef['DATE_BEGIN'], $db_format));

if(!empty($arValuesDef['DATE_END']))
	$dateEnd = date($DB->DateFormatToPHP($fFull), MakeTimeStamp($arValuesDef['DATE_END'], $db_format));

?>
<tr>
	<td colspan="2">
		<div style='background-color: #fff; padding: 0; border-top: 1px solid #8E8E8E; border-bottom: 1px solid #8E8E8E; margin-bottom: 15px;'>
			<div style='background-color: #8E8E8E; height: 30px; padding: 7px; border: 1px solid #fff'>
				<a href='http://www.is-market.ru?param=cl' target='_blank'>
					<img src='/bitrix/images/altasib.floataction/is-market.gif' style='float: left; margin-right: 15px;' border='0' />
				</a>
				<div style='margin: 13px 0px 0px 0px'>
					<a href='http://www.is-market.ru?param=cl' target='_blank' style='color: #fff; font-size: 10px; text-decoration: none'><?=GetMessage('ALTASIB_IS')?></a>
				</div>
			</div>
		</div>
	</td>
</tr>

<table class="bx-width100" id="bx_page_properties">
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_IMG");?></td>
		<td><input type="text" value="<?=htmlspecialcharsEx($arValuesDef['IMG']);?>" size="36" style="width:90%;" name="IMG"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_TITLE");?></td>
		<td><input type="text" value="<?=htmlspecialcharsEx($arValuesDef['TITLE']);?>" size="30" style="width:80%;" name="TITLE"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_ALT");?></td>
		<td><input type="text" value="<?=htmlspecialcharsEx($arValuesDef['ALT']);?>" size="30" style="width:80%;" name="ALT"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_WIDTH_HEIGHT");?></td>
		<td><input type="text" value="<?=$arValuesDef['WIDTH'];?>" size="4" name="WIDTH"> x
		<input type="text" value="<?=$arValuesDef['HEIGHT'];?>" size="4" name="HEIGHT"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_SIZE_TAB");?></td>
		<td><input type="text" value="<?=$arValuesDef['SIZE_TAB'];?>" size="5" name="SIZE_TAB"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_URL");?></td>
		<td><input type="text" value="<?=htmlspecialcharsEx($arValuesDef['URL']);?>" size="36" style="width:90%;" name="URL"></td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_DATES");?></td>
		<td>
			<?echo CalendarDate("DATE_BEGIN", $dateBegin, "altasib_floataction_edit_form", "17");?>
			<?echo CalendarDate("DATE_END", $dateEnd, "altasib_floataction_edit_form", "17");?>
		</td>
	</tr>
	<tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_ACTIVE");?></td>
		<td>
		<input id="altasib_floataction_active" class="adm-designed-checkbox" type="checkbox" <?=($arValuesDef['ACTIVE'] == 'Y' ? 'checked=""' : "");?> value="<?=$arValuesDef['ACTIVE'];?>" name="ACTIVE">
		<label class="adm-designed-checkbox-label" for="altasib_floataction_active" title=""></label>
		</td>
	</tr>
<?if($res){
	?><tr>
		<td class="bx-width50 bx-popup-label"><?=GetMessage("ALTASIB_FLOATACTION_DELETE");?></td>
		<td>
		<input id="altasib_floataction_delete" class="adm-designed-checkbox" type="checkbox" value="N" name="Delete">
		<label class="adm-designed-checkbox-label" for="altasib_floataction_delete" title=""></label>
		</td>
	</tr><?
}
?>
</table>
<input type="text" value="<?=$_REQUEST["PATH_LINK"]?>" size="30" name="PATH_LINK" style="display: none;" />
<input type="text" value="<?=$_REQUEST["SITE_ID"]?>" size="30" name="SITE_ID" style="display: none;" />
<?=$strUpdateInp?>
<input type="hidden" name="save" value="Y" />
<?

$popupWindow->EndContent();
$popupWindow->ShowStandardButtons();

// ------------- End not saving

else:
	$popupWindow->StartContent();

	if($strMess != '')
		CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("ALTASIB_FLOATACTION_FORM_DESCR").$_REQUEST["PATH_LINK"].$strMess, "TYPE"=>"OK"));

	$popupWindow->EndContent();
	$popupWindow->ShowStandardButtons(array("close"));
	echo '<script>'
			.'top.BX.showWait(); '
			.'setTimeout(function() {'
				.'top.'.$popupWindow->jsPopup.'.Close(); '
				."top.BX.reload();"
			.'}, 1700);'
		.'</script>';

endif;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");?>
<?
define('NO_KEEP_STATISTIC', true);
define('NO_AGENT_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?		
$incMod = CModule::IncludeModuleEx("altasib.floataction");
if ($incMod == '3' || $incMod == '0')
	die();

echo CAltasibFloatAction::GetHtmlOfFile();
?>
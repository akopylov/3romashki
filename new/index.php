<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("�������");
?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"gopro",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAXPAGESID" => "ajaxpages_main",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COLUMNS5" => "Y",
		"COMPARE_PATH" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DONT_SHOW_LINKS" => "N",
		"ELEMENT_SORT_FIELD" => "id",
		"ELEMENT_SORT_FIELD2" => "shows",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"FILTER_NAME" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "A",
		"IS_AJAXPAGES" => $IS_AJAXPAGES,
		"IS_SORTERCHANGE" => $IS_SORTERCHANGE,
		"LINE_ELEMENT_COUNT" => "3",
		"MAIN_TITLE" => "������� �� �������",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "� �������",
		"MESS_BTN_BUY" => "������",
		"MESS_BTN_DETAIL" => "���������",
		"MESS_BTN_SUBSCRIBE" => "�����������",
		"MESS_NOT_AVAILABLE" => "��� � �������",
		"META_DESCRIPTION" => "",
		"META_KEYWORDS" => "",
		"MIN_AMOUNT" => "10",
		"OFFERS_CART_PROPERTIES" => array(""),
		"OFFERS_FIELD_CODE" => array("ID","CODE","XML_ID","NAME","TAGS","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","DATE_ACTIVE_FROM","ACTIVE_FROM","DATE_ACTIVE_TO","ACTIVE_TO","SHOW_COUNTER","SHOW_COUNTER_START","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_NAME","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","CREATED_USER_NAME","TIMESTAMP_X","MODIFIED_BY","USER_NAME",""),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER","","","","","","","",""),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "asc",
		"OFF_MEASURE_RATION" => "Y",
		"OFF_SMALLPOPUP" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "gopro",
		"PAGER_TITLE" => "������",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array("BASE"),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(""),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array("CML2_ARTICLE","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","COLOR","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","","","","","","",""),
		"PROPS_ATTRIBUTES" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_16_WEBASYST","PROP_17_WEBASYST","PROP_22_WEBASYST","PROP_23_WEBASYST","PROP_28_WEBASYST","PROP_29_WEBASYST","PROP_31_WEBASYST","PROP_32_WEBASYST","PROP_33_WEBASYST","PROP_35_WEBASYST","PROP_36_WEBASYST","PROP_37_WEBASYST","PROP_38_WEBASYST","PROP_39_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_45_WEBASYST","PROP_46_WEBASYST","PROP_47_WEBASYST"),
		"PROPS_ATTRIBUTES_COLOR" => array("COLOR"),
		"PROP_ACCESSORIES" => "ACCESSORIES",
		"PROP_ARTICLE" => "CML2_ARTICLE",
		"PROP_MORE_PHOTO" => "MORE_PHOTO",
		"PROP_SKU_ARTICLE" => "-",
		"PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "",
		"USE_AUTO_AJAXPAGES" => "N",
		"USE_FAVORITE" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_SHARE" => "Y",
		"USE_STORE" => "N",
		"VIEW" => "showcase"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������� �������");
$APPLICATION->SetPageProperty("title", "������� ������� ��� ����� - �������������� �������� �� ���� / 3 �������");
$APPLICATION->SetPageProperty("description", "������ ��� ����� � ��������-�������� - 3 �������. �������������� �������� �� ����. ������������ ��� ��� � ���. ������� � ���� �� ������ ��� �����.");
$APPLICATION->SetPageProperty("keywords", "������� �������");

$url = explode("?", $_SERVER["REQUEST_URI"]);
global $BannerCatalog, $BannerCatalogDetail, $ProizText;
$ProizText = Array(
	"2322" => "<div style='color:#ff0000; padding:0px 0px 0px 0px;'>*���������� �� ������� ������ 50%</div>",
);
if($url[0]){
	global $TextCtalogDetail;
	if(
		stristr($url[0], "/catalog/avtokresla_/")
		OR stristr($url[0], "/catalog/kolyaski_perenoski/")
	){
		$TextCtalogDetail = "<p style='color:#ff0000; padding:15px 0px 0px 0px;'>������� ������ �� ������ ���������, ����������, � ��������� �������� �� �������� 8-4872-38-47-22</p>";
	}

	if(
		stristr($url[0], "/catalog/mebel/podrostkovye_krovati/")
		OR stristr($url[0], "/catalog/mebel/dvukhyarusnye_krovati/")
		OR stristr($url[0], "/catalog/mebel/krovati_cherdaki/")
		OR stristr($url[0], "/catalog/mebel/krovati_cherdaki_s_rabochey_zonoy/")
		OR stristr($url[0], "/catalog/mebel/shkaf_krovat/")
		OR stristr($url[0], "/catalog/mebel/igrovye_krovati_cherdaki/")
		OR stristr($url[0], "/catalog/mebel/krovati_mashiny/")
	){
		$TextCtalogDetail = "<p style='color:#ff0000; padding:15px 0px 0px 0px;'>�� ������ ����� ��������� ������, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/detskie_matrasy/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a></p>";
	}


	if(
		stristr($url[0], "/catalog/matrasy_prinadlezhnosti_dlya_spalni/detskie_matrasy/")
		OR stristr($url[0], "/catalog/matrasy_prinadlezhnosti_dlya_spalni/kruglye_ovalnye_matrasy/")
		OR stristr($url[0], "/catalog/matrasy_prinadlezhnosti_dlya_spalni/matrasy_dlya_kolybeley/")
		OR stristr($url[0], "/catalog/matrasy_prinadlezhnosti_dlya_spalni/matrasy_dlya_krovatok_0/")
	){
		$TextCtalogDetail = "<p style='color:#ff0000; padding:15px 0px 0px 0px;'>�� ����� ������ ��������� �����������, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/namatrasniki_/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a></p>";
	}

	if(
		stristr($url[0], "/catalog/mebel/detskie_krovatki/")
	){
		$TextCtalogDetail = "<p style='color:#ff0000; padding:15px 0px 0px 0px;'>�� ������ ����� ��������� ������, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/matrasy_dlya_krovatok_0/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a>
		<br />� ����� ���������� �����, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/komplekty_v_krovatku/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a></p>";
	}

	if(
		stristr($url[0], "/catalog/mebel/ovalnye_krovatki_/")
	){
		$TextCtalogDetail = "<p style='color:#ff0000; padding:15px 0px 0px 0px;'>�� ������ ����� ��������� ������, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/kruglye_ovalnye_matrasy/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a>
		<br />� ����� ���������� �����, ������ �� ������ <a href='/catalog/matrasy_prinadlezhnosti_dlya_spalni/komplekty_v_krugluyu_krovatku/' style='border-bottom: 1px dashed #36acd4; color: #36acd4 !important;'>������</a></p>";
	}

	if(
		$url[0] == "/catalog/mebel/podrostkovye_krovati/"
		OR $url[0] == "/catalog/mebel/dvukhyarusnye_krovati/"
		OR $url[0] == "/catalog/mebel/krovati_cherdaki/"
		OR $url[0] == "/catalog/mebel/krovati_mashiny/"
		OR $url[0] == "/catalog/mebel/detskie_divany_i_kresla/divany/"
		OR $url[0] == "/catalog/mebel/mebel_iz_massiva/podrostkovye_krovati_iz_massiva/"
		OR $url[0] == "/catalog/mebel/mebel_iz_massiva/dvukhyarusnye_krovati_iz_massiva/"
		OR $url[0] == "/catalog/mebel/metallicheskie_krovati/"
		){
		$BannerCatalog = "<a href='/catalog/mebel/detskie_komnaty/'><img src='/catalog/b1.png' style='max-width:70%; display:block; margin:15px auto 0px auto;'></a>";
	}

	if(
		stristr($url[0], "/catalog/mebel/podrostkovye_krovati/")
		OR stristr($url[0], "/catalog/mebel/dvukhyarusnye_krovati/")
		OR stristr($url[0], "/catalog/mebel/krovati_cherdaki/")
		OR stristr($url[0], "/catalog/mebel/krovati_mashiny/")
		OR stristr($url[0], "/catalog/mebel/detskie_divany_i_kresla/divany/")
		OR stristr($url[0], "/catalog/mebel/mebel_iz_massiva/podrostkovye_krovati_iz_massiva/")
		OR stristr($url[0], "/catalog/mebel/mebel_iz_massiva/dvukhyarusnye_krovati_iz_massiva/")
		OR stristr($url[0], "/catalog/mebel/metallicheskie_krovati/")
		){
		$BannerCatalogDetail = "<a href='/catalog/mebel/detskie_komnaty/'><img src='/catalog/b1.png' style='max-width:70%; display:block; margin:15px auto 0px auto;'></a>";
	}


}


?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog",
	"gopro",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "2",
		"BASKET_URL" => "/personal/cart/",
		"BIGDATA_BLOCK_NAME" => "",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"COMPARE_FIELD_CODE" => array("PREVIEW_PICTURE","DETAIL_PICTURE",""),
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_OFFERS_FIELD_CODE" => array("ID","CODE","XML_ID","NAME","TAGS","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","DATE_ACTIVE_FROM","ACTIVE_FROM","DATE_ACTIVE_TO","ACTIVE_TO","SHOW_COUNTER","SHOW_COUNTER_START","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_NAME","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","CREATED_USER_NAME","TIMESTAMP_X","MODIFIED_BY","USER_NAME",""),
		"COMPARE_OFFERS_PROPERTY_CODE" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","OPORA","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER",""),
		"COMPARE_PROPERTY_CODE" => array("CML2_ARTICLE","BRAND","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","COLOR","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ACCESSORIES",""),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BROWSER_TITLE" => "meta_title_ru",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_META_DESCRIPTION" => "meta_description_ru",
		"DETAIL_META_KEYWORDS" => "meta_keywords_ru",
		"DETAIL_OFFERS_FIELD_CODE" => array("ID","CODE","XML_ID","NAME","TAGS","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","DATE_ACTIVE_FROM","ACTIVE_FROM","DATE_ACTIVE_TO","ACTIVE_TO","SHOW_COUNTER","SHOW_COUNTER_START","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_NAME","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","CREATED_USER_NAME","TIMESTAMP_X","MODIFIED_BY","USER_NAME",""),
		"DETAIL_OFFERS_PROPERTY_CODE" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","OPORA","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER",""),
		"DETAIL_PROPERTY_CODE" => array("CML2_ARTICLE","BRAND","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","ELEMENT_R","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","COLOR","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST",""),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"DETAIL_TABS_VIEW" => "tab",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"ELEMENT_SORT_FIELD" => "catalog_PRICE_1",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FIELDS" => array(0=>"",1=>"",),
		"FILE_404" => "/404.php",
		"FILTER_FIELD_CODE" => array("",""),
		"FILTER_FIXED" => "N",
		"FILTER_NAME" => "arrFilter",
		"FILTER_OFFERS_FIELD_CODE" => array("",""),
		"FILTER_OFFERS_PROPERTY_CODE" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","OPORA","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER",""),
		"FILTER_PRICE_CODE" => array("BASE"),
		"FILTER_PRICE_GROUPED" => array(),
		"FILTER_PRICE_GROUPED_FOR" => "products",
		"FILTER_PROPERTY_CODE" => array("BRAND","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","COLOR","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST",""),
		"FILTER_PROP_SCROLL" => array("BRAND"),
		"FILTER_PROP_SEARCH" => array("BRAND","PROP_15_WEBASYST","PROP_16_WEBASYST","PROP_17_WEBASYST","PROP_22_WEBASYST","PROP_23_WEBASYST","PROP_28_WEBASYST","PROP_29_WEBASYST","PROP_30_WEBASYST","PROP_31_WEBASYST","PROP_32_WEBASYST","PROP_33_WEBASYST","PROP_34_WEBASYST","PROP_35_WEBASYST","PROP_36_WEBASYST","PROP_37_WEBASYST","PROP_38_WEBASYST","PROP_39_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_45_WEBASYST","PROP_46_WEBASYST","PROP_47_WEBASYST","PROP_50_WEBASYST"),
		"FILTER_SKU_PROP_SCROLL" => array(),
		"FILTER_SKU_PROP_SEARCH" => array(),
		"FILTER_TEMPLATE" => "gopro",
		"FILTER_USE_AJAX" => "N",
		"FORUM_ID" => "1",
		"GIFTS_DETAIL_BLOCK_TITLE" => "�������� ���� �� ��������",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "�������",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "�������� ���� �� �������, ����� �������� �������",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_MESS_BTN_BUY" => "�������",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "������� � ������� ����� �������",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "3",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "�������",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIGHLOAD" => "HIGHLOAD_TYPE_SELECT",
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "N",
		"LINE_ELEMENT_COUNT" => "0",
		"LINK_ELEMENTS_URL" => "",
		"LINK_IBLOCK_ID" => "13",
		"LINK_IBLOCK_TYPE" => "1c_catalog",
		"LINK_PROPERTY_SID" => "CML2_LINK",
		"LIST_BROWSER_TITLE" => "UF_BROWSER_TITLE",
		"LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
		"LIST_META_KEYWORDS" => "UF_KEYWORDS",
		"LIST_OFFERS_FIELD_CODE" => array("ID","CODE","XML_ID","NAME","TAGS","SORT","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_TEXT","DETAIL_PICTURE","DATE_ACTIVE_FROM","ACTIVE_FROM","DATE_ACTIVE_TO","ACTIVE_TO","SHOW_COUNTER","SHOW_COUNTER_START","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_NAME","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","CREATED_USER_NAME","TIMESTAMP_X","MODIFIED_BY","USER_NAME",""),
		"LIST_OFFERS_LIMIT" => "0",
		"LIST_OFFERS_PROPERTY_CODE" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","OPORA","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER","MORE_PHOTO",""),
		"LIST_PROPERTY_CODE" => array("CML2_ARTICLE","BRAND","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","COLOR","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST",""),
		"MAIN_TITLE" => "�������",
		"MESSAGES_PER_PAGE" => "10",
		"MESSAGE_404" => "",
		"MIN_AMOUNT" => "10",
		"MODS_BLOCK_NAME" => "",
		"OFFERS_CART_PROPERTIES" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_36_WEBASYST","OPORA","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST","ECOVERLAYER"),
		"OFFERS_SORT_FIELD" => "catalog_PRICE_1",
		"OFFERS_SORT_FIELD2" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "asc",
		"OFF_MEASURE_RATION" => "Y",
		"OFF_SMALLPOPUP" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "gopro",
		"PAGER_TITLE" => "������",
		"PAGE_ELEMENT_COUNT" => "12",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"PRICE_CODE" => array("BASE"),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array("PROP_33_WEBASYST","PROP_37_WEBASYST","PROP_16_WEBASYST","PROP_28_WEBASYST","PROP_47_WEBASYST","PROP_17_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_39_WEBASYST","PROP_50_WEBASYST","PROP_30_WEBASYST","PROP_36_WEBASYST","PROP_45_WEBASYST","PROP_32_WEBASYST","PROP_22_WEBASYST","PROP_35_WEBASYST","PROP_38_WEBASYST","PROP_29_WEBASYST","PROP_23_WEBASYST","PROP_34_WEBASYST","PROP_15_WEBASYST","PROP_46_WEBASYST","PROP_31_WEBASYST"),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPS_ATTRIBUTES" => array("COLOR","PROP_50_WEBASYST","PROP_30_WEBASYST","FASAD","PROP_16_WEBASYST","PROP_17_WEBASYST","PROP_22_WEBASYST","PROP_23_WEBASYST","PROP_28_WEBASYST","PROP_29_WEBASYST","PROP_31_WEBASYST","PROP_32_WEBASYST","PROP_33_WEBASYST","PROP_35_WEBASYST","PROP_36_WEBASYST","PROP_37_WEBASYST","PROP_38_WEBASYST","PROP_39_WEBASYST","PROP_40_WEBASYST","PROP_41_WEBASYST","PROP_45_WEBASYST","PROP_46_WEBASYST","PROP_47_WEBASYST","OPORA","ECOVERLAYER"),
		"PROPS_ATTRIBUTES_COLOR" => array("COLOR","PROP_30_WEBASYST","FASAD","OPORA","ECOVERLAYER"),
		"PROPS_FILTER_COLORS" => array("COLOR"),
		"PROPS_SKU_FILTER_COLORS" => array(),
		"PROPS_TABS" => array(),
		"PROP_ARTICLE" => "CML2_ARTICLE",
		"PROP_MORE_PHOTO" => "MORE_PHOTO",
		"PROP_SKU_ARTICLE" => "-",
		"PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",
		"REVIEW_AJAX_POST" => "N",
		"SECTIONS_VIEW_MODE" => "VIEW_ELEMENTS",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "10",
		"SEF_FOLDER" => "/catalog/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => Array("compare"=>"compare/?action=#ACTION_CODE#","element"=>"#SECTION_CODE_PATH#/#ELEMENT_CODE#/","section"=>"#SECTION_CODE_PATH#/","sections"=>"","smart_filter"=>"#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/"),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHORT_SORTER" => "Y",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"SHOW_LINK_TO_FORUM" => "N",
		"SHOW_PREVIEW_TEXT" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "N",
		"SORTER_CNT_TEMPLATES" => "3",
		"SORTER_DEFAULT_TEMPLATE" => "",
		"SORTER_OFF_OUTPUT_OF_SHOW" => "N",
		"SORTER_OFF_SORT_BY" => "N",
		"SORTER_OUTPUT_OF" => array("8","12","16","20",""),
		"SORTER_OUTPUT_OF_DEFAULT" => "12",
		"SORTER_TEMPLATE_NAME_1" => "",
		"SORTER_TEMPLATE_NAME_2" => "",
		"SORTER_TEMPLATE_NAME_3" => "",
		"STORES" => "",
		"STORES_TEMPLATE" => "gopro",
		"STORE_PATH" => "/store/#store_id#",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_OFFERS_FIELD_CODE" => array("",""),
		"TOP_OFFERS_LIMIT" => "5",
		"TOP_OFFERS_PROPERTY_CODE" => array("",""),
		"TOP_PROPERTY_CODE" => array("",""),
		"URL_TEMPLATES_READ" => "",
		"USER_FIELDS" => array(0=>"",1=>"",),
		"USE_ALSO_BUY" => "Y",
		"USE_AUTO_AJAXPAGES" => "N",
		"USE_BIG_DATA" => "Y",
		"USE_BLOCK_BIGDATA" => "Y",
		"USE_BLOCK_MODS" => "N",
		"USE_CAPTCHA" => "Y",
		"USE_CHEAPER" => "N",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FAVORITE" => "N",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_GIFTS_SECTION" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "Y",
		"USE_SHARE" => "Y",
		"USE_STORE" => "N",
		"USE_STORE_PHONE" => "N",
		"USE_STORE_SCHEDULE" => "N",
		"VARIABLE_ALIASES" => array("compare"=>array("ACTION_CODE"=>"action",),)
	)
);?>
<? /*

if($url[0] == "/catalog/mebel/detskie_divany_i_kresla/divany/"
	OR $url[0] == "/catalog/mebel/krovati_cherdaki/"
	){ ?>
  <?$APPLICATION->IncludeComponent(
		"webdebug:popup_window",
		".default",
		array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "popup",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/catalog/ban/ban1.php",
			"POPUP_ANIMATION" => "fadeAndPop",
			"POPUP_APPEND_TO_BODY" => "Y",
			"POPUP_AUTOOPEN" => "Y",
			"POPUP_AUTOOPEN_DELAY" => "1000",
			"POPUP_AUTOOPEN_FULLHIDE" => "N",
			"POPUP_AUTOOPEN_ONCE" => "Y",
			"POPUP_AUTOOPEN_PATH" => "N",
			"POPUP_AUTOOPEN_TERM" => "1",
			"POPUP_CALLBACK_CLOSE" => "",
			"POPUP_CALLBACK_INIT" => "",
			"POPUP_CALLBACK_OPEN" => "",
			"POPUP_CALLBACK_SHOW" => "",
			"POPUP_CLASSES" => array(
				0 => "wd_popup_style_01",
				1 => "",
			),
			"POPUP_CLOSE" => "Y",
			"POPUP_CLOSE_TITLE" => "",
			"POPUP_DISPLAY_NONE" => "N",
			"POPUP_FIXED" => "Y",
			"POPUP_ID" => "BabNG",
			"POPUP_LINK_HIDDEN" => "N",
			"POPUP_LINK_SHOW" => "N",
			"POPUP_LINK_TEXT" => "�������",
			"POPUP_MIDDLE" => "Y",
			"POPUP_NAME" => "",
			"POPUP_WIDTH" => "",
			"COMPONENT_TEMPLATE" => ".default",
			"POPUP_LINK_TO" => ""
		),
		false
	);
} */
?>
<? /*

if($url[0] == "/catalog/mebel/krovati_cherdaki_s_rabochey_zonoy/"
	){ ?>
  <?$APPLICATION->IncludeComponent(
		"webdebug:popup_window",
		".default",
		array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "popup",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/catalog/ban/ban2.php",
			"POPUP_ANIMATION" => "fadeAndPop",
			"POPUP_APPEND_TO_BODY" => "Y",
			"POPUP_AUTOOPEN" => "Y",
			"POPUP_AUTOOPEN_DELAY" => "1000",
			"POPUP_AUTOOPEN_FULLHIDE" => "N",
			"POPUP_AUTOOPEN_ONCE" => "Y",
			"POPUP_AUTOOPEN_PATH" => "N",
			"POPUP_AUTOOPEN_TERM" => "1",
			"POPUP_CALLBACK_CLOSE" => "",
			"POPUP_CALLBACK_INIT" => "",
			"POPUP_CALLBACK_OPEN" => "",
			"POPUP_CALLBACK_SHOW" => "",
			"POPUP_CLASSES" => array(
				0 => "wd_popup_style_01",
				1 => "",
			),
			"POPUP_CLOSE" => "Y",
			"POPUP_CLOSE_TITLE" => "",
			"POPUP_DISPLAY_NONE" => "N",
			"POPUP_FIXED" => "Y",
			"POPUP_ID" => "BabNG",
			"POPUP_LINK_HIDDEN" => "N",
			"POPUP_LINK_SHOW" => "N",
			"POPUP_LINK_TEXT" => "�������",
			"POPUP_MIDDLE" => "Y",
			"POPUP_NAME" => "",
			"POPUP_WIDTH" => "",
			"COMPONENT_TEMPLATE" => ".default",
			"POPUP_LINK_TO" => ""
		),
		false
	);
} */
?>

<? /*

if($url[0] == "/catalog/mebel/podrostkovye_krovati/"
OR  $url[0] == "/catalog/mebel/dvukhyarusnye_krovati/"
OR  $url[0] == "/catalog/mebel/krovati_cherdaki/"
OR  $url[0] == "/catalog/mebel/trekhyarusnye_krovati/"
	){ ?>
  <?$APPLICATION->IncludeComponent(
		"webdebug:popup_window",
		".default",
		array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "popup",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/catalog/ban/ban4.php",
			"POPUP_ANIMATION" => "fadeAndPop",
			"POPUP_APPEND_TO_BODY" => "Y",
			"POPUP_AUTOOPEN" => "Y",
			"POPUP_AUTOOPEN_DELAY" => "1000",
			"POPUP_AUTOOPEN_FULLHIDE" => "N",
			"POPUP_AUTOOPEN_ONCE" => "Y",
			"POPUP_AUTOOPEN_PATH" => "N",
			"POPUP_AUTOOPEN_TERM" => "1",
			"POPUP_CALLBACK_CLOSE" => "",
			"POPUP_CALLBACK_INIT" => "",
			"POPUP_CALLBACK_OPEN" => "",
			"POPUP_CALLBACK_SHOW" => "",
			"POPUP_CLASSES" => array(
				0 => "wd_popup_style_01",
				1 => "",
			),
			"POPUP_CLOSE" => "Y",
			"POPUP_CLOSE_TITLE" => "",
			"POPUP_DISPLAY_NONE" => "N",
			"POPUP_FIXED" => "Y",
			"POPUP_ID" => "BabNG",
			"POPUP_LINK_HIDDEN" => "N",
			"POPUP_LINK_SHOW" => "N",
			"POPUP_LINK_TEXT" => "�������",
			"POPUP_MIDDLE" => "Y",
			"POPUP_NAME" => "",
			"POPUP_WIDTH" => "",
			"COMPONENT_TEMPLATE" => ".default",
			"POPUP_LINK_TO" => ""
		),
		false
	);
} */
?>

<? /*

if($url[0] == "/catalog/mebel/dvukhyarusnye_krovati/"
	){ ?>
  <?$APPLICATION->IncludeComponent(
		"webdebug:popup_window",
		".default",
		array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "popup",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/catalog/ban/ban3.php",
			"POPUP_ANIMATION" => "fadeAndPop",
			"POPUP_APPEND_TO_BODY" => "Y",
			"POPUP_AUTOOPEN" => "Y",
			"POPUP_AUTOOPEN_DELAY" => "1000",
			"POPUP_AUTOOPEN_FULLHIDE" => "N",
			"POPUP_AUTOOPEN_ONCE" => "Y",
			"POPUP_AUTOOPEN_PATH" => "N",
			"POPUP_AUTOOPEN_TERM" => "1",
			"POPUP_CALLBACK_CLOSE" => "",
			"POPUP_CALLBACK_INIT" => "",
			"POPUP_CALLBACK_OPEN" => "",
			"POPUP_CALLBACK_SHOW" => "",
			"POPUP_CLASSES" => array(
				0 => "wd_popup_style_01",
				1 => "",
			),
			"POPUP_CLOSE" => "Y",
			"POPUP_CLOSE_TITLE" => "",
			"POPUP_DISPLAY_NONE" => "N",
			"POPUP_FIXED" => "Y",
			"POPUP_ID" => "BabNG",
			"POPUP_LINK_HIDDEN" => "N",
			"POPUP_LINK_SHOW" => "N",
			"POPUP_LINK_TEXT" => "�������",
			"POPUP_MIDDLE" => "Y",
			"POPUP_NAME" => "",
			"POPUP_WIDTH" => "",
			"COMPONENT_TEMPLATE" => ".default",
			"POPUP_LINK_TO" => ""
		),
		false
	);
}*/
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class Meta{
    /**
     * ���������� ����������
     */
    public static $iblock_catalog   = 12;
    public static $iblock_sku       = 13;
    private static $PDO             = false;
    public static $sectionID        = Array();
    public static $Piblock_catalog  = Array();
    public static $Piblock_sku      = Array();

    /**
     * ���������� ������ ��� ������ 1�-�������
     */
    private static $iblock_section = false;
    private static $iblock_element = false;
    private static $iblock_properties = false;

    private static $DBConf = Array(
        'HOST' => 'localhost',
//        'HOST' => '92.53.96.170',
        'USER' => 'romashki_db',
        'PASS' => 'Fe10Sz96Md',
        'NAME' => 'romashki_db',
        'CHAR' => 'cp1251',
    );
    public static $director_pictures = '/import/products_pictures/';
    public static $director_pictures_enl = '/import/products_pictures_enl/';

    function meta(){
        $SQL_element = "
            SELECT
              `productID`,
              `meta_title_ru`,
              `meta_description_ru`,
              `meta_keywords_ru` 
            FROM `SC_products` 
        ";
        $items = self::pdo_select($SQL_element, true);
        if(is_array($items)){
            $el = self::$iblock_element;
            foreach ($items AS $value){
                echo 'ID.W: '.$value['productID'];
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID"=>self::$iblock_catalog, "=PROPERTY_ID_OLD"=>$value['productID']);
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

                $propsToModify['meta_title_ru'] = $value['meta_title_ru'];
                $propsToModify['meta_description_ru'] = $value['meta_description_ru'];
                $propsToModify['meta_keywords_ru'] = $value['meta_keywords_ru'];

                while($ob = $res->GetNextElement())
                {
                    $arFields = $ob->GetFields();
                    if($arFields['ID']){ echo ' - ID.B: '.$arFields['ID'].'<br/>';
                        $el->SetPropertyValuesEx(
                            $arFields['ID'],
                            self::$iblock_catalog,
                            $propsToModify
                        );
                        //$el->Update($arFields['ID'], $fieldsToModify);
                    }
                }
            }
        }
    }


    function __construct(){
        self::connection_db_import();
        if(CModule::IncludeModule("iblock")) {
            self::$iblock_section       = new CIBlockSection;
            self::$iblock_element       = new CIBlockElement;
            self::$iblock_properties    = new CIBlockProperty;
        }
    }
    function __destruct(){

    }
    /**
     * ���������� SQL ������ � �������� Assoc ������
     * @param $sql
     * @return array
     */
    function pdo_select($sql, $Array = NULL){
        $rows = false;
        $query = self::$PDO->query($sql);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $query->fetch()) {
            if($Array == NULL){
                $rows = $row;
            }else{
                $rows[] = $row;
            }
        }
        return $rows;
    }
    /**
     * ������������ � MySQL ����� PDO
     * @return bool
     */
    function connection_db_import(){
        try{
            if(self::$DBConf['CHAR']) {
                $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . self::$DBConf['CHAR']);
            }
            self::$PDO = new PDO(
                'mysql:host='.self::$DBConf['HOST'].';dbname='.self::$DBConf['NAME'],
                self::$DBConf['USER'],
                self::$DBConf['PASS'],
                $options);
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return true;
        }catch(PDOException $e){
            die('No connection DB');
        }
    }
    /**
     * ����� ������� � ����������������� ����
     * @param $m
     */
    function print_arr($m){
        echo '<pre>';
        print_r($m);
        echo '</pre>';
    }

}


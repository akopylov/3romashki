<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test");


$brend = Array();
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");
$arFilter = Array(
	"IBLOCK_ID"=>14,
	"ACTIVE_DATE"=>"Y",
	"ACTIVE"=>"Y",
	"!PREVIEW_PICTURE" => false
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arResult = Array();
while($ob  = $res->GetNextElement()){
 	$arFields = $ob->GetFields();
 	$arFields["PREVIEW_PICTURE"] = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);
 	$arFields["PREVIEW_PICTURE"]["src"] = CFile::ResizeImageGet(
          $arFields["PREVIEW_PICTURE"]["ID"],
          array(
           "width" => 300,
           "height" => 250
          ),
          BX_RESIZE_IMAGE_EXACT
        )["src"];
    $arResult["ITEMS"][] = $arFields;

}


if(is_array($arResult["ITEMS"]) AND count($arResult["ITEMS"])){
	?>
	<link rel="stylesheet" href="/dist/css/lightbox.min.css">
    <script src="/dist/js/lightbox.js"></script>
    <script src="/dist/js/zoomsl-3.0.js"></script>

	<div class="ListItems"><?
	foreach($arResult["ITEMS"] AS $value){
		?>
		<div class="_i">
			<div class="img">
				<img src="<?=$value["PREVIEW_PICTURE"]["src"]?>" data-lightbox="example-1" href="<?=$value["PREVIEW_PICTURE"]["SRC"]?>" data-large="<?=$value["PREVIEW_PICTURE"]["SRC"]?>" class="my-foto genimage click" alt="<?=$value["NAME"]?>" title="<?=$value["NAME"]?>"/>
				<div class="glass_lupa"></div>
			</div>
			<div class="name"><?=$value["NAME"]?></div>
		</div>
		<?
	}
	?></div><?
}

?>
<style>
.ListItems{
	font-size: 0px;
    margin: 0px -15px 0px -15px;
}
.ListItems ._i{
	display: inline-block;
    padding: 15px 15px 30px 15px;
    font-size: 14px;
    background-color: #fff;
    border: 1px solid #F2F2F2;
    max-width: 300px;
}
.ListItems ._i .img{}
.ListItems ._i .img img{
	max-width:100%;
	cursor:pointer;
}
.ListItems ._i .name{
	font-size: 110%;
    padding: 10px 0px 0px 0px;
    font-weight: bold;
    text-align: center;
}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
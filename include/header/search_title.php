<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"inheader",
	Array(
		"CATEGORY_0" => array("iblock_catalog"),
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0_iblock_catalog" => array("12"),
		"CHECK_DATES" => "N",
		"CONTAINER_ID" => "title-search",
		"CONVERT_CURRENCY" => "N",
		"IBLOCK_ID" => array("12"),
		"INPUT_ID" => "title-search-input",
		"NUM_CATEGORIES" => "1",
		"OFFERS_FIELD_CODE" => array("NAME","PREVIEW_PICTURE",""),
		"OFFERS_PROPERTY_CODE" => array("","CML2_LINK",""),
		"ORDER" => "date",
		"PAGE" => "/search/",
		"PRICE_CODE" => array("BASE"),
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "quan",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"TOP_COUNT" => "5",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?>
<?php
$IS_MAIN = 'N';
if ($APPLICATION->GetCurPage(true) == SITE_DIR.'index.php')
	$IS_MAIN = 'Y';
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"catalog",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CATALOG_PATH" => "/catalog/",
		"CHILD_MENU_TYPE" => "",
		"CONVERT_CURRENCY" => "N",
		"DELAY" => "N",
		"IBLOCK_ID" => array(),
		"IS_MAIN" => $IS_MAIN,
		"MAX_ITEM" => "9",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"OFFERS_FIELD_CODE" => array("",""),
		"OFFERS_PROPERTY_CODE" => array("",""),
		"PRICE_CODE" => array(),
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ROOT_MENU_TYPE" => "catalog",
		"RSGOPRO_CATALOG_PATH" => "/catalog/",
		"RSGOPRO_IS_MAIN" => "N",
		"RSGOPRO_MAX_ITEM" => "8",
		"RSGOPRO_PROPCODE_ELEMENT_IN_MENU" => "",
		"USE_EXT" => "Y",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?>